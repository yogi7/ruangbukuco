/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.1.38-MariaDB : Database - db_toko_online
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_toko_online` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_toko_online`;

/*Table structure for table `alamat_transaksi` */

DROP TABLE IF EXISTS `alamat_transaksi`;

CREATE TABLE `alamat_transaksi` (
  `alamat_transaksi_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaksi_id` varchar(25) DEFAULT NULL,
  `nama_penerima` varchar(50) DEFAULT NULL,
  `kode_pos` int(6) DEFAULT NULL,
  `no_hp` varchar(15) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `prov_id` varchar(25) DEFAULT NULL,
  `prov_nama` varchar(50) DEFAULT NULL,
  `kab_id` varchar(25) DEFAULT NULL,
  `kab_nama` varchar(50) DEFAULT NULL,
  `kec_id` varchar(25) DEFAULT NULL,
  `kec_nama` varchar(100) DEFAULT NULL,
  `alamat_lengkap` text,
  UNIQUE KEY `alamat_transaksi_alamat_transaksi_id_IDX` (`alamat_transaksi_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

/*Data for the table `alamat_transaksi` */

/*Table structure for table `banner` */

DROP TABLE IF EXISTS `banner`;

CREATE TABLE `banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `nama_gambar` varchar(50) DEFAULT NULL,
  `mdd` date DEFAULT NULL,
  UNIQUE KEY `banner_banner_id_IDX` (`banner_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

/*Data for the table `banner` */

insert  into `banner`(`banner_id`,`jenis`,`nama_gambar`,`mdd`) values 
(30,'main-banner','200824waqar-khalid-xHrkWhLVM8E-unsplash.jpg','2020-08-24'),
(31,'main-banner','200824parker-burchfield-tvG4WvjgsEY-unsplash.jpg','2020-08-24');

/*Table structure for table `bukti_transfer` */

DROP TABLE IF EXISTS `bukti_transfer`;

CREATE TABLE `bukti_transfer` (
  `bukti_transfer_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaksi_id` varchar(25) DEFAULT NULL,
  `nama_rek` varchar(100) DEFAULT NULL,
  `no_rek_pentransfer` varchar(100) DEFAULT NULL,
  `nama_file` varchar(100) DEFAULT NULL,
  `mdd` datetime DEFAULT NULL,
  UNIQUE KEY `bukti_transaksi_bukti_transaksi_id_IDX` (`bukti_transfer_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `bukti_transfer` */

/*Table structure for table `cart` */

DROP TABLE IF EXISTS `cart`;

CREATE TABLE `cart` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(25) NOT NULL,
  `produk_id` varchar(25) DEFAULT NULL,
  `jumlah` int(9) DEFAULT NULL,
  `nil_bayar` varchar(25) DEFAULT NULL,
  `tgl_insert` datetime DEFAULT NULL,
  `transaksi_st` enum('cart','dibeli') DEFAULT 'cart',
  `tgl_transaksi` datetime DEFAULT NULL,
  `tgl_transaksi_batal` datetime DEFAULT NULL,
  UNIQUE KEY `cart_cart_id_IDX` (`cart_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `cart` */

insert  into `cart`(`cart_id`,`user_id`,`produk_id`,`jumlah`,`nil_bayar`,`tgl_insert`,`transaksi_st`,`tgl_transaksi`,`tgl_transaksi_batal`) values 
(7,'2002070001','2008230008',3,'597000','2020-08-24 09:46:46','cart',NULL,NULL),
(8,'2002070001','2008240010',1,'970000','2020-08-24 09:46:56','cart',NULL,NULL);

/*Table structure for table `com_group` */

DROP TABLE IF EXISTS `com_group`;

CREATE TABLE `com_group` (
  `group_id` varchar(2) NOT NULL,
  `group_name` varchar(50) DEFAULT NULL,
  `group_desc` varchar(100) DEFAULT NULL,
  `mdb` varchar(10) DEFAULT NULL,
  `mdb_name` varchar(40) DEFAULT NULL,
  `mdd` datetime DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `com_group` */

insert  into `com_group`(`group_id`,`group_name`,`group_desc`,`mdb`,`mdb_name`,`mdd`) values 
('1','Developer web','Group developer website','1911130001',NULL,'2019-11-22 09:44:11'),
('2','Operator','Admin Operator','1911130001',NULL,'2019-11-22 09:47:26'),
('3','Pembeli','Pembeli secara umum','1911130001','admin','2020-01-03 09:00:41');

/*Table structure for table `com_menu` */

DROP TABLE IF EXISTS `com_menu`;

CREATE TABLE `com_menu` (
  `nav_id` int(11) NOT NULL,
  `parent_id` varchar(10) DEFAULT NULL,
  `nav_title` varchar(50) DEFAULT NULL,
  `nav_desc` varchar(100) DEFAULT NULL,
  `nav_url` varchar(100) DEFAULT NULL,
  `nav_no` int(11) unsigned DEFAULT NULL,
  `client_st` enum('1','0') DEFAULT '0',
  `active_st` enum('1','0') DEFAULT '1',
  `display_st` enum('1','0') DEFAULT '1',
  `nav_icon` varchar(50) DEFAULT NULL,
  `mdb` varchar(10) DEFAULT NULL,
  `mdb_name` varchar(50) DEFAULT NULL,
  `mdd` datetime DEFAULT NULL,
  PRIMARY KEY (`nav_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `com_menu` */

insert  into `com_menu`(`nav_id`,`parent_id`,`nav_title`,`nav_desc`,`nav_url`,`nav_no`,`client_st`,`active_st`,`display_st`,`nav_icon`,`mdb`,`mdb_name`,`mdd`) values 
(2,'0','Master Data','','#',3,'0','1','1','ti-harddrives','2002070001','superuser','2020-08-24 04:55:36'),
(3,'2','User','menu user','master/user',1,'0','1','1','','1911130001','admin','2019-11-22 04:06:47'),
(4,'0','Sistem','','#',21,'0','1','1','ti-settings','1911130001','admin','2019-11-22 04:06:08'),
(5,'4','Menu Navigasi','navigasi','sistem/navigation',1,'0','1','1',NULL,'1911130001','admin','2019-11-21 17:30:53'),
(6,'4','Role','master role','sistem/role',3,'0','1','1',NULL,'1911130001','admin','2019-11-21 17:31:23'),
(7,'4','Group','group role','sistem/group',2,'0','1','1','','1911130001','admin','2019-11-27 02:30:36'),
(8,'0','Logout','logout','sistem/login/logout',100,'0','1','1','ti-close','1911130001','admin','2019-11-22 04:06:36'),
(9,'0','Lihat Beranda','Link to beranda','client/beranda',99,'0','1','1','ti-home','1911130001','admin','2019-11-27 04:52:38'),
(10,'0','Dashboard','','#',1,'0','1','1','ti-home','1911130001','admin','2019-11-22 04:54:22'),
(11,'10','Dashboard','dashboard content','welcome',1,'0','1','1','','1911130001','admin','2019-11-22 04:58:30'),
(12,'4','Hak Akses','Hak akses user','sistem/permission',4,'0','1','1','','1911130001','admin','2020-01-16 09:17:03'),
(13,'0','Beranda','Untuk menu front client','client/beranda',1,'1','1','1','','1911130001','admin','2019-11-27 04:52:17'),
(14,'0','Tentang','Untuk menu front client','client/about',2,'1','1','0','','1911130001','admin','2020-01-31 07:53:08'),
(15,'0','Login','Login ke portal admin','welcome',3,'1','1','0','','1911130001','admin','2020-01-04 14:09:40'),
(16,'0','Pengaturan','Untuk mengatur halaman client','#',4,'0','1','1','ti-layout-grid3-alt','2002070001','superuser','2020-08-24 04:58:35'),
(17,'16','Atur Beranda','Beranda untuk halaman depan','setclient/beranda',1,'0','0','0','','1911130001','admin','2020-01-02 09:19:18'),
(18,'16','Atur Kontak','Atur data kontak di halaman depan','setclient/contact',2,'0','0','0','','2002070001','superuser','2020-02-08 19:26:56'),
(19,'2','Kategori','Kategori dari produk','master/kategori',3,'0','1','1','','1911130001','admin','2020-01-31 08:26:08'),
(20,'2','Produk','Master Data Produk','master/produk',4,'0','1','1','','1911130001','admin','2020-01-31 08:26:17'),
(21,'16','Banner Utama','untuk mengatur banner utama','setclient/banner',2,'0','1','1','','2002070001','superuser','2020-08-24 04:54:45'),
(22,'0','Transaksi','Transaksi admin','#',2,'0','1','1','ti-exchange-vertical','2002070001','superuser','2020-08-24 04:56:06'),
(23,'22','Pembelian','-','transaksi/pembelian',1,'0','1','1','','1911130001','admin','2020-01-25 18:07:32'),
(24,'22','Bukti Transfer','verifikasi foto bukti transfer','transaksi/bukti_transfer',2,'0','1','1','','1911130001','admin','2020-01-25 18:08:51'),
(25,'22','Masukan Resi','resi','transaksi/resi',3,'0','1','1','','1911130001','admin','2020-01-28 03:11:40'),
(26,'16','Prefrensi Toko','Atur nama dan logo toko','setclient/pref_toko',1,'0','1','1','','2002070001','superuser','2020-08-24 04:54:20'),
(27,'16','Rekening Toko','Rekening toko untuk ditampilkan ketika pembeli ingin melakukan transfer','setclient/rekening',3,'0','1','1','','2002070001','superuser','2020-08-24 05:35:48');

/*Table structure for table `com_reset_pass` */

DROP TABLE IF EXISTS `com_reset_pass`;

CREATE TABLE `com_reset_pass` (
  `data_id` varchar(20) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `nomor_telepon` varchar(50) DEFAULT NULL,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `request_date` datetime DEFAULT NULL,
  `request_st` enum('1','0','2') DEFAULT '0',
  `response_by` varchar(10) DEFAULT NULL,
  `response_date` datetime DEFAULT NULL,
  `response_notes` varchar(100) DEFAULT NULL,
  `request_expired` datetime DEFAULT NULL,
  `request_key` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `com_reset_pass` */

/*Table structure for table `com_role` */

DROP TABLE IF EXISTS `com_role`;

CREATE TABLE `com_role` (
  `role_id` varchar(5) NOT NULL,
  `group_id` varchar(2) DEFAULT NULL,
  `role_nm` varchar(100) DEFAULT NULL,
  `role_desc` varchar(100) DEFAULT NULL,
  `mdb` varchar(50) DEFAULT NULL,
  `mdb_name` varchar(50) DEFAULT NULL,
  `mdd` datetime DEFAULT NULL,
  PRIMARY KEY (`role_id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `com_role_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `com_group` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `com_role` */

insert  into `com_role`(`role_id`,`group_id`,`role_nm`,`role_desc`,`mdb`,`mdb_name`,`mdd`) values 
('1','1','Developer','Web developer','1911130001',NULL,'2019-11-22 09:45:00'),
('2','2','Administrator','Admin Website','admin',NULL,'2019-11-13 07:49:08'),
('3','3','Pembeli','','1911130001','admin','2020-01-03 09:01:37');

/*Table structure for table `com_role_menu` */

DROP TABLE IF EXISTS `com_role_menu`;

CREATE TABLE `com_role_menu` (
  `role_id` varchar(5) NOT NULL,
  `nav_id` varchar(10) NOT NULL,
  `role_tp` varchar(4) NOT NULL DEFAULT '1111',
  PRIMARY KEY (`nav_id`,`role_id`),
  KEY `role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `com_role_menu` */

insert  into `com_role_menu`(`role_id`,`nav_id`,`role_tp`) values 
('1','10','1111'),
('2','10','1111'),
('1','11','1111'),
('2','11','1111'),
('1','12','1111'),
('1','16','1111'),
('2','16','1111'),
('1','17','1111'),
('2','17','1111'),
('1','18','1111'),
('2','18','1111'),
('1','19','1111'),
('2','19','1111'),
('1','2','1111'),
('2','2','1111'),
('1','20','1111'),
('2','20','1111'),
('1','21','1111'),
('2','21','1111'),
('1','22','1111'),
('2','22','1111'),
('1','23','1111'),
('2','23','1111'),
('1','24','1111'),
('2','24','1111'),
('1','25','1111'),
('2','25','1111'),
('1','26','1111'),
('2','26','1111'),
('1','27','1111'),
('1','3','1111'),
('2','3','1111'),
('1','4','1111'),
('1','5','1111'),
('1','6','1111'),
('1','7','1111'),
('1','8','1111'),
('2','8','1111'),
('1','9','1111'),
('2','9','1111');

/*Table structure for table `com_role_user` */

DROP TABLE IF EXISTS `com_role_user`;

CREATE TABLE `com_role_user` (
  `user_id` varchar(10) NOT NULL,
  `role_id` varchar(5) NOT NULL,
  `role_default` enum('1','2') DEFAULT '2',
  `role_display` enum('1','0') DEFAULT '1',
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `com_role_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `com_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `com_role_user_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `com_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `com_role_user` */

insert  into `com_role_user`(`user_id`,`role_id`,`role_default`,`role_display`) values 
('2002070001','1','2','1'),
('2002070002','2','2','1'),
('2008130001','3','2','1');

/*Table structure for table `com_user` */

DROP TABLE IF EXISTS `com_user`;

CREATE TABLE `com_user` (
  `user_id` varchar(10) NOT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `user_pass` varchar(255) DEFAULT NULL,
  `user_key` varchar(50) DEFAULT NULL,
  `user_mail` varchar(50) DEFAULT NULL,
  `default_page` varchar(100) DEFAULT NULL,
  `user_st` enum('1','0','2') DEFAULT NULL,
  `examiner_number` varchar(50) DEFAULT NULL COMMENT 'Medex - Nomor Penunjukan Penguji',
  `mdb` varchar(10) DEFAULT NULL,
  `mdb_name` varchar(50) DEFAULT NULL,
  `mdd` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `com_user` */

insert  into `com_user`(`user_id`,`user_name`,`user_pass`,`user_key`,`user_mail`,`default_page`,`user_st`,`examiner_number`,`mdb`,`mdb_name`,`mdd`) values 
('2002070001','superuser','cObpWdg8YXeixxP0ovMV5xk4vMeqLCxeJtneYJu9NAWzrztPfgtz5Ft2mrgyaR57iq2omlllQJVyUg7beTLD+w==','1485659163','founder@artdev.id','welcome','1',NULL,'1911130001','admin','2020-05-05 09:27:15'),
('2002070002','operator','9hzMDkEFzxITeab+PtkYMAQyCEh4AtWDwhwtoeMtzlzn8Ld7tMlSK/KOYXwNyzhwiAZxjKn8pVuRfFYEurTZnQ==','4181705925','operator@gmail.com','welcome','1',NULL,'2002070001','superuser','2020-02-07 09:16:11'),
('2008130001','pembeli','LAU1H41D/mZB1A2gzUBc4MCj48C7ApdwCmq9Mbm0l2NpTIFGQ89VTKkGAbT7GjRPxZqcVve7xkQRDZChYESCXg==','1787488596','lathiif.aji.s@gmail.com','client/beranda','1',NULL,NULL,NULL,'2020-08-22 12:11:05');

/*Table structure for table `com_user_login` */

DROP TABLE IF EXISTS `com_user_login`;

CREATE TABLE `com_user_login` (
  `user_id` varchar(10) NOT NULL,
  `login_date` datetime NOT NULL,
  `logout_date` datetime DEFAULT NULL,
  `ip_address` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`login_date`),
  CONSTRAINT `com_user_login_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `com_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `com_user_login` */

/*Table structure for table `com_user_super` */

DROP TABLE IF EXISTS `com_user_super`;

CREATE TABLE `com_user_super` (
  `user_id` varchar(10) NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `com_user_super_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `com_user` (`user_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `com_user_super` */

insert  into `com_user_super`(`user_id`) values 
('2002070001');

/*Table structure for table `favorit` */

DROP TABLE IF EXISTS `favorit`;

CREATE TABLE `favorit` (
  `favorit_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(25) NOT NULL,
  `produk_id` varchar(25) DEFAULT NULL,
  `date_favorit` date DEFAULT NULL,
  UNIQUE KEY `favorit_favorit_id_IDX` (`favorit_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `favorit` */

insert  into `favorit`(`favorit_id`,`user_id`,`produk_id`,`date_favorit`) values 
(1,'2008130001','2002190003','2020-08-22');

/*Table structure for table `gambar_produk` */

DROP TABLE IF EXISTS `gambar_produk`;

CREATE TABLE `gambar_produk` (
  `gambar_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_id` varchar(25) NOT NULL,
  `gambar_nama` text,
  `mdb` varchar(25) DEFAULT NULL,
  `mdb_name` varchar(50) DEFAULT NULL,
  `mdd` datetime DEFAULT NULL,
  UNIQUE KEY `gambar_produk_gambar_id_IDX` (`gambar_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

/*Data for the table `gambar_produk` */

insert  into `gambar_produk`(`gambar_id`,`produk_id`,`gambar_nama`,`mdb`,`mdb_name`,`mdd`) values 
(10,'2002190005','200219pants.jpg','2002070001','superuser','2020-02-19 03:34:53'),
(13,'2008230008','200823163997_jogger-lia-in-pink_pink_4F8H3.jpg','2002070001','superuser','2020-08-23 12:59:24'),
(15,'2008230008','200823celana.jpg','2002070001','superuser','2020-08-23 12:59:24'),
(19,'2008230009','200823osella-celana-pendek-pria-khaki.jpg','2002070001','superuser','2020-08-23 13:02:05'),
(20,'2008240010','200824klim-musalimov-XUjjgpF4EPg-unsplash.jpg','2002070001','superuser','2020-08-24 06:19:01'),
(21,'2008240010','200824klim-musalimov-XGJH7Xgd1x4-unsplash.jpg','2002070001','superuser','2020-08-24 06:19:01'),
(22,'2008240010','200824klim-musalimov-PZBBTazSsqY-unsplash.jpg','2002070001','superuser','2020-08-24 06:19:01'),
(23,'2008240011','200824alina-grubnyak-tpo12_X_I2M-unsplash.jpg','2002070001','superuser','2020-08-24 06:21:55'),
(25,'2008240011','200824marc-schaefer-s1oKPhSPHAE-unsplash.jpg','2002070001','superuser','2020-08-24 06:23:32'),
(26,'2008240012','200824francesca-tosolini-HD7QBx2Yfa4-unsplash.jpg','2002070001','superuser','2020-08-24 06:30:22'),
(27,'2008240012','200824adam-winger-nclN_J0UtJ8-unsplash.jpg','2002070001','superuser','2020-08-24 06:30:22'),
(28,'2008240012','200824kim-schouten-f7h2nTvEknM-unsplash.jpg','2002070001','superuser','2020-08-24 06:30:22'),
(29,'2008240013','200824maxresdefault_(1).jpg','2002070001','superuser','2020-08-24 06:37:29'),
(30,'2008240013','200824maxresdefault.jpg','2002070001','superuser','2020-08-24 06:37:29'),
(31,'2008240014','200824jefferson.jpg','2002070001','superuser','2020-08-24 06:54:08');

/*Table structure for table `info_kurir` */

DROP TABLE IF EXISTS `info_kurir`;

CREATE TABLE `info_kurir` (
  `info_kurir_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaksi_id` varchar(25) DEFAULT NULL,
  `nama_kurir` varchar(50) DEFAULT NULL,
  `service` varchar(50) DEFAULT NULL,
  `wkt_perkiraan` varchar(50) DEFAULT NULL,
  `biaya_ongkir` int(11) DEFAULT NULL,
  `desk_kurir` text,
  `resi` varchar(100) DEFAULT NULL,
  UNIQUE KEY `info_kurir_info_kurir_id_IDX` (`info_kurir_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

/*Data for the table `info_kurir` */

/*Table structure for table `kategori` */

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_nama` varchar(50) DEFAULT NULL,
  `kategori_st` enum('yes','no') DEFAULT 'yes',
  `logo` text,
  `mdb` varchar(10) DEFAULT NULL,
  `mdb_name` varchar(50) DEFAULT NULL,
  `mdd` datetime DEFAULT NULL,
  PRIMARY KEY (`kategori_id`),
  UNIQUE KEY `user_name` (`kategori_nama`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `kategori` */

insert  into `kategori`(`kategori_id`,`kategori_nama`,`kategori_st`,`logo`,`mdb`,`mdb_name`,`mdd`) values 
(1,'sepeda','yes','200219bike.png','2002070001','superuser','2020-02-19 03:10:42'),
(2,'mobil','yes','200219car.png','2002070001','superuser','2020-02-19 03:10:47'),
(3,'kaos','yes','200219cloth.png','2002070001','superuser','2020-02-19 03:11:00'),
(4,'celana','yes','200219garment.png','2002070001','superuser','2020-02-19 03:11:09'),
(5,'gitar','yes','200219guitar.png','2002070001','superuser','2020-02-19 03:11:16'),
(6,'topi wanita','yes','200219headgear.png','2002070001','superuser','2020-02-19 03:11:27'),
(7,'sandal','yes','200219home.png','2002070001','superuser','2020-02-19 03:11:37'),
(8,'keyboard','yes','200219keyboard.png','2002070001','superuser','2020-02-19 03:11:45'),
(9,'motor','yes','200219motorcycle.png','2002070001','superuser','2020-02-19 03:11:52'),
(10,'mouse','yes','200219mouse-clicker.png','2002070001','superuser','2020-02-19 03:11:59'),
(11,'smartphone','yes','200219phone.png','2002070001','superuser','2020-02-19 03:12:12'),
(12,'dasi','yes','200219scarf.png','2002070001','superuser','2020-02-19 03:12:18'),
(13,'sepatu','yes','200219sneakers.png','2002070001','superuser','2020-02-19 03:12:25'),
(14,'Kasur','yes','200824bed.png','2002070001','superuser','2020-08-24 06:27:54'),
(15,'Mainan','yes','200824lego.png','2002070001','superuser','2020-08-24 06:36:20'),
(16,'Alat Musik','yes','200824electric-guitar.png','2002070001','superuser','2020-08-24 06:40:27');

/*Table structure for table `kontak` */

DROP TABLE IF EXISTS `kontak`;

CREATE TABLE `kontak` (
  `telephone` varchar(25) DEFAULT NULL,
  `no_whatsapp` varchar(25) DEFAULT NULL,
  `fanpage_fb` varchar(50) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `alamat` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kontak` */

insert  into `kontak`(`telephone`,`no_whatsapp`,`fanpage_fb`,`email`,`alamat`) values 
('082126641201','082126641201','lathiif aji santhosho','ajisanthoshol@gmail.com','karangsong');

/*Table structure for table `prefrensi` */

DROP TABLE IF EXISTS `prefrensi`;

CREATE TABLE `prefrensi` (
  `pref_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_pref` varchar(50) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `value_pref` varchar(100) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `nama_bank` varchar(70) DEFAULT NULL,
  `img_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`pref_id`),
  UNIQUE KEY `prefrensi_pref_id_IDX` (`pref_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `prefrensi` */

insert  into `prefrensi`(`pref_id`,`jenis_pref`,`kode`,`value_pref`,`keterangan`,`nama_bank`,`img_name`) values 
(1,'rekening','008','012651672','Lathiif Aji Santhosho','Mandiri','mandiri.png'),
(2,'email','0','founder@artdev.id','@Hijrah1996',NULL,NULL),
(3,'pref_toko','0','Artdev Toko',NULL,NULL,'artdeveloper.png');

/*Table structure for table `produk` */

DROP TABLE IF EXISTS `produk`;

CREATE TABLE `produk` (
  `produk_id` varchar(25) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `harga` varchar(12) DEFAULT NULL,
  `satuan` varchar(25) DEFAULT NULL,
  `berat` int(11) DEFAULT NULL COMMENT 'dalam bentuk gram',
  `deskripsi` text,
  `stok_st` enum('yes','no') DEFAULT NULL,
  `rekomendasi_st` enum('yes','no') DEFAULT 'no',
  `produk_eco_st` enum('yes','no') DEFAULT 'no',
  `active_st` enum('yes','no') DEFAULT 'yes',
  `mdb` varchar(10) DEFAULT NULL,
  `mdb_name` varchar(50) DEFAULT NULL,
  `mdd` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `produk` */

insert  into `produk`(`produk_id`,`kategori_id`,`nama`,`harga`,`satuan`,`berat`,`deskripsi`,`stok_st`,`rekomendasi_st`,`produk_eco_st`,`active_st`,`mdb`,`mdb_name`,`mdd`) values 
('2002190005',4,'Celana brown cool','350000','unit',600,'celana coklat import dengan bahan terbaik','yes','no','no','yes','2002070001','superuser','2020-08-24 06:26:56'),
('2008230008',4,'Osella Celana Jogger Perempuan Stripe Pink','199000','buah',540,'Point One 308661 Julia Relaxed Celana Wanita - Pink, long pants berbahan katun rami yang didesain trendy dengan elastic waistband dan side pockets sehingga nyaman digunakan.\r\n\r\nSize Chart : \r\n\r\nSize S/M : \r\nLingkar Pinggang : 72 cm, Lingkar Pinggul : 100 cm, Panjang Badan : 95 cm\r\nSize M/L : \r\nLingkar Pinggang : 77 cm, Lingkar Pinggul : 103 cm, Panjang Badan : 97 cm\r\nSize L/XL :\r\nLingkar Pinggang : 82 cm, Lingkar Pinggul : 105 cm, Panjang Badan : 97 cm\r\nTinggi model 172cm, mengenakan ukuran S/M','yes','yes','no','yes','2002070001','superuser','2020-08-24 06:31:57'),
('2008230009',4,'Bukalapak Celana Chino','99000','buah',400,'OUR NEW PRODUCT SHORT CHINO PANTS! Celana Chino Pendek Harga : size 28 36 = Rp. 110.000, size 38 40 = Rp. 120.000 Pilih Varian Warna','yes','no','no','yes','2002070001','superuser','2020-08-24 06:37:44'),
('2008240010',13,'Nike Sb Brown','970000','pasang',500,'NIKE Unisex Sportswear SB Charge Canvas Sepatu Olahraga Unisex [CD6279-202] merupakan sepatu olahraga terbuat dari material Suede berkualitas yang didesain sporty dengan eyelets, dan outsole yang nyaman pada saat digunakan berolahraga.\r\n\r\n \r\n\r\nSize Chart (US) :\r\n\r\nSize 6 (38.5) \r\nPanjang : 24 cm \r\nSize 6.5 (39) \r\nPanjang : 24.5 cm \r\nSize 7 (40) \r\nPanjang : 25 cm \r\nSize 7.5 (40.5) \r\nPanjang : 25.5 cm \r\nSize 8 (41) \r\nPanjang : 26 cm \r\nSize 8.5 (42) \r\nPanjang : 26.5 cm \r\nSize 9 (42.5) \r\nPanjang : 27 cm \r\nSize 9.5 (43)\r\nPanjang : 27.5 cm \r\nSize 10 (44) \r\nPanjang : 28 cm \r\nSize 10.5 (44.5)\r\nPanjang : 28.5 cm \r\nSize 11 (45) \r\nPanjang : 29 cm \r\nSize 11.5 (45.5) \r\nPanjang : 29.5 cm \r\nSize 12 (46) \r\nPanjang : 30 cm \r\nSize 13 (47.5) \r\nPanjang : 31 cm \r\nSize 14 (48.5)\r\nPanjang : 32 cm \r\nSize 15 (49.5)\r\nPanjang : 33 cm','yes','yes','no','yes','2002070001','superuser','2020-08-24 06:31:21'),
('2008240011',13,'Adidas Strip','890000','pasang',520,'Adidas Advantage France Sepatu Olahraga, sneakers shoes berbahan leather yang didesain trendy dengan neat stitching, 6 eyelets dan outsole yang nyaman saat digunakan.\r\n\r\nSize Chart\r\n\r\nSize 36\r\nPanjang : 22 cm\r\nSize 37\r\nPanjang : 23 cm\r\nSize 38\r\nPanjang : 23.5 cm\r\nSize 39\r\nPanjang : 24 cm\r\nSize 40\r\nPanjang : 25 cm\r\nSize 41\r\nPanjang : 26 cm\r\nSize 42\r\nPanjang : 26.5 cm\r\nSize 43\r\nPanjang : 27 cm\r\nSize 44\r\nPanjang : 28 cm','yes','yes','no','yes','2002070001','superuser','2020-08-24 06:32:33'),
('2008240012',14,'Kasur Cream soft','5600000','buah',2500,'Fasilitas :\r\nKasur Springbed Fullset\r\nTerbuat dari Busa Rebounded fiber foam berkualitas tinggi\r\nUntuk menikmati tidur yang berkualitas\r\nMampu beraktivitas dengan baik untuk melewati kegiatan sehari-hari yang lebih optimal\r\nTebal : 18 cm\r\n\r\nDeskripsi tambahan :\r\nCentral Maxy Kasur Busa Rebounded HB Elegance Oreo Set Springbed [Tebal 18 cm/ Fullset/ Khusus Jabodetabek] merupakan kasur busa rebounded fiber foam yang memiliki ketebalan 18 cm untuk menikmati tidur yang berkualitas sehingga mampu beraktivitas dengan baik untuk melewati kegiatan sehari-hari yang lebih optimal.','yes','yes','no','yes','2002070001','superuser','2020-08-24 06:30:22'),
('2008240013',15,'Starscream Transformers','570000','buah',200,'Transformers Generations Studio Series 05 Voyager Class Transformers Movie Starscream (NEW ARRIVAL !!!)\r\nFeatures:\r\n\r\n- Authentic and originally product by Hasbro Canada with Takara Tomy, Japan\r\n- Licensed from Paramount Pictures Corporation\r\n- Product dimension 3.2 x 7 x 9.5 inches\r\n- Manufacturer recommended age 8 years and up\r\n- Studio Series 06 Voyager Class Transformers Movie Starscream figure\r\n- Premium figure and packaging inspired by the iconic Mission City Battle scene\r\n- Figure scale reflects the character\'s size in the world of Transformers Movie.\r\n- Converts between robot and jet modes in 26 steps and comes with 1 accessory\r\n- Removable backdrop displays Starscream figure in Mission City Battle scene\r\n- Includes Transformers Studio Series 06 Voyager Class Transformers Movie Starscream figure, accessory, and instructions.\r\n- 2017 Hasbro. 2017 Paramount Pictures Corporation. All Rights Reserved.\r\n- Transformers and all related characters are trademarks of Hasbro.','yes','yes','no','yes','2002070001','superuser','2020-08-24 06:37:52'),
('2008240014',16,'Gitar Accoustic Brown','2500000','buah',400,'Gitar Akustik \r\n\r\nReady stok Gitar Akustik murah berkualitas sudah tanam besi ( trusrood dalam neck ) bisa distel lurus kalau bengkok dan settingan senar CEPER jadi sangat nyaman di pakai\r\n\r\nSpesifikasi : Body top meranti, Neck Mahogany ( tanam besi ), trusrood doble action ( bisa distel lurus kalau bengkok ), driyer stenlis ( bukan plastik ), senar 09\r\n\r\nKeunggulan Gitar ini\r\n\r\n? Suara Mantul\r\n? Jarak senar rendah tidak sakit di tangan CEPER\r\n? Ada besi dalam neck bisa distel kalau bengkok\r\n? Cocok untuk pemula dan rame2\r\n? Harga murah tp kualitas mantul\r\n\r\nPaket Gitar + Pick                             Rp 250.000\r\nPaket Gitar + Pick + Tas                   Rp 280.000\r\nPaket Gitar + Pick + Senar 1 set     Rp 299.000\r\n\r\nNB : \r\n1. COD HANYA UNTUK PULAU JAWA - BALI\r\n\r\n2. LUAR PULAU JAWA WAJIB PAKAI POS ( SUDAH PLUS BIAYA PAKING KAYU DAN BERGARANSI )\r\n\r\n3. KITA GARANSI FULL JADI KALAU DATANG ADA KENDALA TOLONG LAPOR KITA DULU BIAR KLAIM GARANSI BERLAKU','yes','no','no','yes','2002070001','superuser','2020-08-24 06:54:08');

/*Table structure for table `transaksi` */

DROP TABLE IF EXISTS `transaksi`;

CREATE TABLE `transaksi` (
  `transaksi_id` varchar(25) DEFAULT NULL,
  `user_id` varchar(10) DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL COMMENT 'hanya menjumlahkan harga * jumlah',
  `kode_unik` varchar(25) DEFAULT NULL COMMENT 'kode unik transfer',
  `transaksi_st` enum('dibeli','kirim_bukti','dibayar','dikirim','batal') DEFAULT 'dibeli',
  `mdb` varchar(10) DEFAULT NULL,
  `mdb_name` varchar(50) DEFAULT NULL,
  `mdd` datetime DEFAULT NULL,
  `tgl_batas_bayar` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `transaksi` */

/*Table structure for table `transaksi_produk` */

DROP TABLE IF EXISTS `transaksi_produk`;

CREATE TABLE `transaksi_produk` (
  `transaksi_produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaksi_id` varchar(25) DEFAULT NULL,
  `produk_id` varchar(25) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  UNIQUE KEY `transaksi_produk_transaksi_produk_id_KEY` (`transaksi_produk_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;

/*Data for the table `transaksi_produk` */

insert  into `transaksi_produk`(`transaksi_produk_id`,`transaksi_id`,`produk_id`,`jumlah`) values 
(66,'2002190345500001','2002190002',2),
(67,'2002190345500001','2002190006',1),
(68,'2008221305340002','2002190003',3),
(69,'2008221305340002','2002190007',2);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` char(10) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `provinsi` varchar(100) DEFAULT NULL,
  `kabupaten` varchar(100) DEFAULT NULL,
  `kecamatan` varchar(100) DEFAULT NULL,
  `alamat` text,
  `hp` varchar(25) DEFAULT NULL,
  `tempat_lahir` varchar(30) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jns_kelamin` enum('L','P') DEFAULT NULL,
  `agama` varchar(30) DEFAULT NULL,
  `nik` char(16) DEFAULT NULL,
  `kode_pos` varchar(25) DEFAULT NULL,
  `nama_bank` varchar(30) DEFAULT NULL,
  `norek` varchar(30) DEFAULT NULL,
  `ahli_waris` varchar(50) DEFAULT NULL,
  `hubungan` varchar(30) DEFAULT NULL,
  `no_ahli_waris` varchar(25) DEFAULT NULL,
  `hu` varchar(30) DEFAULT NULL,
  `sponsor` varchar(30) DEFAULT NULL,
  `user_img` text,
  `status_anggota` enum('aktif','nonaktif') DEFAULT 'nonaktif',
  `mdd_anggota` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `com_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`user_id`,`nama`,`provinsi`,`kabupaten`,`kecamatan`,`alamat`,`hp`,`tempat_lahir`,`tanggal_lahir`,`jns_kelamin`,`agama`,`nik`,`kode_pos`,`nama_bank`,`norek`,`ahli_waris`,`hubungan`,`no_ahli_waris`,`hu`,`sponsor`,`user_img`,`status_anggota`,`mdd_anggota`) values 
('2002070001','Superuser',NULL,NULL,NULL,'indramayu',NULL,NULL,NULL,'L',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'nonaktif',NULL),
('2002070002','operator',NULL,NULL,NULL,'indarmayu',NULL,NULL,NULL,'L',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'nonaktif',NULL),
('2008130001','latip',NULL,NULL,NULL,'karangsong',NULL,NULL,NULL,'L',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'nonaktif',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
