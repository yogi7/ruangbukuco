<?php

class M_prefrensi extends CI_Model {

    public function get_pref($where)
    {
        $this->db->select('*');
        $this->db->from('prefrensi');
        $this->db->where($where); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        }
        return array();
    }

    //insert
    public function insert($table, $params)
    {
        return $this->db->insert($table, $params); 
    }

    //update
    public function update($table, $params, $where)
    {
        $this->db->set($params);
        $this->db->where($where);
        return $this->db->update($table);
    }

    //delete
    public function delete($table ,$where)
    {
      $this->db->where($where);
      return $this->db->delete($table);
    }

}