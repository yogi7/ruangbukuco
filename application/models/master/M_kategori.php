<?php

class M_kategori extends CI_Model {

    //get all
    public function get_all($number,$offset, $params)
    {
        $this->db->select('*');
        $this->db->from('kategori');
        if (!empty($params)) {
          $this->db->like($params);
        }
        $this->db->limit($number, $offset); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          $result = $query->result_array();
          $query->free_result();
          return $result;
        }
        return array();
    }

    //get all
    public function get_all_nf()
    {
        $this->db->select('*');
        $this->db->from('kategori'); 
        $this->db->order_by('kategori_nama', 'ASC'); 
        $query = $this->db->get();
        // echo "<pre>";echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
          $result = $query->result_array();
          $query->free_result();
          return $result;
        }
        return array();
    }


    //get by id
    public function get_by_id($kategori_id)
    {
        $this->db->select('*');
        $this->db->from('kategori');
        $this->db->where('kategori_id', $kategori_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          $result = $query->row_array();
          $query->free_result();
          return $result;
        }
        return array();
    }

    //count all
    public function count_all()
    {
        $this->db->select('*');
        $this->db->from('kategori');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          $result = $query->num_rows();
          return $result;
        }
        return 0;
    }

    //insert
    public function insert($table ,$params)
    {
      return $this->db->insert($table, $params);
    }

    //delete
    public function delete($table ,$where)
    {
      $this->db->where($where);
      return $this->db->delete($table);
    }

    //update
    public function update($table, $params, $where)
    {
      $this->db->set($params);
      $this->db->where($where);
      return $this->db->update($table);
    }
  
    
}