<?php

class M_produk extends CI_Model {

    //generate id terakhir
    public function generate_id($prefixdate)
    {
        $sql = "SELECT RIGHT(produk_id, 4)'last_number'
                FROM produk
                ORDER BY produk_id DESC
                LIMIT 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            // create next number
            $number = intval($result['last_number']) + 1;
            if ($number > 9999) {
                return false;
            }
            $zero = '';
            for ($i = strlen($number); $i < 4; $i++) {
                $zero .= '0';
            }
            return $prefixdate . $zero . $number;
        } else {
            // create new number
            return $prefixdate . '0001';
        }
    }

    //get all
    public function get_all($number,$offset, $params)
    {
        $this->db->select('a.*, b.kategori_nama');
        $this->db->from('produk as a');
        $this->db->join('kategori as b', 'b.kategori_id = a.kategori_id');
        if (!empty($params)) {
          $this->db->like($params);
        }
        $this->db->limit($number, $offset); 
        $query = $this->db->get();
        // echo "<pre>";echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
          $result = $query->result_array();
          $query->free_result();
          return $result;
        }
        return array();
    }

     //get all aktif
     public function get_all_aktif()
     {
         $this->db->select('a.*');
         $this->db->from('produk as a');
         $this->db->where('produk_eco_st', 'yes'); 
         $this->db->group_by('a.produk_id');
         $query = $this->db->get();
         if ($query->num_rows() > 0) {
           $result = $query->result_array();
           $query->free_result();
           return $result;
         }
         return array();
     }

    //get all rekomendasi
    public function get_all_rekomendasi()
    {
        $this->db->select('a.*, b.kategori_nama, c.gambar_nama');
        $this->db->from('produk as a');
        $this->db->join('kategori as b', 'b.kategori_id = a.kategori_id');
        $this->db->join('gambar_produk as c', 'c.produk_id = a.produk_id');
        $this->db->where('rekomendasi_st', 'yes'); 
        $this->db->group_by('a.produk_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          $result = $query->result_array();
          $query->free_result();
          return $result;
        }
        return array();
    }

    //get all
    public function get_all_nf()
    {
        $this->db->select('a.*, b.kategori_nama');
        $this->db->from('produk as a');
        $this->db->join('kategori as b', 'b.kategori_id = a.kategori_id');
        $query = $this->db->get();
        // echo "<pre>";echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
          $result = $query->result_array();
          $query->free_result();
          return $result;
        }
        return array();
    }

    //get all
    public function get_all_active()
    {
        $this->db->select('a.*, b.kategori_nama');
        $this->db->from('produk as a');
        $this->db->join('kategori as b', 'b.kategori_id = a.kategori_id');
        $this->db->where('a.active_st', 'yes');
        $query = $this->db->get();
        // echo "<pre>";echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
          $result = $query->result_array();
          $query->free_result();
          return $result;
        }
        return array();
    } 
    
    //get max price
    public function get_all_max_price()
    {
        $this->db->select('MAX(harga) as max');
        $this->db->from('produk as a');
        $this->db->where('a.active_st', 'yes');
        $query = $this->db->get();
        // echo "<pre>";echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
          $result = $query->row_array();
          $query->free_result();
          return $result;
        }
        return array();
    } 

    //get all
    public function get_produk_by_search($params_search)
    {

        $this->db->select('a.*, b.kategori_nama');
        $this->db->from('produk as a');
        $this->db->join('kategori as b', 'b.kategori_id = a.kategori_id');
        $this->db->where('a.active_st', 'yes');
        if (!empty($params_search['kategori_id'])) {
          $this->db->where('b.kategori_id',$params_search['kategori_id']);
        }
        if (!empty($params_search['nama'])) {
          $this->db->like('a.nama',$params_search['nama'],'both');
        }
        // if (!empty($params_search['max_harga'])) {
        //   $this->db->where('a.harga BETWEEN "'. $params_search['min_harga']. '" and "'. $params_search['max_harga'].'"');
        // }
        $this->db->order_by($params_search['nama_field'], $params_search['isi_field']);
        $query = $this->db->get();
        // echo "<pre>";echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
          $result = $query->result_array();
          $query->free_result();
          return $result;
        }
        return array();
    }

    //get gambar
    public function get_gambar_by_produk_rekomendasi($produk_id)
    {
        $this->db->select('a.*');
        $this->db->from('gambar_produk as a');
        $this->db->join('produk as b', 'b.produk_id = a.produk_id');
        $this->db->where('rekomendasi_st', 'yes'); 
        $this->db->where('a.produk_id', $produk_id); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          $result = $query->row_array();
          $query->free_result();
          return $result;
        }
        return 0;
    }

    //get gambar
    public function get_gambar_by_produk($produk_id)
    {
        $this->db->select('*');
        $this->db->from('gambar_produk');
        $this->db->where('produk_id', $produk_id); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          $result = $query->row_array();
          $query->free_result();
          return $result;
        }
        return 0;
    }

    //get all gambar
    public function get_all_gambar_by_produk($produk_id)
    {
        $this->db->select('*');
        $this->db->from('gambar_produk');
        $this->db->where('produk_id', $produk_id); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          $result = $query->result_array();
          $query->free_result();
          return $result;
        }
        return 0;
    }

    //get by produk
    public function get_by_id($produk_id)
    {
        $this->db->select('a.*, b.kategori_nama');
        $this->db->from('produk as a');
        $this->db->join('kategori as b', 'b.kategori_id = a.kategori_id');
        $this->db->where('produk_id', $produk_id); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          $result = $query->row_array();
          $query->free_result();
          return $result;
        }
        return 0;
    }

    //get by produk harga
    public function get_harga_by_id($produk_id)
    {
        $this->db->select('a.harga as harga');
        $this->db->from('produk as a');
        $this->db->join('kategori as b', 'b.kategori_id = a.kategori_id');
        $this->db->where('produk_id', $produk_id); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          $result = $query->row_array();
          $query->free_result();
          return $result['harga'];
        }
        return 0;
    }

    //get by produk
    public function get_by_id_favorit($params)
    {
        $this->db->select('a.*, b.kategori_nama');
        $this->db->from('produk as a');
        $this->db->join('kategori as b', 'b.kategori_id = a.kategori_id');
        $this->db->join('favorit as c', 'c.produk_id = a.produk_id');
        if (!empty($params['produk_nama'])) {
          $this->db->like('a.nama',$params['produk_nama'],'both');
        }
        if (!empty($params['kategori_id'])) {
          $this->db->where('b.kategori_id',$params['kategori_id']);
        }
        $this->db->where('c.user_id', $params['user_id']); 
        $query = $this->db->get();
        // echo "<pre>";echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
          $result = $query->result_array();
          $query->free_result();
          return $result;
        }
        return 0;
    }

    //count all
    public function count_all()
    {
        $this->db->select('*');
        $this->db->from('produk');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          $result = $query->num_rows();
          return $result;
        }
        return 0;
    }

    //cek email
    public function is_exist_email($params)
    {
        $query = $this->db->get_where('com_user', array('user_mail' => $params));
        if ($query->num_rows() > 0) {
          $result = $query->row_array();
          $query->free_result();
          return $result;
        }
        return 0;
    }

    //cek username
    public function is_exist_produk($params)
    {
        $query = $this->db->get_where('produk', array('produk_id' => $params));
        if ($query->num_rows() > 0) {
          $result = $query->row_array();
          $query->free_result();
          return $result;
        }
        return 0;
    }

    //insert
    public function insert($table, $params)
    {
        return $this->db->insert($table, $params);
        $this->session->unset_userdata('produk'); 
    }

    //delete
    public function delete($table ,$where)
    {
      $this->db->where($where);
      return $this->db->delete($table);
    }

    //update
    public function update($table, $params, $where)
    {
      $this->db->set($params);
      $this->db->where($where);
      return $this->db->update($table);
    }
  
    
}