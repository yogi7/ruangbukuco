<?php

class M_favorit extends CI_Model {
    
    //count all
    public function count_by_id($user_id)
    {
        $this->db->select('*');
        $this->db->from('favorit');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          $result = $query->num_rows();
          return $result;
        }
        return 0;
    }

    //get all by id
    public function get_all_by_id($user_id)
    {
        $this->db->select('*');
        $this->db->from('favorit');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        // echo "<pre>";echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
          $result = $query->result_array();
          $query->free_result();
          return $result;
        }
        return array();
    }
   
    //cek produk
    public function is_exist_produk($where)
    {
        $this->db->select('*');
        $this->db->from('favorit');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          $result = $query->num_rows();
          return $result;
        }
        return 0;
    }

    //insert
    public function insert($table, $params)
    {
        return $this->db->insert($table, $params);
    }
    
    //update
    public function update($table, $params, $where)
    {
      $this->db->set($params);
      $this->db->where($where);
      return $this->db->update($table);
    }

    //delete
    public function delete($table ,$where)
    {
      $this->db->where($where);
      return $this->db->delete($table);
    }
  
}