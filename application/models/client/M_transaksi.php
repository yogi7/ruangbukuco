<?php

class M_transaksi extends CI_Model {
    
    //generate id terakhir
    public function generate_id($prefixdate)
    {
        $sql = "SELECT RIGHT(transaksi_id, 4)'last_number'
                FROM transaksi
                ORDER BY transaksi_id DESC
                LIMIT 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            // create next number
            $number = intval($result['last_number']) + 1;
            if ($number > 9999) {
                return false;
            }
            $zero = '';
            for ($i = strlen($number); $i < 4; $i++) {
                $zero .= '0';
            }
            return $prefixdate . $zero . $number;
        } else {
            // create new number
            return $prefixdate . '0001';
        }
    }

    //get all transaksi
    public function get_all_transaksi()
    {
        $this->db->select('a.*,b.produk_id,c.biaya_ongkir,d.nama');
        $this->db->from('transaksi as a');
        $this->db->join('transaksi_produk as b', 'on a.transaksi_id = b.transaksi_id', 'inner');
        $this->db->join('info_kurir as c', 'on a.transaksi_id = c.transaksi_id', 'inner');
        $this->db->join('user as d', 'on a.user_id = d.user_id', 'inner');
        $this->db->where('a.transaksi_st', 'dibeli');
        $this->db->group_by('transaksi_id'); 
        $this->db->order_by('mdd DESC'); 
        $query = $this->db->get();
    //    echo $this->db->last_query();die;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        }
        return array();
    }
    
    //get by user_id
    public function get_transaksi_by_user_id($where)
    {
        $this->db->select('a.*');
        $this->db->from('transaksi as a');
        $this->db->where('user_id', $where); 
        $this->db->order_by('mdd DESC'); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        }
        return array();
    }

    //get by bukti
    public function get_all_bukti()
    {
        $this->db->select('a.nama_file, a.transaksi_id, a.nama_rek, a.no_rek_pentransfer,b.subtotal, b.kode_unik, c.biaya_ongkir, b.mdd, d.nama');
        $this->db->from('bukti_transfer as a');
        $this->db->join('transaksi as b', 'on a.transaksi_id = b.transaksi_id', 'inner');
        $this->db->join('info_kurir as c', 'on c.transaksi_id = b.transaksi_id', 'inner');
        $this->db->join('user as d', 'on b.user_id = d.user_id', 'inner');
        $this->db->where('b.transaksi_st', 'kirim_bukti');
        $this->db->order_by('b.mdd ASC'); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        }
        return NULL;
    }

    //get by all pembelian untuk resi
    public function get_all_pembelian()
    {
        $this->db->select('a.*, d.nama, c.biaya_ongkir');
        $this->db->from('transaksi as a');
        $this->db->join('info_kurir as c', 'on c.transaksi_id = a.transaksi_id', 'inner');
        $this->db->join('user as d', 'on a.user_id = d.user_id', 'inner');
        $this->db->where("(a.transaksi_st = 'dibayar' OR a.transaksi_st = 'dikirim')");
        // $this->db->where('a.transaksi_st', 'dikirim');
        $this->db->order_by('a.transaksi_st ASC'); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        }
        return NULL;
    }

    //get bukti by transaksi
    public function get_bukti_by_transaksi($where)
    {
        $this->db->select('*');
        $this->db->from('bukti_transfer');
        $this->db->where('transaksi_id', $where); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        }
        return array();
    }


    //get by user_id
    public function get_by_user_transaksi_id($where)
    {
        $this->db->select('a.*, c.*');
        $this->db->from('transaksi as a');
        $this->db->join('transaksi_produk as b', 'b.transaksi_id = a.transaksi_id');
        $this->db->join('gambar_produk as c', 'c.produk_id = b.produk_id');
        $this->db->where($where); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        }
        return array();
    }

    //get by id
    public function get_by_transaksi_id($where)
    {
        $this->db->select('a.*');
        $this->db->from('transaksi as a');
        $this->db->where($where); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        }
        return array();
    }
    

    //get gambar
    public function get_gambar_by_produk($produk_id)
    {
        $this->db->select('*');
        $this->db->from('gambar_produk');
        $this->db->where('produk_id', $produk_id); 
        $this->db->group_by('gambar_id'); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        }
        return 0;
    }

    //get produk
    public function get_produk_by_transaksi($transaksi_id)
    {
        $this->db->select('b.*, a.jumlah');
        $this->db->from('transaksi_produk as a');
        $this->db->join('produk as b', 'on b.produk_id = a.produk_id', 'inner');
        $this->db->where('a.transaksi_id', $transaksi_id); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        }
        return array();
    }

    //get produk
    public function get_alamat_by_transaksi($transaksi_id)
    {
        $this->db->select('*');
        $this->db->from('alamat_transaksi');
        $this->db->where('transaksi_id', $transaksi_id); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        }
        return array();
    }

    //get produk
    public function get_kurir_by_transaksi($transaksi_id)
    {
        $this->db->select('*');
        $this->db->from('info_kurir');
        $this->db->where('transaksi_id', $transaksi_id); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        }
        return array();
    }
    

    public function count_belum_bayar_by_id($where)
    {
        $this->db->select('a.*');
        $this->db->from('transaksi as a');
        $this->db->where('a.user_id', $where);
        $this->db->where('a.transaksi_st', 'dibeli');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          $result = $query->num_rows();
          return $result;
        }
        return 0;
    }

    //get by user_id
    public function get_pref($where)
    {
        $this->db->select('*');
        $this->db->from('prefrensi');
        $this->db->where($where); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        }
        return array();
    }

    //get by user_id
    public function get_by_id($transaksi_id)
    {
        $this->db->select('a.*, b.biaya_ongkir');
        $this->db->from('transaksi as a');
        $this->db->join('info_kurir as b', 'b.transaksi_id = a.transaksi_id');
        $this->db->where('a.transaksi_id', $transaksi_id); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        }
        return array();
    }

    //cek username
    public function is_exist_kode_unik($where)
    {
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->where($where); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        }
        return 0;
    }

    //insert
    public function insert($table, $params)
    {
        return $this->db->insert($table, $params); 
    }

    //update
    public function update($table, $params, $where)
    {
        $this->db->set($params);
        $this->db->where($where);
        return $this->db->update($table);
    }

    //delete
    public function delete($table ,$where)
    {
      $this->db->where($where);
      return $this->db->delete($table);
    }

}