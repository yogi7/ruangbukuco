<?php

class M_profile extends CI_Model {
    

   //get data profile
   public function get_data_profile($id)
   {
       $this->db->select('user.*,com_user.*');
       $this->db->from('user');
       $this->db->join('com_user', 'user.user_id = com_user.user_id', 'left');
       $this->db->where('com_user.user_id', $id);
       $query = $this->db->get();
       if ($query->num_rows() > 0) {
         $result = $query->row_array();
         $query->free_result();
         return $result;
       }
       return array();
   }

    //insert
    public function insert($table, $params)
    {
        return $this->db->insert($table, $params);
    }
    
    //update
    public function update($table, $params, $where)
    {
      $this->db->set($params);
      $this->db->where($where);
      return $this->db->update($table);
    }

    //delete
    public function delete($table ,$where)
    {
      $this->db->where($where);
      return $this->db->delete($table);
    }
  
}