<?php

class M_cart extends CI_Model {
    
    //count all
    public function count_by_id($user_id)
    {
        $this->db->select('a.*');
        $this->db->from('cart as a');
        $this->db->join('produk as b', 'b.produk_id = a.produk_id');
        $this->db->where('a.user_id', $user_id);
        $this->db->where('a.transaksi_st', 'cart');
        $this->db->where('b.stok_st', 'yes');
        $this->db->where('b.active_st', 'yes');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          $result = $query->num_rows();
          return $result;
        }
        return 0;
    }

    //get by user_id
    public function get_by_user_id($where)
    {
        $this->db->select('a.jumlah, a.nil_bayar, b.*, c.kategori_nama');
        $this->db->from('cart as a');
        $this->db->join('produk as b', 'b.produk_id = a.produk_id');
        $this->db->join('kategori as c', 'c.kategori_id = b.kategori_id');
        $this->db->where($where); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          $result = $query->result_array();
          $query->free_result();
          return $result;
        }
        return array();
    }

    //cek produk
    public function is_exist_produk($where)
    {
        $this->db->select('*');
        $this->db->from('cart');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          $result = $query->num_rows();
          return $result;
        }
        return 0;
    }

    //update
    public function update($table, $params, $where)
    {
      $this->db->set($params);
      $this->db->where($where);
      return $this->db->update($table);
    }

}