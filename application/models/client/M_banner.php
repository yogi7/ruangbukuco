<?php

class M_banner extends CI_Model {
    //update
    public function update($table, $params, $where)
    {
      $this->db->set($params);
      $this->db->where($where);
      return $this->db->update($table);
    }
  
    //get main banner
    public function get_main_banner()
    {
        $this->db->select('*');
        $this->db->from('banner');
        $this->db->where('jenis', 'main-banner');
        $query = $this->db->get();
        // echo "<pre>";echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
          $result = $query->result_array();
          $query->free_result();
          return $result;
        }
        return 0;
    }

     //get main banner
     public function get_banner_byid($banner_id)
     {
         $this->db->select('*');
         $this->db->from('banner');
         $this->db->where('banner_id', $banner_id);
         $query = $this->db->get();
         // echo "<pre>";echo $this->db->last_query();exit;
         if ($query->num_rows() > 0) {
           $result = $query->row_array();
           $query->free_result();
           return $result;
         }
         return 0;
     }

    public function get_last_date(){
      $sql = "SELECT mdd FROM banner ORDER BY mdd DESC
      ";
      //execute query
      $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
          $result = $query->row_array();
          $query->free_result();
          return $result;
        } else {
          return array();
        }
  }

    //insert
    public function insert($table, $params)
    {
        return $this->db->insert($table, $params);
        $this->session->unset_userdata('banner'); 
    }

    //delete
    public function delete($table ,$where)
    {
      $this->db->where($where);
      return $this->db->delete($table);
    }
}