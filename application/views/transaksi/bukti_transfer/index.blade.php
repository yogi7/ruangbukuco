<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{ $title }}
            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Transaksi</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">{{ $title }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<div class="container-fluid">
{{-- notif wajib ada di setiap halaman admin kecuali delete--}}
    @include('template/notif')
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card"> 
                <div class="card-body">
                    <div class="row">
                        <div class="row">
                            <h4 class="card-title col-lg-12">Daftar Bukti Transfer</h4>
                            <br>
                            <i style="margin-left:2%">
                                <h6>(*klik pada foto untuk memperbesar)</h6>
                            </i>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th class="text-align text-center" width="10%">No.</th>
                                    <th class="text-align text-center" width="15%">Foto bukti</th>
                                    <th class="text-align text-center" width="15%">Pembeli</th>
                                    <th class="text-align text-center" width="15%">No. Rekening</th>
                                    <th class="text-align text-center" width="15%">Nama Rekening</th>
                                    <th class="text-align text-center" width="15%">Tanggal Transaksi
                                    </th>
                                    <th class="text-align text-center" width="25%">Total</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($result))
                                <?php $nomor = 1;?>
                                     @foreach ($result as $rs)
                                    <tr>
                                        <th class="text-align text-center"> {{ $no++ }} </th>
                                        <th class="text-align text-center"><img width="100"
                                                height="100"
                                                src="{{ base_url('assets/images/bukti_transfer/').$rs['nama_file'] }}"
                                                data-toggle="modal"
                                                data-id="{{ base_url('assets/images/bukti_transfer/').$rs['nama_file'] }}"
                                                class="open-modal" href="#modal" />
                                        </th>
                                        <th class="text-align"> {{ $rs['nama'] }}</th>
                                        <th class="text-align text-center"> {{ $rs['no_rek_pentransfer'] }}</th>
                                        <th class="text-align"> {{ $rs['nama_rek'] }}</th>
                                        <td>
                                            @php $date=date_create($rs['mdd']); echo
                                            date_format($date, 'd F Y'); @endphp
                                            <br>
                                            @php echo substr($rs['mdd'], 11, 8); @endphp
                                        </td>
                                        <td class="text-align text-center">
                                            Rp.
                                            {{  number_format($rs['subtotal'] + $rs['kode_unik'] + $rs['biaya_ongkir']) }}
                                        </td>
                                        <td>
                                            <a data-toggle="modal"
                                                data-id="{{ site_url('transaksi/bukti_transfer/confirm_process/'.$rs['transaksi_id']) }}"
                                                href="#modal"
                                                class="btn btn-success btn-rounded m-b-10 m-l-5 open-modal-confirm"
                                                title="Verifiksi"><i class="ti-check"></i> </a>
                                            <a data-toggle="modal"
                                                data-id="{{ site_url('transaksi/bukti_transfer/reject_process/'.$rs['transaksi_id']) }}"
                                                href="#modal"
                                                class="btn btn-danger btn-rounded m-b-10 m-l-5 open-modal-reject"
                                                title="Verifiksi"><i class="ti-close"></i> </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <th class="text-align text-center" colspan="7"><h3> Data kosong </h3></th>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        @if (isset($pagination))
                                {!! $pagination !!}
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <!-- Modal -->
 <div class="modal fade" id="open-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Foto Bukti</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <center>
                            <img height="50%" width="50%" class="img_bukti" src="" alt="">
                        </center>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="open-modal-confirm" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Konfirmasi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <i class="ti-check btn btn-success"></i> &nbsp; <b>Verifikasi bahwa bukti sudah benar?</b>
                    </div>
                    <div class="modal-footer">
                        <a href="" class="konfirm btn btn-success">Konfirmasi</a>
                        <a href="#" class="btn btn-danger" data-dismiss="modal">Batal</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="open-modal-reject" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Konfirmasi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <i class="ti-close btn btn-danger"></i> &nbsp; <b>Tolak bukti pembayaran?</b>
                    </div>
                    <div class="modal-footer">
                        <a href="" class="konfirm btn btn-success">Konfirmasi</a>
                        <a href="#" class="btn btn-danger" data-dismiss="modal">Batal</a>
                    </div>
                </div>
            </div>
        </div>



        @push('ext_js')
        <script>
            $(document).on("click", ".open-modal", function () {
                var nama_gambar = $(this).data('id');
                // $(".modal-body #nama_gambar").val(nama_gambar);
                // document.getElementById("nm").innerHTML = nama_gambar;
                $(".img_bukti").attr("src", nama_gambar);
                // As pointed out in comments, 
                // it is unnecessary to have to manually call the modal.
                $('#open-modal').modal('show');
            });

            //confirm
            $(document).on("click", ".open-modal-confirm", function () {
                var id = $(this).data('id');
                $(".konfirm").attr("href", id);
                $('#open-modal-confirm').modal('show');
            });

            //reject
            $(document).on("click", ".open-modal-reject", function () {
                var id = $(this).data('id');
                $(".konfirm").attr("href", id);
                $('#open-modal-reject').modal('show');
            });
        </script>

        @endpush