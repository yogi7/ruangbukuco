<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{ $title }}
            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="" class="text-muted">Transaksi</a></li>
                        <li class="breadcrumb-item"><a href="{{ site_url('transaksi/resi') }}"
                                class="text-muted">{{ $title }}</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Detail</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="container-fluid">
    {{-- notif wajib ada di setiap halaman admin kecuali delete--}}
    @include('template/notif')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-10">
                            <h4 class="card-title">Detail</h4>
                        </div>
                        <div class="col-lg-2">
                            <div class="text-right">
                                <a href="{{ site_url('transaksi/resi') }}" type="submit"
                                    class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                    </div>
                    <div class="horizontal-form-elements">
                        <div class="row">
                            <div class="col-lg-7">
                            <form action="{{ site_url('transaksi/resi/update_resi_process') }}"
                                method="post">
                                <input type="hidden" name="transaksi_id"
                                    value="{{ $result['transaksi_id'] }}">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-8">
                                            <input type="text" name="resi" class="form-control"
                                                placeholder="Isi kode resi..."
                                                value="{{ $result['resi'] }}">
                                            </div>
                                            <div class="col-lg-4">
                                            <button id="button"
                                                style="margin-top:2%; margin-right:2%"
                                                type="submit"
                                                class="btn btn-success btn-rounded m-b-10 m-l-5"><i
                                                    class="ti-check"></i>
                                                Simpan</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-12">
                                            @if(empty($result['resi']))
                                            <label class="control-label">-</label>
                                            @else
                                            <!-- seukses get resi -->
                                            @if($status_resi == '')
                                            <div class="alert alert-danger" role="alert"
                                                style="margin-bottom:5%">
                                                <strong> Nomor resi yang diinputkan salah
                                                    !</strong>
                                            </div>
                                            @elseif($status_resi['code'] == 200)
                                            <b>Status pengiriman</b>
                                            <table class="table table-responsive">
                                                <thead>
                                                    <tr>
                                                        <th class="text-align text-center"
                                                            width="50%">Status</th>
                                                        <th class="text-align text-center"
                                                            width="25%">Penerima</th>
                                                        <th class="text-align text-center"
                                                            width="25%">Tanggal</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-align text-center">
                                                            @if($track['delivery_status']['status']
                                                            == 'DELIVERED')
                                                            <span
                                                                class="badge badge-success">Terkirim</span>
                                                            @else
                                                            <span
                                                                class="badge badge-danger">Tidak
                                                                dikirim</span>
                                                            @endif
                                                        </td>
                                                        <td class="text-align text-center">
                                                            {!!
                                                            $track['delivery_status']['pod_receiver']
                                                            !!}
                                                        </td>
                                                        <td class="text-align text-center">
                                                            {!!
                                                            $track['delivery_status']['pod_date']
                                                            !!} {!!
                                                            $track['delivery_status']['pod_time']
                                                            !!}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            @php $jumlah = count($track['manifest']) @endphp
                                            <b>Riwayat Pengiriman</b>
                                            <table class="table table-responsive">
                                                <thead>
                                                    <tr>
                                                        <th class="text-align text-center"
                                                            width="5%">No.</th>
                                                        <th class="text-align text-center"
                                                            width="25%">Tanggal</th>
                                                        <th class="text-align text-center"
                                                            width="20%">Kota</th>
                                                        <th class="text-align text-center"
                                                            width="50%">Keterangan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @for ($i = 0; $i < $jumlah; $i++) <tr>
                                                        <th class="text-align text-center"
                                                            width="5%"> {{ $i+1 }} </th>
                                                        <th class="text-align">
                                                            {!!
                                                            $track['manifest'][$i]['manifest_date']
                                                            !!}<br>
                                                            {!!
                                                            $track['manifest'][$i]['manifest_time']
                                                            !!}
                                                        </th>
                                                        <th class="text-align">{!!
                                                            $track['manifest'][$i]['city_name']
                                                            !!}</th>
                                                        <th class="text-align">{!!
                                                            $track['manifest'][$i]['manifest_description']
                                                            !!}</th>
                                                        </tr>
                                                        @endfor
                                                </tbody>
                                            </table>
                                            @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </form>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label class="control-label"><b> Nama Kurir
                                            </b></label>
                                    </div>
                                    <div class="col-lg-12">
                                        <label
                                            class="control-label">{{ $result['nama_kurir'] }}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label class="control-label"><b> Jenis
                                                Service</b></label>
                                    </div>
                                    <div class="col-lg-12">
                                        <label
                                            class="control-label">{{ $result['service'] }}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label class="control-label"><b>
                                                Deskripsi</b></label>
                                    </div>
                                    <div class="col-lg-12">
                                        <label
                                            class="control-label">{{ $result['desk_kurir'] }}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label class="control-label"><b> Waktu Perkiraan
                                            </b></label>
                                    </div>
                                    <div class="col-lg-12">
                                        <label
                                            class="control-label">{{ $result['wkt_perkiraan'] }}
                                            hari</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label class="control-label"><b> Biaya Ongkos Kirim
                                            </b></label>
                                    </div>
                                    <div class="col-lg-12">
                                        <label class="control-label">Rp.
                                            {{ number_format($result['biaya_ongkir']) }}</label>
                                    </div>
                                </div>
                            </div><!-- /# column -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>