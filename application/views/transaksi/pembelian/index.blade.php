<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{ $title }}
            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Transaksi</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">{{ $title }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<div class="container-fluid">
{{-- notif wajib ada di setiap halaman admin kecuali delete--}}
    @include('template/notif')
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card"> 
                <div class="card-body">
                    <div class="row">
                        <div class="row">
                            <h4 class="card-title col-lg-12">Daftar Pembelian</h4>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th>No</th>
                                    <th>Pembelian</th>
                                    <th>Nama Pembeli</th>
                                    <th>Jumlah Bayar</th>
                                    <th>Batas Bayar</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($result))
                                <?php $nomor = 1;?>
                                    @foreach($result as $key => $rs)
                                    <tr>
                                        <th scope="row">{{$nomor++}}</th>
                                        <td>
                                            <img height="130" width="150"
                                            src="{{ base_url('assets/images/gambar_produk/'.$gambar[$key]['gambar_nama']) }}" /><br>
                                            <b>{{$rs['transaksi_id']}}</b><br>
                                        </td>
                                        <td>{{$rs['nama']}}</td>
                                        <td>Rp. {{ number_format($rs['subtotal'] + $rs['kode_unik'] + $rs['biaya_ongkir']) }}</td>
                                        <td> 
                                            @php $date=date_create($rs['tgl_batas_bayar']); echo date_format($date, 'd F Y');
                                            @endphp @php echo substr($rs['tgl_batas_bayar'], 11, 8); @endphp
                                        </td>
                                        <td style="text-align:left">
                                            @if($rs['transaksi_st'] == 'dibeli')
                                            <span class="badge badge-pill badge-warning">Belum Dibayar</span>
                                            @elseif($rs['transaksi_st'] == 'kirim_bukti')
                                                <span class="badge badge-pill badge-info">Menunggu konfirmasi bukti</span>
                                            @elseif($rs['transaksi_st'] == 'dibayar')
                                                <span class="badge badge-pill badge-success">Telah dibayar</span>
                                            @else
                                                <span class="badge badge-pill badge-danger">Dibatalkan</span>
                                            @endif 
                                        </td>
                                        <td>
                                            <a href="{{ site_url('transaksi/pembelian/detail/'.$rs['transaksi_id']) }}" type="button" class="btn btn-info btn-rounded m-b-10 m-l-5" title="Detail"><i class="ti-eye"></i> </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <th class="text-align text-center" colspan="7"><h3> Data kosong </h3></th>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        @if (isset($pagination))
                                {!! $pagination !!}
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>