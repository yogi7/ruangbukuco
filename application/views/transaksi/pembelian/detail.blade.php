<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{ $title }}
            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="" class="text-muted">Transaksi</a></li>
                        <li class="breadcrumb-item"><a href="{{ site_url('transaksi/pembelian') }}"
                                class="text-muted">{{ $title }}</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Detail</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="container-fluid">
    {{-- notif wajib ada di setiap halaman admin kecuali delete--}}
    @include('template/notif')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-10">
                            <h4 class="card-title">Detail</h4>
                        </div>
                        <div class="col-lg-2">
                            <div class="text-right">
                                <a href="{{ site_url('transaksi/pembelian') }}" type="submit"
                                    class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                    </div>
                    <nav class="navbar navbar-expand-lg navbar-dark bg-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item active">
                                    <a class="nav-link btn btn-primary" href="{{ site_url('transaksi/pembelian/detail/') }}{{$result['transaksi_id']}}">Bukti Transfer<span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn btn-primary" href="{{ site_url('transaksi/pembelian/detail_produk/') }}{{$result['transaksi_id']}}">Produk</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn btn-primary" href="{{ site_url('transaksi/pembelian/detail_tujuan/') }}{{$result['transaksi_id']}}">Tujuan Pengririman</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn btn-primary" href="{{ site_url('transaksi/pembelian/detail_kurir/') }}{{$result['transaksi_id']}}">Kurir Pengiriman</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="row">
                        <div class="col-lg-12 card-body">
                        @if(!empty($bukti_transfer))
                        <div  style="margin-top:1%; margin-bottom:1%">
                            <h3>Bukti Transfer</h3>
                            <img height="600" width="600" src="{{ base_url('assets/images/bukti_transfer/'.$bukti_transfer['nama_file']) }}" />   
                        </div>
                        @else
                        <div  style="margin-top:1%; margin-bottom:1%">
                            <b>Pembeli belum mengirimkan bukti pembayaran ! </b>
                        </div>
                        @endif
                        </div><!-- /# column -->
					</div>
                    

                </div>
            </div>
        </div>
    </div>
</div>