<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h3 class="page-title text-truncate text-dark font-weight-medium mb-1">{{ $title }}</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html">Edit rekening</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    {{-- notif --}}
    @include('template/notif')
    <div class="card-group">
        <div class="card">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <h3 class="text-dark mb-1 font-weight-medium"> Rekening </h3>
                    </div>
                </div>
                <hr>
                <center>
                    <img class="w-30" style="height: 300px" src="{{ base_url('assets/images/prefrensi/'.$rekening['img_name']) }}">
                </center>
                <h3 style="margin-top:2%" class="text-dark mb-1 font-weight-medium"> Edit rekening </h3>
                    <form action="{{ site_url('setclient/rekening/edit_process') }}" method="post"
                    enctype="multipart/form-data">
                    <div class="card-body">
                        <div class="basic-form" style="margin-top:20px">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Pilih Logo bank<sup style="color:red">*</sup>
                                </label>
                                <input type="file" name="files" class="form-control">
                            </div>
                        </div>
                        <div class="basic-form" style="margin-top:20px">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nama Bank<sup style="color:red">*</sup>
                                </label>
                                <input type="text" name="nama_bank" value="{{ $rekening['value_pref_2'] }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nama di Rekening (Atas Nama)<sup style="color:red">*</sup>
                                </label>
                                <input type="text" name="keterangan" value="{{ $rekening['keterangan'] }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Kode Bank (Contoh: mandiri -> 008)<sup style="color:red">*</sup>
                                </label>
                                <input type="number" name="kode" value="{{ $rekening['kode'] }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nomor Rekening<sup style="color:red">*</sup>
                                </label>
                                <input type="number" name="value_pref" value="{{ $rekening['value_pref'] }}" class="form-control">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="col-lg-12">
                        <div class="text-right">
                            <button type="submit" class="btn btn-success m-b-10 m-l-5"> Simpan</button>
                            <button type="reset" class="btn btn-secondary m-b-10 m-l-5"> Reset</button>
                        </div>
                    </div>
                    </form>
            </div>
        </div>
    </div>