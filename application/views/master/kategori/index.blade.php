<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{ $title }}
            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Master Data</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">{{ $title }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<div class="container-fluid">
    {{-- notif wajib ada di setiap halaman admin kecuali delete--}}
    @include('template/notif')
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card"> 
                <div class="card-body">
                    <div class="row">
                        <div class="row">
                            <h4 class="card-title col-lg-12">Daftar Kategori</h4>
                        </div>
                        <form class="col-lg-12 row" action="{{ site_url('master/user/search_process') }}" method="POST">
                            <div class="col-lg-3">
                                <input type="text" name="kategori_nama" class="form-control" placeholder="Isian nama kategori..." value="{{ $search['nama'] }}">
                            </div>
                            <div class="col-lg-3">
                                <button type="submit" name="search" value="tampilkan" class="btn btn-info">Cari</button>
                                <button type="submit" name="search" value="reset"
                                    class="btn btn-secondary">Reset</button>
                            </div>
                        </form>
                        <div class="text-right col-lg-12">
                            <a href="{{ site_url('master/kategori/add') }}"  type="submit" class="btn btn-primary">Tambah Data</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th class="text-align text-center" width="10%">No.</th>
                                    <th class="text-align text-center" width="25%">Logo Kategori</th>
                                    <th class="text-align text-center" width="25%">Nama Kategori</th>
                                    <th class="text-align text-center" width="25%">Status</th>
                                <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($result))
                                    @foreach ($result as $rs)
                                        <tr>
                                            <th class="text-align text-center" width="10%"> {{ $no++ }} </th>
                                            <th class="text-align text-center" width="25%"><img width="100"
                                                        height="100"
                                                        src="{{ base_url('assets/images/logo_kategori/').$rs['logo'] }}" />
                                                </th>
                                            <td width="25%">{{ $rs['kategori_nama'] }}</td>
                                            <td class="text-align text-center" width="25%">
                                                @if ($rs['kategori_st'] == 'yes')
                                                <span class="badge badge-success">aktif</span> 
                                                @else
                                                <span class="badge badge-danger">tidak aktif</span>  
                                                @endif
                                            </td>
                                            <td width="10%">
                                                    <a href="{{ site_url('master/kategori/edit/'.$rs['kategori_id']) }}" class="btn btn-success btn-rounded m-b-10 m-l-5" title="Edit"><i class="ti-pencil"></i></a>
                                                    <a href="{{ site_url('master/kategori/delete/'.$rs['kategori_id']) }}" class="btn btn-danger btn-rounded m-b-10 m-l-5" title="Delete"><i class="ti-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <th class="text-align text-center" colspan="7"><h3> Data kosong </h3></th>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <div class="text-right">
                            @if (isset($pagination))
                            {!! $pagination !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>