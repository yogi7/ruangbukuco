<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{ $title }}
            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Master Data</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">{{ $title }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<div class="container-fluid">
{{-- notif wajib ada di setiap halaman admin kecuali delete--}}
    @include('template/notif')
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card"> 
                <div class="card-body">
                    <div class="row">
                        <div class="row">
                            <h4 class="card-title col-lg-12">Daftar Produk</h4>
                        </div>
                        <form class="col-lg-12 row" action="{{ site_url('master/produk/search_process') }}" method="POST">
                            <div class="col-lg-3">
                                <select name="kategori_nama" id="single" class="form-control select2-single">
                                    <option value="%">Kategori....</option>
                                    @foreach ($kategoris as $kategori)
                                    <option value="{{ $kategori['kategori_nama'] }}">
                                        {{ $kategori['kategori_nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <input type="text" name="nama" class="form-control" placeholder="Isian nama produk..." value="{{ $search['a.nama'] }}">
                            </div>
                            <div class="col-lg-3">
                                <button type="submit" name="search" value="tampilkan" class="btn btn-info">Cari</button>
                                <button type="submit" name="search" value="reset"
                                    class="btn btn-secondary">Reset</button>
                            </div>
                        </form>
                        <div class="text-right col-lg-12">
                            <a href="{{ site_url('master/produk/add') }}" type="submit" class="btn btn-primary">Tambah Data</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th class="text-align text-center" width="5%">No.</th>
                                    <th class="text-align text-center" width="15%">Gambar</th>
                                    <th class="text-align text-center" width="20%">Produk</th>
                                    <th class="text-align text-center" width="15%">Kategori</th>
                                    <th class="text-align text-center" width="10%">Harga</th>
                                    <th class="text-align text-center" width="10%">Rekomendasi</th>
                                    <th class="text-align text-center" width="10%">Stok</th>
                                    <th class="text-align text-center" width="10%">Status</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($result))
                                    @foreach ($result as $key => $rs)
                                    <tr>
                                        <td class="text-align text-center" width="5%"> {{ $no++ }} </td>
                                        <td class="text-align text-center" width="15%"><img width="150"
                                                height="150"
                                                src="{{ base_url('assets/images/gambar_produk/').$gambar[$key]['gambar_nama'] }}" />
                                        </td>
                                        <td class="text-align" width="20%">{{ $rs['nama'] }}</td>
                                        <td class="text-align" width="15%">{{ $rs['kategori_nama'] }}</td>
                                        <td class="text-align text-right" width="10%">Rp. {!!
                                            number_format($rs['harga']) !!}</td>
                                        <td class="text-align text-center" width="10%">
                                            @if ($rs['rekomendasi_st'] == 'yes')
                                            <span class="badge badge-success">Aktif</span>
                                            @else
                                            <span class="badge badge-danger">Tidak aktif</span>
                                            @endif
                                        </td>
                                        <td class="text-align text-center" width="10%">
                                            @if ($rs['stok_st'] == 'yes')
                                            <span class="badge badge-success">Tersedia</span>
                                            @else
                                            <span class="badge badge-danger">Tidak Tersedia</span>
                                            @endif
                                        </td>
                                        <td class="text-align text-center" width="10%">
                                            @if ($rs['active_st'] == 'yes')
                                            <span class="badge badge-success">Aktif</span>
                                            @else
                                            <span class="badge badge-danger">Tidak aktif</span>
                                            @endif
                                        </td>
                                        <td width="10%">
                                            <a href="{{ site_url('master/produk/detail/'.$rs['produk_id']) }}"
                                                class="btn btn-info btn-rounded m-b-10 m-l-5"
                                                title="Edit"><i class="ti-eye"></i></a>
                                            <a href="{{ site_url('master/produk/edit/'.$rs['produk_id']) }}"
                                                class="btn btn-success btn-rounded m-b-10 m-l-5"
                                                title="Edit"><i class="ti-pencil"></i></a>
                                            <a href="{{ site_url('master/produk/delete/'.$rs['produk_id']) }}"
                                                class="btn btn-danger btn-rounded m-b-10 m-l-5"
                                                title="Delete"><i class="ti-trash"></i></button>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <th class="text-align text-center" colspan="7"><h3> Data kosong </h3></th>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        @if (isset($pagination))
                                {!! $pagination !!}
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('ext_js')
         <script>
             $(document).ready(function () {
                 $(".select2-single").select2({
                     width: '100%',
                     containerCssClass: ':all:'
                 });
             });
         </script>
         @endpush