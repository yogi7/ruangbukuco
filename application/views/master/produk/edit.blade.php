<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{ $title }}
            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="" class="text-muted">Master Data</a></li>
                        <li class="breadcrumb-item"><a href="{{ site_url('master/produk') }}"
                                class="text-muted">{{ $title }}</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Edit Data</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="container-fluid">
    {{-- notif wajib ada di setiap halaman admin kecuali delete--}}
    @include('template/notif')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-10">
                            <h4 class="card-title">Edit Data</h4>
                        </div>
                        <div class="col-lg-2">
                            <div class="text-right">
                                <a href="{{ site_url('master/produk') }}" type="submit"
                                    class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                    </div>
                    <form action="{{ site_url('master/produk/edit_process') }}" method="POST"  enctype="multipart/form-data" >
                        <input type="hidden" name="produk_id" value="{{ $result['produk_id'] }}">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>Kategori </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <select name="kategori_id" id="single" class="form-control select2-single">
                                                @foreach ($kategoris as $kategori)
                                                <option value="{{ $kategori['kategori_id'] }}"
                                                    @if($result['kategori_id']==$kategori['kategori_id']) selected=""
                                                    @endif>{{ $kategori['kategori_nama'] }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <label>Nama Produk </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <input type="text" name="nama" class="form-control"
                                                value="{{ $result['nama'] }}" placeholder="Isian nama produk...">
                                            </div>
                                        </div>
                                    </div>
                                    <label>Deskripsi </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <textarea class="form-control" id="ckeditor" name="deskripsi" rows="24" cols="50">{{ $result['deskripsi'] }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label>Harga </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <input type="text" name="harga" class="form-control" id="formatnumber"
                                                value="{{ number_format($result['harga']) }}"
                                                placeholder="Isian harga...">
                                            </div>
                                        </div>
                                    </div>
                                    <label>Satuan </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <input type="text" name="satuan" class="form-control"
                                                value="{{ $result['satuan'] }}" placeholder="Isian Satuan...">
                                            </div>
                                        </div>
                                    </div>
                                    <label>Berat </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <input type="number" name="berat" class="form-control"
                                                value="{{ $result['berat'] }}" placeholder="Dalam bentuk gram..">
                                            </div>
                                        </div>
                                    </div>
                                    <label>Gambar </label>
                                    <label class="col-sm-3 control-label"><sup
                                                style="color:red">Pilih Gambar (Max 6)</sup></label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="file" name="files[]" class="form-control" multiple="multiple"
                                                style="margin-bottom:10px">
                                            </div>
                                             <label class="col-sm-6 control-label"><i>*Klik pada gambar untuk
                                                    menghapus</i></label>
                                            <div class="row">
                                                @foreach ($gambar as $i)
                                                <a data-toggle="modal" data-id="{{ $i['gambar_nama'] }}"
                                                    class="open-modal btn btn-primary" href="#modal"> <img width="150"
                                                        height="150"
                                                        src="{{ base_url('assets/images/gambar_produk/').$i['gambar_nama'] }}" />
                                                </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <label>Aktifkan sebagai produk rekomendasi </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            @if($result['rekomendasi_st'] == 'yes')
                                            <input type="checkbox" name="rekomendasi_st" checked data-toggle="toggle"
                                                data-onstyle="primary">
                                            @else
                                            <input type="checkbox" name="rekomendasi_st" data-toggle="toggle"
                                                data-onstyle="primary">
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                    <label>Stok </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <select name="stok_st" class="form-control">
                                                @if($result['stok_st'] == 'yes')
                                                <option value="yes" selected="">Tersedia</option>
                                                <option value="no">Habis</option>
                                                @else
                                                <option value="yes">Tersedia</option>
                                                <option value="no" selected="">Habis</option>
                                                @endif
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                    <label>Status </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <select name="active_st" class="form-control">
                                                @if($result['active_st'] == 'yes')
                                                <option value="yes" selected="">Aktif</option>
                                                <option value="no">Tidak Aktif</option>
                                                @else
                                                <option value="yes">Aktif</option>
                                                <option value="no" selected="">Tidak Aktif</option>
                                                @endif
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success m-b-10 m-l-5"> Simpan</button>
                                        <button type="reset" class="btn btn-secondary m-b-10 m-l-5"> Reset</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

   <!-- Modal -->
   <div class="modal fade" id="open-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">konfirmasi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Hapus gambar <label id="nm"></label> ?
                    <input type="hidden" name="nama_gambar" id="nama_gambar" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary" onclick="hapusGambar()">Hapus</button>
                </div>
            </div>
        </div>
    </div>

@push('ext_js')
    <script>

        var ckeditor = CKEDITOR.replace('deskripsi',{
                    height:'600px'
        });

        CKEDITOR.disableAutoInline = true;
        CKEDITOR.inline('editable');

        $(document).ready(function () {
            $(".select2-single").select2({
                width: '100%',
                containerCssClass: ':all:'
            });
        });

        //separator
        var formatnumber = document.getElementById('formatnumber');
        formatnumber.addEventListener('keyup', function () {
            var val = this.value;
            val = val.replace(/[^0-9\.]/g, '');

            if (val != "") {
                valArr = val.split('.');
                valArr[0] = (parseInt(valArr[0], 10)).toLocaleString();
                val = valArr.join('.');
            }

            this.value = val;
        });

        $(document).on("click", ".open-modal", function () {
            var nama_gambar = $(this).data('id');
            $(".modal-body #nama_gambar").val(nama_gambar);
            document.getElementById("nm").innerHTML = nama_gambar;
            // As pointed out in comments, 
            // it is unnecessary to have to manually call the modal.
            $('#open-modal').modal('show');
        });

        //hapus gambar
        function hapusGambar() {
            var nama_gambar = $(".modal-body #nama_gambar").val();
            $.ajax({
                type: "POST",
                url: "{{ site_url('master/produk/hapus_gambar/') }}",
                data: {
                    'nama_gambar': nama_gambar
                },
                success: function (data) {
                    if (data == "true") {
                        alert("Data berhasil dihapus.");
                        location.reload();
                    } else {
                        alert("Data gagal dihapus !");
                        location.reload();
                    }
                }
            });
        }
    </script>
    @endpush