 <!-- breadcrumb start-->
 <section class="breadcrumb breadcrumb_bg">
     <div class="container">
         <div class="row justify-content-center">
             <div class="col-lg-10" style="margin-bottom:3%">
                 <div class="breadcrumb_iner">
                     <div class="breadcrumb_iner_item">
                         <p>{{ $title }}</p>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </section>
 <!-- breadcrumb start-->

 <div class="container" style="margin-top:5%;margin-bottom:5%;">
     <form action="{{ site_url('client/profile/edit_process') }}" method="post">
         <input type="text" name="user_id" value="{{$result['user_id']}}" hidden>
         <div class="form-group">
             <label for="exampleInputEmail1">Nama</label>
             <input type="text" name="nama" class="form-control" value="{{ $result['nama'] }}">
         </div>
         <div class="form-group">
             <label for="exampleInputEmail1">Nomor HP</label>
             <input type="text" name="hp" class="form-control" value="{{ $result['hp'] }}">
         </div>
         <div class="form-group">
             <label for="exampleInputEmail1">Alamat</label>
             <input type="text" name="alamat" class="form-control" value="{{ $result['alamat'] }}">
         </div>
         <div class="form-group">
             <label for="exampleInputEmail1">Jenis Kelamin</label>
             <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="jns_kelamin">
                 <option value="L" @if ($result['jns_kelamin']=='L' ) selected @endif>Laki - Laki</option>
                 <option value="P" @if ($result['jns_kelamin']=='P' ) selected @endif>Perempuan</option>
             </select>
         </div>
         <div class="form-group">
             <label for="exampleInputEmail1">Username</label>
             <input type="text" name="user_name" class="form-control" value="{{ $result['user_name']}}">
         </div>
         <div class="form-group">
             <label for="exampleInputEmail1">Password</label>
             <input type="text" class="form-control" name="password" value="">
             <small id="emailHelp" class="form-text text-muted">Kosongkan jika password tidak ingin diganti.</small>
         </div>
         <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Edit</button>
     </form>
 </div>