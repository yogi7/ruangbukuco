 <!-- breadcrumb start-->
 <section class="breadcrumb breadcrumb_bg">
     <div class="container">
         <div class="row justify-content-center">
             <div class="col-lg-12">
                 <div class="breadcrumb_iner">
                     <div class="breadcrumb_iner_item" style="margin-left: 8%">
                         <p>Favorit</p>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div style="margin-right:4%">
         <a href="{{ site_url('client/beranda') }}" type="button" class="btn btn-primary m-b-10 m-l-5">Kembali</a>
     </div>
 </section>
 <!-- breadcrumb start-->

 <!-- new arrival part here -->
 <section class="new_arrival" style="margin-top: 5%;margin-bottom: 93px">
     <div class="container" style="margin-bottom: 2%">


         <div class="row align-items-center">
             <div class="col-lg-4">
                 <div class="arrival_tittle">
                     <h2>Favorit</h2>
                 </div>
             </div>

         </div>
         <div style="margin-bottom: 10px">
             <form method="POST" enctype="multipart/form-data" action="{{ site_url('client/favorit/search')}}">
                 <div class="input-group">
                     <input type="text" name="produk_nama" class="form-control" placeholder="Masukkan Nama Produk"
                         @if(!empty($rs_search['produk_nama'])) value="{{ $rs_search['produk_nama'] }}" @else @endif>
                     <div class="input-group-append">
                         <button type="submit" class="btn btn-primary" value="tampilkan" name="search">
                             <i class="fa fa-search fa-sm mr-1"></i>
                             <div class="d-none d-xs-block">Search</div>
                         </button>
                         <button value="reset" name="search" class="btn btn-dark">
                             <i class="fa text-white fa-sync fa-sm mr-1"></i>
                             <div class="d-none d-xs-block">Reset</div>
                         </button>
                     </div>
                 </div>
             </form>
         </div>
         <div class="row align-items-center">
             <div class="col-lg-8">
                 <div class="arrival_filter_item filters">

                     <ul>
                         {{-- <li @if($rs_search_kategori['kategori_id'] == 0) class="active controls" @endif onclick="cariKategori(0)"><b>Tampilkan Semua</b></li> --}}
                         {{-- @foreach($kategoris as $key => $rs)
                        <li @if($rs_search_kategori['kategori_id'] == $rs['kategori_id']) class="active controls" @endif onclick="cariKategori({{ $rs['kategori_id'] }})">{{ $rs['kategori_nama'] }}
                         </li>
                         @endforeach --}}
                         @foreach($kategoris as $key => $rs)
                         <li class="controls">
                             <div class="card">
                                 <div class="card-body">
                                     <img width="35" height="35" onclick="cariKategori({{ $rs['kategori_id'] }})"
                                         src="{{ base_url('assets/images/logo_kategori/').$rs['logo'] }}" />
                                 </div>
                             </div>
                         </li>
                         {{-- <li @if($rs_search_kategori['kategori_id'] == $rs['kategori_id']) class="active controls" @endif onclick="cariKategori({{ $rs['kategori_id'] }})">{{ $rs['kategori_nama'] }}
                         </li> --}}
                         @endforeach
                     </ul>
                 </div>
             </div>
         </div>
         <div class="new_arrival_iner" style="margin-top:2%">
             @if(!empty($produk))
                @foreach($produk as $i => $rs_rekomendasi)
                <div class="single_arrivel_item col-md-3 mix women men">
                    <a href="{{ site_url('client/beranda/detail/'.$rs_rekomendasi['produk_id']) }}">
                        <img height="230" width="150"
                            src="{{ base_url('assets/images/gambar_produk/'.$gambar[$i]['gambar_nama']) }}" alt="#" class="rounded">
                        <div class="hover_text">
                            <p>{{ $rs_rekomendasi['kategori_nama'] }}</p>
                            <h3>{{ $rs_rekomendasi['nama'] }}</h3>
                            <h3>Rp. {{ number_format($rs_rekomendasi['harga']) }}</h3>
                        </div>
                    </a>
                </div>
                @endforeach
            @endif
         </div>
     </div>

 </section>
 <!-- new arrival part end -->
 @push('ext_js')
 <script>
     function cariKategori(kategori_id) {
         $.ajax({
             type: "POST",
             url: "{{ site_url('client/favorit/search_by_kategori/') }}",
             data: {
                 'kategori_id': kategori_id
             },
             success: function (data) {
                 location.reload();
             }
         });
     }
 </script>
 @endpush