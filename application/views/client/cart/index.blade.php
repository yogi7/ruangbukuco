 <!-- breadcrumb start-->
 <section class="breadcrumb breadcrumb_bg">
     <div class="container">
         <div class="row justify-content-center">
             <div class="col-lg-12">
                 <div class="breadcrumb_iner">
                     <div class="breadcrumb_iner_item">
                         <p>{{ $title }}</p>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div style="margin-right:4%">
         <a href="{{ site_url('client/beranda') }}" type="button" class="btn btn-primary m-b-10 m-l-5">Kembali</a>
     </div>
 </section>
 <!-- breadcrumb start-->


 <!-- new arrival part here -->
 <section class="new_arrival section_padding">
     <div class="container">
         <div class="row align-items-center">
             <div class="col-lg-8" style="margin-bottom:3%">
                 <div class="arrival_tittle">
                     <h2>{{ $title }}</h2>
                 </div>
             </div>
         </div>
     </div>

     <!-- tampilan mobile -->
     <div class="col-lg-6 col-md-6 d-lg-none d-block">

     </div>
     <!-- tampilan website -->
     <div class="col-lg-6 col-md-6  d-none d-lg-block">

     </div>

     <form action="{{ site_url('client/Pembayaran/checkout_process') }}" method="post">
         <div class="row">
             @if($status_anggota == 'aktif')
             <div class="col-md-6 form-group p_star" style="margin-left:8%">
                 <div class="alert alert-success" role="alert">
                     <div class="row">
                         <div class="col-md-1"><i class="far fa-check-circle fa-4x"></i></div>
                         <div class="col-md-11 align-middle">
                             <h3> <b style="color:green">Selamat !!</b> Anda telah menjadi anggota bisnis eco, <b>
                                     pemotongan harga 50%</b> untuk setiap produk eco</h3>
                         </div>
                     </div>
                 </div>
             </div>
             @endif
         </div>
         <div class="row">
             <div class="col-lg-6" style="margin-left:8%">
                 <h2>List Keranjang</h2>
                 <div class="container">
                     <div class="cart_inner">
                         <div class="table-responsive">
                             <table class="table">
                                 <thead>
                                     <tr>
                                         <th scope="col">Produk</th>
                                         <th scope="col">Harga</th>
                                         <th scope="col">Jumlah</th>
                                         <th scope="col">Total Harga</th>
                                         <th scope="col"></th>
                                     </tr>
                                 </thead>
                                 <tbody>
                                     <div id="divCheckbox" style="display: none;">
                                         {{ $prev_nil = 0 }}
                                     </div>
                                     @foreach($produk as $key => $rs)
                                     <input type="hidden" name="produk_id[]" class="produk_id"
                                         value="{{ $rs['produk_id'] }}">
                                     <input type="hidden" name="berat[]" class="berat" value="{{ $rs['berat'] }}">
                                     <tr>
                                         <td>
                                             <div class="media">
                                                 <div class="d-flex">
                                                     <img height="130" width="150"
                                                         src="{{ base_url('assets/images/gambar_produk/'.$gambar[$key]['gambar_nama']) }}" />
                                                 </div>
                                                 <div class="media-body">
                                                     <p>
                                                         <h3><b>{{ $rs['nama'] }}</b></h3>
                                                     </p>
                                                 </div>
                                             </div>
                                         </td>
                                         <td>
                                             @if($status_anggota == 'aktif' AND $rs['produk_eco_st'] == 'yes')
                                             @php $nilai_diskon = $rs['harga'] * 50 / 100 @endphp
                                             <h5 style="color:grey"><strike><i>Rp. {!! number_format($rs['harga'])
                                                         !!}</i></strike></h5>
                                             <h5><i>Rp. {!! number_format($nilai_diskon) !!}</i></h5>
                                             @else
                                             <h5>Rp. {!! number_format($rs['harga']) !!}</h5>
                                             @endif
                                         </td>
                                         <td>
                                             <div class="product_count">
                                                 <!-- <span class="input-number-decrement"> <i class="ti-minus"></i></span> -->
                                                 @if($status_anggota == 'aktif' AND $rs['produk_eco_st'] == 'yes')
                                                 <input class="input-number jumlah" data-id="total_{{ $key }}"
                                                     name="jumlah[]" type="text"
                                                     data-harga="{{ $rs['harga'] * 50 / 100 }}"
                                                     value="{{ $rs['jumlah'] }}" min="0" max="10">
                                                 @else
                                                 <input class="input-number jumlah" data-id="total_{{ $key }}"
                                                     name="jumlah[]" type="text"
                                                     data-harga="{{ $rs['harga'] }}"
                                                     value="{{ $rs['jumlah'] }}" min="0" max="10">
                                                @endif
                                             </div>
                                         </td>
                                         <td>
                                             <h5>Rp.
                                                 <label for="" class="total-harga" id="total_{{ $key }}"> {!!
                                                     number_format($rs['nil_bayar']) !!} </label>
                                                 <input type="hidden" id="hidden_total_{{ $key }}"
                                                     name="harga_pluss_jumlah[]" class="hitung"
                                                     value="{{ $rs['nil_bayar'] }}">
                                             </h5>
                                         </td>
                                         <td>
                                             <div class="cupon_text float-right">
                                                 <a data-toggle="modal" data-name="{{ $rs['nama'] }}"
                                                     data-id="{{ $rs['produk_id'] }}" class="open-modal btn_1_red"
                                                     href="#modal"><i class="ti-trash"></i></a>
                                             </div>
                                         </td>
                                     </tr>
                                     <div id="divCheckbox" style="display: none;">
                                         {{ $prev_nil = $prev_nil + $rs['nil_bayar'] }}
                                     </div>
                                     @endforeach

                                     <tr>
                                         <td></td>
                                         <td></td>
                                         <td>
                                             <h5>Subtotal</h5>
                                         </td>
                                         <td>
                                             <h5>Rp.
                                                 <label for="" class="total-harga" id="sub_total"> {!!
                                                     number_format($prev_nil) !!} </label>
                                                 <!-- <input type="text" id="hidden_total_{{ $key }}" name="harga_jumlah[]" value="{{ $rs['nil_bayar'] }}"> -->
                                             </h5>
                                         </td>
                                     </tr>
                                 </tbody>
                             </table>
                             <div class="checkout_btn_inner float-right">
                                 {{-- <a class="btn_1" href="#">Continue Shopping</a>
                                <a class="btn_1 checkout_btn_1" href="#">Proceed to checkout</a> --}}
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="col-lg-4">
                 <h2>Tujuan Pengiriman</h2>
                 <div style="font-size:10; color:grey"><i>Form yang memiliki tanda bintang (*) wajib diisi</i></div>
                 <input type="hidden" name="nama_provinsi" id="nama_provinsi">
                 <input type="hidden" name="nama_kota" id="nama_kota">
                 <input type="hidden" name="nama_kecamatan" id="nama_kecamatan">
                 <input type='hidden' name="biaya_ongkir" id='biaya_ongkir' value="">
                 <input type='hidden' name="wkt_perkiraan" id='waktu_kirim' value="">
                 <input type='hidden' name="desk" id='desk' value="">

                 <div class="col-md-12 form-group p_star">
                     <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Penerima*" />
                 </div>
                 <div class="col-md-12 form-group p_star">
                     <input type="number" class="form-control" id="pos" name="pos" placeholder="Kode pos*" />
                 </div>
                 <div class="col-md-12 form-group p_star">
                     <input type="text" class="form-control" id="phone" name="phone" placeholder="Nomor Hp*" />
                 </div>
                 <div class="col-md-12 form-group p_star">
                     <input type="text" class="form-control" id="email" name="email" placeholder="Email" />
                 </div>
                 <div class="col-md-12 form-group">
                     <select name="province_id" id="prov" class="form-control select2-single">
                         <option value="">Provinsi</option>
                         @for($i=0; $i < count($provinsi); $i++) { <option value="{{ $provinsi[$i]['province_id'] }}"
                             data-id="{{ $provinsi[$i]['province'] }}">
                             {{ $provinsi[$i]['province'] }}
                             </option>
                             @endfor
                     </select>
                 </div>
                 <div class="col-md-12 form-group">
                     <select name="city_id" id="city_id" class="form-control select2-single">
                         <option value="">Kabupaten</option>
                     </select>
                 </div>
                 <div class="col-md-12 form-group">
                     <select name="subdistrict_id" id="subdistrict_id" class="form-control select2-single">
                         <option value="">Kecamatan</option>
                     </select>
                 </div>
                 <div class="col-md-12 form-group p_star">
                     <textarea class="form-control" name="alamat" rows="4" cols="10"
                         placeholder="Alamat lengkap*"></textarea>
                 </div>
                 <div class="col-md-12 form-group">
                     <select name="kurir" id="kurir" class="form-control select2-single">
                         <option value="">Pilih Kurir</option>
                         <option value="jne">JNE</option>
                         <option value="tiki">Tiki</option>
                         <option value="pos">Pos</option>
                     </select>
                 </div>
                 <div class="col-md-12 form-group">
                     <label id="isi_ongkir"></label>
                 </div>
                 <div class="col-md-12 form-group" id="btn_checkout">
                 </div>
             </div>
         </div>
     </form>

 </section>
 <!-- new arrival part end -->


 <!-- Modal -->
 <div class="modal fade" id="open-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLongTitle">konfirmasi</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 Hapus gambar <b><label id="nm"></label></b> ?
                 <input type="hidden" name="produk_id" id="produk_id" value="">
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                 <button type="button" class="btn btn-primary" onclick="hapusProduk()">Hapus</button>
             </div>
         </div>
     </div>
 </div>


 @push('ext_js')
 <script>

     var selectedCity = "";
     var selectedKecamatan = "";

     $(document).ready(function () {
         $('#city_id').prop('disabled', 'disabled');
         $('#subdistrict_id').prop('disabled', 'disabled');
         $('#kurir').prop('disabled', 'disabled');
     });

     $(document).on("click", ".open-modal", function () {
         var produk_id = $(this).data('id');
         var produk_nama = $(this).data('name');
         $(".modal-body #produk_id").val(produk_id);
         document.getElementById("nm").innerHTML = produk_nama;
         // As pointed out in comments, 
         // it is unnecessary to have to manually call the modal.
         $('#open-modal').modal('show');
     });

     //hapus gambar
     function hapusProduk() {
         var produk_id = $(".modal-body #produk_id").val();
         $.ajax({
             type: "POST",
             url: "{{ site_url('client/cart/remove_cart_process/') }}",
             data: {
                 'produk_id': produk_id
             },
             success: function (data) {
                 location.reload();
             }
         });
     }

     $(".jumlah").change(function () {
         var id = $(this).data('id');
         var harga = $(this).data('harga');
         var total = this.value * harga;
         document.getElementById(id).innerHTML = Number(total).toLocaleString('ES-es');
         $('#hidden_' + id).val(total);
     });

     $(".jumlah").change(function () {
         var sum = 0;
         $('.hitung').each(function () {
             sum += parseInt($(this).val()); // Or this.innerHTML, this.innerText
         });
         document.getElementById('sub_total').innerHTML = Number(sum).toLocaleString('ES-es');
     });

     $("#prov").change(function () {
         var prov_id = $(this).val();
         $('#nama_provinsi').val($(this).find(':selected').data('id'));
         $.ajax({
             type: "POST",
             url: "{{ site_url('client/cart/get_kabupaten_by_prov/') }}",
             data: {
                 'prov_id': prov_id
             },
             success: function (kota) {

                 //aktifkan option kota
                 $('#city_id').prop('disabled', false);
                 $('select[name="city_id"]').empty();
                 var i;
                 var list_kota = JSON.parse(kota);
                 $('select[name="city_id"]').append('<option value="">Kabupaten</option>');
                 for (i = 0; i < list_kota.length; i++) {
                     $('select[name="city_id"]').append('<option data-id="' + list_kota[i][
                             'city_name'
                         ] + '" value="' + list_kota[i]['city_id'] +
                         '">' + list_kota[i]['city_name'] + '</option>');
                 }
             }
         });
     });


     $("#city_id").change(function () {
         var city_id = $(this).val();
         $('#nama_kota').val($(this).find(':selected').data('id'));
         $.ajax({
             type: "POST",
             url: "{{ site_url('client/cart/get_kecamatan_by_kab/') }}",
             data: {
                 'city_id': city_id
             },
             success: function (kecamatan) {
                 //aktifkan option kecamatan
                 $('#subdistrict_id').prop('disabled', false);
                 $('select[name="subdistrict_id"]').empty();
                 var i;
                 var list_kecamatan = JSON.parse(kecamatan);
                 $('select[name="subdistrict_id"]').append('<option value="">Kecamatan</option>');
                 for (i = 0; i < list_kecamatan.length; i++) {
                     $('select[name="subdistrict_id"]').append('<option data-id="' + list_kecamatan[
                             i]['subdistrict_name'] + '" value="' + list_kecamatan[i][
                             'subdistrict_id'
                         ] +
                         '">' + list_kecamatan[i]['subdistrict_name'] + '</option>');
                 }
             }
         });
     });


     $("#subdistrict_id").change(function () {
         selectedKecamatan = $(this).val();
         $('#nama_kecamatan').val($(this).find(':selected').data('id'));
         $('#kurir').prop('disabled', false);
     });

     $("#kurir").change(function () {
         // var produk_id = [];
         // produk_id = $('.produk_id').val();
         var inputs = document.getElementsByClassName('produk_id');
         produk_id = [].map.call(inputs, function (input) {
             return input.value;
         });
         var inputs_jml = document.getElementsByClassName('jumlah');
         jumlah = [].map.call(inputs_jml, function (input) {
             return input.value;
         });

         var kurir_nama = $(this).val();
         $.ajax({
             type: "POST",
             url: "{{ site_url('client/cart/get_ongkir/') }}",
             data: {
                 'kurir_nama': kurir_nama,
                 'tujuan': selectedKecamatan,
                 'produk_id': produk_id,
                 'jumlah': jumlah
             },
             success: function (data) {
                 var hasil = JSON.parse(data);
                 //console.log(hasil);
                 var i = 0;
                 var radio = [];
                 while (hasil[0]['costs'][i]) {
                     radio.push(`
                        <div class="alert alert-primary" role="alert">
                        <strong><input type="radio" name="kurir_service" data-id="` + hasil[0]['costs'][i]['cost'][0][
                             'value'
                         ] + `" data-waktu="` + hasil[0]['costs'][i]['cost'][0]['etd'] +
                         `" data-desk="` + hasil[0]['costs'][i]['description'] +
                         `" class="radio_kurir" value="` + hasil[0]['costs'][i]['service'] +
                         `"> ` + hasil[0]['costs'][i]['service'] + `</strong>
                        <br> <b>Deskripsi :</b>  ` + hasil[0]['costs'][i]['description'] +
                         ` <br> <b>Waktu Perkiraan : </b> ` + hasil[0]['costs'][i]['cost'][0][
                             'etd'
                         ] + ` hari <br> <b>Biaya ongkos kirim : </b> <i>Rp. ` + Number(hasil[0]
                             ['costs'][i]['cost'][0]['value']).toLocaleString('ES-es') + ` </i>.
                    </div>`);
                     i++;
                 }
                 document.getElementById('isi_ongkir').innerHTML = radio.join(" ");
             }
         });
     });

     $('body').on('click', '.radio_kurir', function () {
         //alert($(this).val())

         var btn = `<button class="btn btn_1"> Pembayaran </button>`;
         document.getElementById('btn_checkout').innerHTML = btn;
         $('#biaya_ongkir').val($(this).data('id'));
         $('#waktu_kirim').val($(this).data('waktu'));
         $('#desk').val($(this).data('desk'));
     });
 </script>
 @endpush