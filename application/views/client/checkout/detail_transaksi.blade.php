 <!-- breadcrumb start-->
 <section class="breadcrumb breadcrumb_bg">
     <div class="container">
         <div class="row justify-content-center">
             <div class="col-lg-12">
                 <div class="breadcrumb_iner">
                     <div class="breadcrumb_iner_item">
                         <p>Detail Transaksi</p>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div style="margin-right:4%">
         <a href="{{ site_url('client/pembayaran/list_transaksi') }}" type="button"
             class="btn btn-primary m-b-10 m-l-5">Kembali</a>
     </div>
 </section>
 <!-- breadcrumb start-->


 <!-- new arrival part here -->
 <section class="new_arrival section_padding">
     <div class="container">
         <div class="row align-items-center">
             <div class="col-lg-8" style="margin-bottom:3%">
                 <div class="arrival_tittle">
                     <h2>Detail Transaksi</h2>
                 </div>
             </div>
         </div>
     </div>
     <form action="#" method="post">
         <!-- tampilan mobile -->
         <div class="d-lg-none d-block" style="margin-left:3%;margin-right:3%;margin-top:1%">
            <div class="row">
                <div class="col-sm-11 form-group p_star">
                    <div class="alert alert-primary" role="alert">
                        <b>Tanggal transaksi : </b> @php $date=date_create($result['mdd']); echo
                        date_format($date, 'd F Y'); @endphp
                        <br>
                        <!-- tampilan mobile -->
                        <div class="d-lg-none d-block" style="margin-top:1%">
                            @if($result['transaksi_st'] == 'dibeli')
                            <b class="text-danger">Bukti transfer belum di upload, upload sekarang </b>
                            <br>
                            <div style="margin-top:1%" class="row">
                                <a href="{{ site_url('client/pembayaran/index/'.$result['transaksi_id']) }}"
                                    style="margin-right:1%" type="button" class="btn btn-info col-sm-1">Detail
                                    Pembayaran</a>
                                <a href="{{ site_url('client/pembayaran/kirim_bukti_tf/'.$result['transaksi_id']) }}"
                                    style="margin-right:1%; margin-top:2%" type="button"
                                    class="btn btn-success col-sm-1">Kirim Bukti
                                    Transfer</a>
                            </div>
                            @elseif($result['transaksi_st'] == 'batal')
                            <div style="margin-top:1%">
                                <b class="text-danger">Transaksi dibatalkan.</b>
                            </div>
                            @else
                            <div style="margin-top:1%">
                                <a href="{{ site_url('client/pembayaran/index/'.$result['transaksi_id']) }}"
                                    style="margin-right:1%" type="button" class="btn btn-info m-b-10 m-l-5">Detail
                                    Pembayaran</a>
                                <a href="{{ site_url('client/pembayaran/detail_pengiriman/'.$result['transaksi_id'].'/'.$kurir['resi'] ) }}"
                                    type="button" class="btn btn-primary m-b-10 m-l-5">Cek Pengiriman</a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
         </div>
         <!-- tampilan website -->
         <div class="d-none d-lg-block">
            <div class="row" style="margin-left:7%">
                <div class="col-sm-11 form-group p_star">
                    <div class="alert alert-primary" role="alert">
                        <b>Tanggal transaksi : </b> @php $date=date_create($result['mdd']); echo
                        date_format($date, 'd F Y'); @endphp
                        <br>
                        <!-- tampilan websute -->
                        <div class="d-none d-lg-block">
                            @if($result['transaksi_st'] == 'dibeli')
                            <b class="text-danger">Bukti transfer belum di upload, upload sekarang </b>
                            <br>
                            <div style="margin-top:1%">
                                <a href="{{ site_url('client/pembayaran/index/'.$result['transaksi_id']) }}"
                                    style="margin-right:1%" type="button" class="btn btn-info m-b-10 m-l-5">Detail
                                    Pembayaran</a>
                                <a href="{{ site_url('client/pembayaran/kirim_bukti_tf/'.$result['transaksi_id']) }}"
                                    style="margin-right:1%" type="button" class="btn btn-success m-b-10 m-l-5">Kirim Bukti
                                    Transfer</a>
                            </div>
                            @elseif($result['transaksi_st'] == 'batal')
                            <div style="margin-top:1%">
                                <b class="text-danger">Transaksi dibatalkan.</b>
                            </div>
                            @else
                            <div style="margin-top:1%">
                                <a href="{{ site_url('client/pembayaran/index/'.$result['transaksi_id']) }}"
                                    style="margin-right:1%" type="button" class="btn btn-info m-b-10 m-l-5">Detail
                                    Pembayaran</a>
                                <a href="{{ site_url('client/pembayaran/detail_pengiriman/'.$result['transaksi_id'].'/'.$kurir['resi'] ) }}"
                                    type="button" class="btn btn-primary m-b-10 m-l-5">Cek Pengiriman</a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
         </div>
         <div class="row">
             <div class="col-sm-5" style="margin-left:8%">
                 <h2>Detail Produk</h2>
                 <div class="container">
                     <div class="cart_inner">
                         <div class="table-responsive">
                             <table class="table">
                                 <thead>
                                     <tr>
                                         <th scope="col">Produk</th>
                                         <th scope="col">Harga</th>
                                         <th scope="col">Jumlah</th>
                                         <th scope="col">Total Harga</th>
                                     </tr>
                                 </thead>
                                 <tbody>
                                     <div id="divCheckbox" style="display: none;">
                                         {{ $prev_nil = 0 }}
                                     </div>
                                     @foreach($produk as $key => $rs)
                                     <input type="hidden" name="produk_id[]" value="{{ $rs['produk_id'] }}">
                                     <tr>
                                         <td>
                                             <div class="media">
                                                 <div class="d-flex">
                                                     <img height="130" width="150"
                                                         src="{{ base_url('assets/images/gambar_produk/'.$gambar[$key]['gambar_nama']) }}" />
                                                 </div>
                                                 <div class="media-body">
                                                     <p>
                                                         <h3><b>{{ $rs['nama'] }}</b></h3>
                                                     </p>
                                                 </div>
                                             </div>
                                         </td>
                                         <td>
                                             <h5>Rp. {!! number_format($rs['harga']) !!}</h5>
                                         </td>
                                         <td>
                                             <div class="product_count">
                                                 <label>{{ $rs['jumlah'] }}</label>
                                             </div>
                                         </td>
                                         <td>
                                             <h5>Rp. {{ number_format($rs['harga'] * $rs['jumlah']) }}
                                             </h5>
                                         </td>
                                     </tr>
                                     <div id="divCheckbox" style="display: none;">
                                         {{ $prev_nil = $prev_nil + $rs['nil_bayar'] }}
                                     </div>
                                     @endforeach

                                     <tr>
                                         <td></td>
                                         <td></td>
                                         <td>
                                             <h5>Subtotal</h5>
                                         </td>
                                         <td>
                                             <h5>Rp.
                                                 <label for="" class="total-harga" id="sub_total">
                                                     {{ number_format($result['subtotal']) }} </label>
                                             </h5>
                                         </td>
                                     </tr>
                                 </tbody>
                             </table>
                             <div class="checkout_btn_inner float-right">
                                 {{-- <a class="btn_1" href="#">Continue Shopping</a>
                                <a class="btn_1 checkout_btn_1" href="#">Proceed to checkout</a> --}}
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="col-lg-3">
                 <h2>Tujuan Pengiriman</h2>
                 <div class="col-md-12 form-group p_star">
                     <label><b>Nama Penerima :</b></label><br>
                     <label>{{ $alamat['nama_penerima'] }}</label>
                 </div>
                 <div class="col-md-12 form-group p_star">
                     <label><b>Kode Pos :</b></label><br>
                     <label>{{ $alamat['kode_pos'] }}</label>
                 </div>
                 <div class="col-md-12 form-group p_star">
                     <label><b>No Hp :</b></label><br>
                     <label>{{ $alamat['no_hp'] }}</label>
                 </div>
                 <div class="col-md-12 form-group p_star">
                     <label><b>Email Penerima :</b></label><br>
                     <label>{{ $alamat['email'] }}</label>
                 </div>
                 <div class="col-md-12 form-group p_star">
                     <label><b>Provinsi :</b></label><br>
                     <label>{{ $alamat['prov_nama'] }}</label>
                 </div>
                 <div class="col-md-12 form-group p_star">
                     <label><b>Kabupaten :</b></label><br>
                     <label>{{ $alamat['kab_nama'] }}</label>
                 </div>
                 <div class="col-md-12 form-group p_star">
                     <label><b>Alamat Lengkap :</b></label><br>
                     <label>{{ $alamat['alamat_lengkap'] }}</label>
                 </div>
             </div>
             <div class="col-lg-2">
                 <h2>Kurir Pengiriman</h2>
                 <div class="col-md-12 form-group p_star">
                     <label><b>Nama Kurir :</b></label><br>
                     <label>{{ $kurir['nama_kurir'] }}</label>
                 </div>
                 <div class="col-md-12 form-group p_star">
                     <label><b>Jenis Pelayanan :</b></label><br>
                     <label>{{ $kurir['service'] }}</label>
                 </div>
                 <div class="col-md-12 form-group p_star">
                     <label><b>Deskripsi Kurir :</b></label><br>
                     <label>{{ $kurir['desk_kurir'] }}</label>
                 </div>
                 <div class="col-md-12 form-group p_star">
                     <label><b>Waktu Perkiraan Sampai :</b></label><br>
                     <label>{{ $kurir['wkt_perkiraan'] }} hari</label>
                 </div>
                 <div class="col-md-12 form-group p_star">
                     <label><b>Biaya Ongkos Kirim :</b></label><br>
                     <label>Rp. {{ number_format($kurir['biaya_ongkir']) }}</label>
                 </div>
             </div>
         </div>
     </form>

 </section>
 <!-- new arrival part end -->


 <!-- Modal -->
 <div class="modal fade" id="open-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLongTitle">konfirmasi</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 Hapus gambar <b><label id="nm"></label></b> ?
                 <input type="hidden" name="produk_id" id="produk_id" value="">
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                 <button type="button" class="btn btn-primary" onclick="hapusProduk()">Hapus</button>
             </div>
         </div>
     </div>
 </div>


 @push('ext_js')
 <script>
     var selectedCity = "";

     $(document).ready(function () {
         $('#city_id').prop('disabled', 'disabled');
         $('#kurir').prop('disabled', 'disabled');
     });

     $(document).on("click", ".open-modal", function () {
         var produk_id = $(this).data('id');
         var produk_nama = $(this).data('name');
         $(".modal-body #produk_id").val(produk_id);
         document.getElementById("nm").innerHTML = produk_nama;
         // As pointed out in comments, 
         // it is unnecessary to have to manually call the modal.
         $('#open-modal').modal('show');
     });

     //hapus gambar
     function hapusProduk() {
         var produk_id = $(".modal-body #produk_id").val();
         $.ajax({
             type: "POST",
             url: "{{ site_url('client/cart/remove_cart_process/') }}",
             data: {
                 'produk_id': produk_id
             },
             success: function (data) {
                 location.reload();
             }
         });
     }

     $(".jumlah").change(function () {
         var id = $(this).data('id');
         var harga = $(this).data('harga');
         var total = this.value * harga;
         document.getElementById(id).innerHTML = Number(total).toLocaleString('ES-es');
         $('#hidden_' + id).val(total);
     });

     $(".jumlah").change(function () {
         var sum = 0;
         $('.hitung').each(function () {
             sum += parseInt($(this).val()); // Or this.innerHTML, this.innerText
         });
         document.getElementById('sub_total').innerHTML = Number(sum).toLocaleString('ES-es');
     });

     $("#prov").change(function () {
         var prov_id = $(this).val();
         $('#nama_provinsi').val($(this).find(':selected').data('id'));
         $.ajax({
             type: "POST",
             url: "{{ site_url('client/cart/get_kabupaten_by_prov/') }}",
             data: {
                 'prov_id': prov_id
             },
             success: function (kota) {
                 //aktifkan option kota
                 $('#city_id').prop('disabled', false);
                 $('select[name="city_id"]').empty();
                 var i;
                 var list_kota = JSON.parse(kota);
                 $('select[name="city_id"]').append('<option value="">Kabupaten</option>');
                 for (i = 0; i < list_kota.length; i++) {
                     $('select[name="city_id"]').append('<option data-id="' + list_kota[i][
                             'city_name'
                         ] + '" value="' + list_kota[i]['city_id'] +
                         '">' + list_kota[i]['city_name'] + '</option>');
                 }
             }
         });

         $("#city_id").change(function () {
             selectedCity = $(this).val();
             $('#nama_kota').val($(this).find(':selected').data('id'));
             $('#kurir').prop('disabled', false);
         });

         $("#kurir").change(function () {
             var kurir_nama = $(this).val();
             $.ajax({
                 type: "POST",
                 url: "{{ site_url('client/cart/get_ongkir/') }}",
                 data: {
                     'kurir_nama': kurir_nama,
                     'tujuan': selectedCity
                 },
                 success: function (data) {
                     var hasil = JSON.parse(data);
                     //console.log(hasil);
                     var i = 0;
                     var radio = [];
                     while (hasil[0]['costs'][i]) {
                         radio.push(`
                            <div class="alert alert-primary" role="alert">
                            <strong><input type="radio" name="kurir_service" data-id="` + hasil[0]['costs'][i]['cost'][
                                 0
                             ]['value'] + `" data-waktu="` + hasil[0]['costs'][i][
                                 'cost'
                             ][0]['etd'] + `" data-desk="` + hasil[0]['costs']
                             [i]['description'] + `" class="radio_kurir" value="` +
                             hasil[0]['costs'][i]['service'] + `"> ` + hasil[0]['costs']
                             [i]['service'] + `</strong>
                            <br> <b>Deskripsi :</b>  ` + hasil[0]['costs'][i]['description'] +
                             ` <br> <b>Waktu Perkiraan : </b> ` + hasil[0]['costs'][i][
                                 'cost'
                             ][0]['etd'] +
                             ` hari <br> <b>Biaya ongkos kirim : </b> <i>Rp. ` + Number(
                                 hasil[0]['costs'][i]['cost'][0]['value'])
                             .toLocaleString('ES-es') + ` </i>.
                        </div>`);
                         i++;
                     }
                     document.getElementById('isi_ongkir').innerHTML = radio.join(" ");
                 }
             });
         });

         $('body').on('click', '.radio_kurir', function () {
             //alert($(this).val())

             var btn = `<button class="btn btn_1"> Pembayaran </button>`;
             document.getElementById('btn_checkout').innerHTML = btn;
             $('#biaya_ongkir').val($(this).data('id'));
             $('#waktu_kirim').val($(this).data('waktu'));
             $('#desk').val($(this).data('desk'));
         })

     });
 </script>
 @endpush