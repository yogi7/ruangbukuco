 <!-- breadcrumb start-->
 <section class="breadcrumb breadcrumb_bg">
     <div class="container">
         <div class="row justify-content-center">
             <div class="col-lg-12">
                 <div class="breadcrumb_iner">
                     <div class="breadcrumb_iner_item">
                         <p>Detail Pengiriman</p>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div style="margin-right:4%">
         <a href="{{ site_url('client/pembayaran/detail_transaksi/'.$transaksi_id) }}" type="button"
             class="btn btn-primary m-b-10 m-l-5">Kembali</a>
     </div>
 </section>
 <!-- breadcrumb start-->


 <!-- new arrival part here -->
 <section class="new_arrival section_padding">
     <div class="container">
         <div class="row align-items-center">
             <div class="col-lg-8" style="margin-bottom:3%">
                 <div class="arrival_tittle">
                     <h2>Detail Pengiriman</h2>
                 </div>
             </div>
         </div>
     </div>
     <form action="#" method="post">
         <div class="row" style="margin-left:7%">
             <div class="col-sm-11 form-group p_star">
                 <div class="alert alert-primary" role="alert">
                     <b>Resi Pengiriman : {{ $resi }}</b>
                 </div>
             </div>
         </div>
         <div class="row" style="margin-left:8%; margin-right:8%">
             @if(!empty($status_resi['code']))
             @if($status_resi['code'])
             <b>Status pengiriman</b>
             <div class="table-responsive "> 
             <table class="table">
                 <thead>
                     <tr>
                         <th class="text-align text-center" width="50%">Status</th>
                         <th class="text-align text-center" width="25%">Penerima</th>
                         <th class="text-align text-center" width="25%">Tanggal</th>
                     </tr>
                 </thead>
                 <tbody>
                     <tr>
                         <td class="text-align text-center">
                             @if($track['delivery_status']['status']
                             == 'DELIVERED')
                             <span class="badge badge-success">Terkirim</span>
                             @else
                             <span class="badge badge-danger">Tidak
                                 dikirim</span>
                             @endif
                         </td>
                         <td class="text-align text-center">
                             {!!
                             $track['delivery_status']['pod_receiver']
                             !!}
                         </td>
                         <td class="text-align text-center">
                             {!!
                             $track['delivery_status']['pod_date']
                             !!} {!!
                             $track['delivery_status']['pod_time']
                             !!}
                         </td>
                     </tr>
                 </tbody>
             </table>
             </div>
             @php $jumlah = count($track['manifest']) @endphp
             <b>Riwayat Pengiriman</B>
             <div class="table-responsive">
             <table class="table">
                 <thead>
                     <tr>
                         <th class="text-align text-center" width="5%">No.</th>
                         <th class="text-align text-center" width="25%">Tanggal</th>
                         <th class="text-align text-center" width="20%">Kota</th>
                         <th class="text-align text-center" width="50%">Keterangan</th>
                     </tr>
                 </thead>
                 <tbody>
                     @for ($i = 0; $i < $jumlah; $i++) <tr>
                         <th class="text-align text-center" width="5%"> {{ $i+1 }} </th>
                         <th class="text-align">
                             {!!
                             $track['manifest'][$i]['manifest_date']
                             !!}<br>
                             {!!
                             $track['manifest'][$i]['manifest_time']
                             !!}
                         </th>
                         <th class="text-align">{!!
                             $track['manifest'][$i]['city_name']
                             !!}</th>
                         <th class="text-align">{!!
                             $track['manifest'][$i]['manifest_description']
                             !!}</th>
                         </tr>
                         @endfor
                 </tbody>
             </table>
             </div>
             @elseif($status_resi['code'] == 400)
             <div class="alert alert-danger" role="alert" style="margin-bottom:5%">
                 <strong> Nomor resi yang diinputkan salah
                     !</strong>
             </div>
             @endif
             @else
             Resi belum diinputkan !
             @endif
         </div>
         </div>
     </form>

 </section>
 <!-- new arrival part end -->


 <!-- Modal -->
 <div class="modal fade" id="open-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLongTitle">konfirmasi</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 Hapus gambar <b><label id="nm"></label></b> ?
                 <input type="hidden" name="produk_id" id="produk_id" value="">
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                 <button type="button" class="btn btn-primary" onclick="hapusProduk()">Hapus</button>
             </div>
         </div>
     </div>
 </div>


 @push('ext_js')
 <script>
     var selectedCity = "";

     $(document).ready(function () {
         $('#city_id').prop('disabled', 'disabled');
         $('#kurir').prop('disabled', 'disabled');
     });

     $(document).on("click", ".open-modal", function () {
         var produk_id = $(this).data('id');
         var produk_nama = $(this).data('name');
         $(".modal-body #produk_id").val(produk_id);
         document.getElementById("nm").innerHTML = produk_nama;
         // As pointed out in comments, 
         // it is unnecessary to have to manually call the modal.
         $('#open-modal').modal('show');
     });

     //hapus gambar
     function hapusProduk() {
         var produk_id = $(".modal-body #produk_id").val();
         $.ajax({
             type: "POST",
             url: "{{ site_url('client/cart/remove_cart_process/') }}",
             data: {
                 'produk_id': produk_id
             },
             success: function (data) {
                 location.reload();
             }
         });
     }

     $(".jumlah").change(function () {
         var id = $(this).data('id');
         var harga = $(this).data('harga');
         var total = this.value * harga;
         document.getElementById(id).innerHTML = Number(total).toLocaleString('ES-es');
         $('#hidden_' + id).val(total);
     });

     $(".jumlah").change(function () {
         var sum = 0;
         $('.hitung').each(function () {
             sum += parseInt($(this).val()); // Or this.innerHTML, this.innerText
         });
         document.getElementById('sub_total').innerHTML = Number(sum).toLocaleString('ES-es');
     });

     $("#prov").change(function () {
         var prov_id = $(this).val();
         $('#nama_provinsi').val($(this).find(':selected').data('id'));
         $.ajax({
             type: "POST",
             url: "{{ site_url('client/cart/get_kabupaten_by_prov/') }}",
             data: {
                 'prov_id': prov_id
             },
             success: function (kota) {
                 //aktifkan option kota
                 $('#city_id').prop('disabled', false);
                 $('select[name="city_id"]').empty();
                 var i;
                 var list_kota = JSON.parse(kota);
                 $('select[name="city_id"]').append('<option value="">Kabupaten</option>');
                 for (i = 0; i < list_kota.length; i++) {
                     $('select[name="city_id"]').append('<option data-id="' + list_kota[i][
                             'city_name'
                         ] + '" value="' + list_kota[i]['city_id'] +
                         '">' + list_kota[i]['city_name'] + '</option>');
                 }
             }
         });

         $("#city_id").change(function () {
             selectedCity = $(this).val();
             $('#nama_kota').val($(this).find(':selected').data('id'));
             $('#kurir').prop('disabled', false);
         });

         $("#kurir").change(function () {
             var kurir_nama = $(this).val();
             $.ajax({
                 type: "POST",
                 url: "{{ site_url('client/cart/get_ongkir/') }}",
                 data: {
                     'kurir_nama': kurir_nama,
                     'tujuan': selectedCity
                 },
                 success: function (data) {
                     var hasil = JSON.parse(data);
                     //console.log(hasil);
                     var i = 0;
                     var radio = [];
                     while (hasil[0]['costs'][i]) {
                         radio.push(`
                            <div class="alert alert-primary" role="alert">
                            <strong><input type="radio" name="kurir_service" data-id="` + hasil[0]['costs'][i]['cost'][
                                 0
                             ]['value'] + `" data-waktu="` + hasil[0]['costs'][i][
                                 'cost'
                             ][0]['etd'] + `" data-desk="` + hasil[0]['costs']
                             [i]['description'] + `" class="radio_kurir" value="` +
                             hasil[0]['costs'][i]['service'] + `"> ` + hasil[0]['costs']
                             [i]['service'] + `</strong>
                            <br> <b>Deskripsi :</b>  ` + hasil[0]['costs'][i]['description'] +
                             ` <br> <b>Waktu Perkiraan : </b> ` + hasil[0]['costs'][i][
                                 'cost'
                             ][0]['etd'] +
                             ` hari <br> <b>Biaya ongkos kirim : </b> <i>Rp. ` + Number(
                                 hasil[0]['costs'][i]['cost'][0]['value'])
                             .toLocaleString('ES-es') + ` </i>.
                        </div>`);
                         i++;
                     }
                     document.getElementById('isi_ongkir').innerHTML = radio.join(" ");
                 }
             });
         });

         $('body').on('click', '.radio_kurir', function () {
             //alert($(this).val())

             var btn = `<button class="btn btn_1"> Pembayaran </button>`;
             document.getElementById('btn_checkout').innerHTML = btn;
             $('#biaya_ongkir').val($(this).data('id'));
             $('#waktu_kirim').val($(this).data('waktu'));
             $('#desk').val($(this).data('desk'));
         })

     });
 </script>
 @endpush