 <!-- breadcrumb start-->
 <section class="breadcrumb breadcrumb_bg">
     <div class="container">
         <div class="row justify-content-center">
             <div class="col-lg-10" style="margin-bottom:3%">
                 <div class="breadcrumb_iner">
                     <div class="breadcrumb_iner_item">
                         <p>{{ $title }}</p>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div style="margin-right:4%">
         <a href="{{ site_url('client/cart/index') }}" type="button" class="btn btn-primary m-b-10 m-l-5">Kembali</a>
     </div>
 </section>
 <!-- breadcrumb start-->


 <!-- new arrival part here -->
 <section class="new_arrival section_padding" style="margin-left:5%; margin-right:5%">
     <div class="container">
         <div class="row align-items-center">

             <div class="col-lg-8">
                 <div class="arrival_tittle">
                     <h2>{{ $title }}</h2>
                 </div>
             </div>
         </div>
     </div>
     <section class="confirmation_part" style="margin-top:2%">
         <div class="container">
             <div class="row">
                 <div class="col-lg-10">
                     <div class="confirmation_tittle">
                         <span><b>
                                 <h3>Terima kasih.
                                     <br>Segera selesaikan pembayaran anda sebelum stok habis.</h3>
                             </b></span>
                     </div>
                 </div>
                 <div class="alert alert-primary col-lg-10" role="alert">
                     <div style="margin-left:2%">
                         <h4>Selesai pembayaran sebelum :</h4>
                     </div>
                     <span>
                         <center><b>Tanggal</b></center>
                     </span>
                     <center>
                         <h2>@php $date=date_create($result['tgl_batas_bayar']); echo date_format($date, 'd F Y');
                             @endphp</h2>
                     </center>
                     <span>
                         <center><b>Pukul</b></center>
                     </span>
                     <center>
                         <h2>@php
                             echo substr($result['tgl_batas_bayar'], 11, 8);
                             @endphp</h2>
                     </center>
                     <span>
                         <center>Jam : Menit : Detik</center>
                     </span>
                 </div>
                 <div class="alert col-lg-10 bg-light" role="alert">
                     <div style="margin-left:2%">
                         <h4>Transfer pada rekening :</h4>
                     </div>
                     <center><img style="width:120px;height:70px"
                             src="{{ base_url('assets/images/prefrensi/'.$pref['img_name']) }}" />
                         <div style="margin-top:1%">
                             <h2>(kode bank : {{ $pref['kode'] }}) {{ $pref['value_pref'] }}</h2>
                             <h3>a.n {{ $pref['keterangan'] }}</h3>
                         </div>
                     </center>
                 </div>
                 <div class="alert col-lg-10 bg-light" role="alert">
                     <div style="margin-left:2%">
                         <h4>jumlah yang harus dibayar :</h4>
                         <i> (*Sudah termasuk ongkos kirim)</i>
                    </div>
                     <center>
                         <div style="margin-top:1%">
                             <h2>Rp.
                                 {{ number_format($result['subtotal'] + $result['kode_unik'] + $result['biaya_ongkir']) }}
                             </h2>
                         </div>
                         <div class="alert col-lg-6 alert-warning" role="alert">
                             <center>
                                 <div style="margin-top:1%"><i>
                                         *Transfer dengan jumlah yang sesuai sampai dengan 3 angka dibelakang titik (.)
                                         untuk mempermudah verifikasi</i>
                                 </div>
                             </center>
                         </div>
                     </center>
                 </div>
             </div>
             <!-- tampilan mobile -->
             <div class="col-lg-6 col-md-6 d-lg-none d-block">
             <div>
                     <a href="{{ site_url('client/beranda') }}" type="button" class="btn btn-primary m-b-10 m-l-5">Lihat
                         produk lain</a>
                     <a href="{{ site_url('client/pembayaran/kirim_bukti_tf/'.$result['transaksi_id']) }}" type="button"
                         class="btn btn-success m-b-10 m-l-5">Kirim bukti transfer</a>
                 </div>
             </div>
             <!-- tampilan website -->
             <div class="col-lg-6 col-md-6  d-none d-lg-block">
                 <div class="float-right" style="margin-right:17%">
                     <a href="{{ site_url('client/beranda') }}" type="button" class="btn btn-primary m-b-10 m-l-5">Lihat
                         produk lain</a>
                     <a href="{{ site_url('client/pembayaran/kirim_bukti_tf/'.$result['transaksi_id']) }}" type="button"
                         class="btn btn-success m-b-10 m-l-5">Kirim bukti transfer</a>
                 </div>
             </div>
         </div>
     </section>
 </section>