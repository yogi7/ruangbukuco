 <!-- breadcrumb start-->
 <section class="breadcrumb breadcrumb_bg">
     <div class="container">
         <div class="row justify-content-center">
             <div class="col-lg-10" style="margin-bottom:3%">
                 <div class="breadcrumb_iner">
                     <div class="breadcrumb_iner_item">
                         <p>{{ $title }}</p>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div style="margin-right:4%">
         <a href="{{ site_url('client/cart/index') }}" type="button" class="btn btn-primary m-b-10 m-l-5">Kembali</a>
     </div>
 </section>
 <!-- breadcrumb start-->


 <!-- new arrival part here -->
 <section class="new_arrival section_padding" style="margin-left:5%; margin-right:5%">
     <div class="container">
         <div class="row align-items-center">

             <div class="col-lg-8">
                 <div class="arrival_tittle">
                     <h2>{{ $title }}</h2>
                 </div>
             </div>
         </div>
     </div>
     <form action="{{ site_url('client/Pembayaran/kirim_bukti_tf_process') }}" method="post"
         enctype="multipart/form-data">
         <input type="hidden" name="transaksi_id" value="{{ $result['transaksi_id'] }}">
         <section class="confirmation_part" style="margin-top:2%">
             <div class="container">
                 <div class="row">
                     <div class="col-lg-10">
                         <div class="confirmation_tittle">
                             <span><b>
                                     <h3>
                                         <br>Segera kirimkan bukti transfer pembayaran anda.</h3>
                                 </b></span>
                         </div>
                     </div>
                     <div class="alert alert-primary col-lg-10" role="alert">
                         <div style="margin-left:2%">
                             <h4>Kirim bukti pembayaran sebelum :</h4>
                         </div>
                         <span>
                             <center><b>Tanggal</b></center>
                         </span>
                         <center>
                             <h2>@php $date=date_create($result['tgl_batas_bayar']); echo date_format($date, 'd F Y');
                                 @endphp</h2>
                         </center>
                         <span>
                             <center><b>Pukul</b></center>
                         </span>
                         <center>
                             <h2>@php
                                 echo substr($result['tgl_batas_bayar'], 11, 8);
                                 @endphp</h2>
                         </center>
                         <span>
                             <center>Jam : Menit : Detik</center>
                         </span>
                         <center>
                             <div class="alert col-lg-6 alert-danger" role="alert" style="margin-top:3%">
                                 <center>
                                     <div style="margin-top:1%"><i>
                                             *Transaksi akan otomatis <b>dibatalkan</b> bila bukti transfer tidak
                                             dikirimkan
                                             sampai dengan tanggal tersebut</i>
                                     </div>
                                 </center>
                             </div>
                         </center>
                     </div>

                     <div class="alert alert-primary col-lg-10" role="alert">
                         <div class="form-group">
                             <div style="margin-left:2%">
                                 <h4>Nomor Rekening Pentrasfer :</h4>
                             </div>
                             <center>
                                 <div class="col-md-12 form-group p_star">
                                     <input type="text" class="form-control" name="no_rek_pentransfer" />
                                 </div>
                             </center>
                         </div>
                         <div class="form-group">
                             <div style="margin-left:2%">
                                 <h4>Nama Rekening :</h4>
                             </div>
                             <center>
                                 <div class="col-md-12 form-group p_star">
                                     <input type="text" class="form-control" name="nama_rek" />
                                 </div>
                             </center>
                         </div>
                         <div class="form-group">
                             <div style="margin-left:2%">
                                 <h4>Foto bukti transfer :</h4>
                             </div>
                             <center>
                                 <div class="col-md-12 form-group p_star">
                                     <input type="file" class="form-control" name="file_bukti_tf" />
                                 </div>
                             </center>
                         </div>
                     </div>

                 </div>
                 <div class="float-right" style="margin-right:16%">
                     <button class="btn btn_1_green"> Upload </button>
                 </div>
             </div>
         </section>
     </form>
 </section>