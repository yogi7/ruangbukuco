<style>
    .centered {
  position: absolute;
  top: 50%;
  left: 22%;
  transform: translate(-50%, -50%);
}

.rightcostume {
  position: absolute;
  top: 50%;
  left: 30%;
  transform: translate(-50%, -50%);
}
</style>

<div class="row">
    <!-- tampilan mobile -->
    <div class="col-sm-7 d-lg-none d-block">
        <!-- banner part start-->
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            @php
            $i = 1;
            $o = 1;
            @endphp
            <div class="carousel-inner">
                @foreach ($main_banner as $gbr_banner)
                <div class="carousel-item @if($i==1) {{ 'active' }} @endif " id="{{ $i++ }}">
                    <img class="w-100" style="height: 200px"
                        src="{{ base_url('assets/images/banner/'.$gbr_banner['nama_gambar']) }}" alt="First slide">
                        <div class="rightcostume">
                            <h5 class="text-primary"><b>{{ $pref_toko['value_pref'] }}</b></h5>
                            <h3 class="text-primary">{{ $pref_toko['keterangan'] }}</h3>
                    </div>
                </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!-- banner part end-->
    </div>
    <!-- tampilan website -->
    <div class="col-lg-row d-none d-lg-block">
        <!-- banner part start-->
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            @php
            $i = 1;
            $o = 1;
            @endphp
            <div class="carousel-inner">
                @foreach ($main_banner as $gbr_banner)
                <div class="carousel-item @if($i==1) {{ 'active' }} @endif " id="{{ $i++ }}">
                    <img class="w-100" style="height: 700px"
                        src="{{ base_url('assets/images/banner/'.$gbr_banner['nama_gambar']) }}" alt="First slide">
                    <div class="centered">
                        <h5 class="text-primary">{{ $pref_toko['value_pref'] }}</h5>
                        <h1 class="text-primary">{{ $pref_toko['keterangan'] }}</h1>
                        <a href="{{ site_url('client/beranda/search') }}" class="btn_1">Beli Sekarang</a>
                    </div>
                </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!-- banner part end-->
    </div>
</div>
<!-- new arrival part here -->
<section class="new_arrival">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <!-- tampilan website -->
                <div class="new_arrival_iner filter-container" style="padding-left:1%; padding-right:1%; margin-top:2%">
                    @foreach($rekomendasi as $i => $rs_rekomendasi)
                    <a href="{{ site_url('client/beranda/detail/'.$rs_rekomendasi['produk_id']) }}">
                        <div class="single_arrivel_item mix col-lg-3 women men ">
                                <img style="width:500px;height:600px;"
                                    src="{{ base_url('assets/images/gambar_produk/'.$rs_rekomendasi['gambar_nama']) }}"
                                    alt="#"/>
                                <div class="hover_text">
                                    <p>{{ $rs_rekomendasi['kategori_nama'] }}</p>
                                    <h3>{{ $rs_rekomendasi['nama'] }}</h3>
                                    <div class="rate_icon">
                                        <a href="#"> <i class="fas fa-star"></i> </a>
                                        <a href="#"> <i class="fas fa-star"></i> </a>
                                        <a href="#"> <i class="fas fa-star"></i> </a>
                                        <a href="#"> <i class="fas fa-star"></i> </a>
                                        <a href="#"> <i class="fas fa-star"></i> </a>
                                    </div>
                                    <h3>Rp. {{ number_format($rs_rekomendasi['harga']) }}</h3>
                                </div>
                        </div>
                        </a>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<!-- new arrival part end -->

    <div class="col-lg-12 text-center">
        <a href="{{ site_url('client/beranda/search') }}" class="btn_2"><b>Tampilkan semua</b></a>
    </div>

    <!-- free shipping here -->
    <section class="shipping_details section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="single_shopping_details">
                        <img src="{{ base_url('assets/images/icon/icon_1.png') }}" alt="">
                        <h4>Kurir Terbaik</h4>
                        <p>Pengiriman paket menggunakan kurir terkenal seperti JNE, Tiki dan POS.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="single_shopping_details">
                    <img src="{{ base_url('assets/images/icon/icon_2.png') }}" alt="">
                        <h4>Harga terjangkau</h4>
                        <p>Harga terjangkau dengan kualitas terbaik.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="single_shopping_details">
                    <img src="{{ base_url('assets/images/icon/icon_4.png') }}" alt="">
                        <h4>Pengiriman Cepat</h4>
                        <p>Dengan kurir terbaik pengiriman cepat dan tepat waktu.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- free shipping end -->
@push('ext_js')
<script>
    function cariKategori(kategori_id) {
        $.ajax({
            type: "POST",
            url: "{{ site_url('client/beranda/search_by_kategori/') }}",
            data: {
                'kategori_id': kategori_id
            },
            success: function (data) {
                location.reload();
                redirect("client/beranda/cari");

            }
        });
    }
</script>
@endpush