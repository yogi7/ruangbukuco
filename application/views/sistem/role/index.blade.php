<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{ $title }}
            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Sistem</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">{{ $title }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<div class="container-fluid">
{{-- notif wajib ada di setiap halaman admin kecuali delete--}}
    @include('template/notif')
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card"> 
                <div class="card-body">
                    <div class="row">
                        <div class="row">
                            <h4 class="card-title col-lg-12">Daftar Role</h4>
                        </div>
                        <form class="col-lg-12 row" action="{{ site_url('sistem/role/search_process') }}" method="POST">
                            <div class="col-lg-3">
                            <select name="group_id" id="single" class="form-control select2-single">
                                <option value="">Group...</option>
                                @foreach ($groups as $group)
                                <option value="{{ $group['group_id'] }}">{{ $group['group_name'] }}</option>
                                @endforeach
                            </select>
                            </div>
                            <div class="col-lg-3">
                                <button type="submit" name="search" value="tampilkan" class="btn btn-info">Cari</button>
                                <button type="submit" name="search" value="reset"
                                    class="btn btn-secondary">Reset</button>
                            </div>
                        </form>
                        <div class="text-right col-lg-12">
                            <a href="{{ site_url('sistem/role/add') }}" type="submit" class="btn btn-primary">Tambah Data</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th class="text-align text-center">No.</th>
                                    <th class="text-align text-center">Role</th>
                                    <th class="text-align text-center">Group</th>
                                    <th class="text-align text-center">Deskripsi</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($result))
                                    @foreach ($result as $rs)
                                        <tr>
                                            <th class="text-align text-center"> {{ $no++ }} </th>
                                            <td>{{ $rs['role_nm'] }}</td>
                                            <td class="text-align text-center">{{ $rs['group_name'] }}</td>
                                            <td>{{ $rs['role_desc'] }}</td>
                                            <td>
                                                    <a href="{{ site_url('sistem/role/edit/'.$rs['role_id']) }}" class="btn btn-success btn-rounded m-b-10 m-l-5" title="Edit"><i class="ti-pencil"></i></a>
                                                    <a href="{{ site_url('sistem/role/delete/'.$rs['role_id']) }}" class="btn btn-danger btn-rounded m-b-10 m-l-5" title="Delete"><i class="ti-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <th class="text-align text-center" colspan="7"><h3> Data kosong </h3></th>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        @if (isset($pagination))
                            {!! $pagination !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('ext_js')
<script>
    $(document).ready(function () {
        $( ".select2-single" ).select2( {
                width: '100%',
				containerCssClass: ':all:'
			} );
    });
</script>
@endpush