<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{ $title }}
            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Sistem</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">{{ $title }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<div class="container-fluid">
{{-- notif wajib ada di setiap halaman admin kecuali delete--}}
    @include('template/notif')
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card"> 
                <div class="card-body">
                    <div class="row">
                        <div class="row">
                            <h4 class="card-title col-lg-12">Daftar Permission</h4>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="bg-primary text-white">
                            <tr>
                                <th class="text-align text-center">No.</th>
                                <th class="text-align text-center">Group</th>
                                <th class="text-align text-center">Nama Role</th>
                                <th class="text-align text-center">Deskripsi Role</th>
                                <th class="text-align text-center"> </th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(!empty($result))
                                @foreach ($result as $rs)
                                    <tr>
                                        <th class="text-align text-center"> {{ $no++ }} </th>
                                        <td>{{ $rs['group_name'] }}
                                        </td>
                                        <td>{{ $rs['role_nm'] }}
                                        </td>
                                        <td>{{ $rs['role_desc'] }}</td>
                                        <td>
                                                <a href="{{ site_url('sistem/permission/edit/'.$rs['role_id']) }}" class="btn btn-success btn-rounded m-b-10 m-l-5" title="Edit"><i class="ti-pencil"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                @else
                                    <tr>
                                        <th class="text-align text-center" colspan="7"><h3> Data kosong </h3></th>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        @if (isset($pagination))
                            {!! $pagination !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('ext_js')
<script>
    $(document).ready(function () {
        $( ".select2-single" ).select2( {
                width: '100%',
				containerCssClass: ':all:'
			} );
    });
</script>
@endpush