<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"><?php echo e($title); ?>

            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="" class="text-muted">Master Data</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo e(site_url('master/produk')); ?>"
                                class="text-muted"><?php echo e($title); ?></a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Tambah Data</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>

<div class="container-fluid">
    
    <?php echo $__env->make('template/notif', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-10">
                            <h4 class="card-title">Tambah Data</h4>
                        </div>
                        <div class="col-lg-2">
                            <div class="text-right">
                                <a href="<?php echo e(site_url('master/produk')); ?>" type="submit"
                                    class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                    </div>
                    <form action="<?php echo e(site_url('master/produk/add_process')); ?>" method="POST"  enctype="multipart/form-data" >
                        <div class="form-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>Kategori </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <select name="kategori_id" id="single" class="form-control select2-single">
                                                    <?php $__currentLoopData = $kategoris; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kategori): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($kategori['kategori_id']); ?>">
                                                        <?php echo e($kategori['kategori_nama']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <label>Nama Produk </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" name="nama" class="form-control"
                                                    placeholder="Isian nama produk...">
                                            </div>
                                        </div>
                                    </div>
                                    <label>Deskripsi </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <textarea class="form-control" id="ckeditor" name="deskripsi" rows="16" cols="50"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label>Harga </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <input type="text" name="harga" class="form-control" id="formatnumber"
                                                placeholder="Isian harga...">
                                            </div>
                                        </div>
                                    </div>
                                    <label>Satuan </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <input type="text" name="satuan" class="form-control"
                                                placeholder="Isian Satuan...">
                                            </div>
                                        </div>
                                    </div>
                                    <label>Berat </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <input type="number" name="berat" class="form-control"
                                                placeholder="Dalam bentuk gram..">
                                            </div>
                                        </div>
                                    </div>
                                    <label>Gambar </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <input type="file" name="files[]" class="form-control" multiple="multiple">
                                            </div>
                                        </div>
                                    </div>
                                    <label>Aktifkan sebagai produk rekomendasi </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <input name="rekomendasi_st" type="checkbox" data-toggle="toggle"
                                                data-onstyle="primary">
                                            </div>
                                        </div>
                                    </div>
                                    <label>Stok </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <select name="stok_st" class="form-control">
                                            <option value="yes">Tersedia</option>
                                            <option value="no">Habis</option>
                                        </select>
                                            </div>
                                        </div>
                                    </div>
                                    <label>Status </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <select name="active_st" class="form-control">
                                            <option value="yes">Aktif</option>
                                            <option value="no">Tidak Aktif</option>
                                        </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success m-b-10 m-l-5"> Simpan</button>
                                        <button type="reset" class="btn btn-secondary m-b-10 m-l-5"> Reset</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->startPush('ext_js'); ?>
    <script>

        var ckeditor = CKEDITOR.replace('deskripsi',{
                    height:'600px'
        });

        CKEDITOR.disableAutoInline = true;
        CKEDITOR.inline('editable');

        $(document).ready(function () {
            $(".select2-single").select2({
                width: '100%',
                containerCssClass: ':all:'
            });
        });

        var formatnumber = document.getElementById('formatnumber');
        formatnumber.addEventListener('keyup', function () {
            var val = this.value;
            val = val.replace(/[^0-9\.]/g, '');
            if (val != "") {
                valArr = val.split('.');
                valArr[0] = (parseInt(valArr[0], 10)).toLocaleString();
                val = valArr.join('.');
            }
            this.value = val;
        });
    </script>
    <?php $__env->stopPush(); ?>