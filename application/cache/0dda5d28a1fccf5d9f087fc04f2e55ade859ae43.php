 <!-- breadcrumb start-->
 <section class="breadcrumb breadcrumb_bg">
     <div class="container">
         <div class="row justify-content-center">
             <div class="col-lg-12">
                 <div class="breadcrumb_iner">
                     <div class="breadcrumb_iner_item" style="margin-left: 8%">
                         <p>Favorit</p>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div style="margin-right:4%">
         <a href="<?php echo e(site_url('client/beranda')); ?>" type="button" class="btn btn-primary m-b-10 m-l-5">Kembali</a>
     </div>
 </section>
 <!-- breadcrumb start-->

 <!-- new arrival part here -->
 <section class="new_arrival" style="margin-top: 5%;margin-bottom: 93px">
     <div class="container" style="margin-bottom: 2%">


         <div class="row align-items-center">
             <div class="col-lg-4">
                 <div class="arrival_tittle">
                     <h2>Favorit</h2>
                 </div>
             </div>

         </div>
         <div style="margin-bottom: 10px">
             <form method="POST" enctype="multipart/form-data" action="<?php echo e(site_url('client/favorit/search')); ?>">
                 <div class="input-group">
                     <input type="text" name="produk_nama" class="form-control" placeholder="Masukkan Nama Produk"
                         <?php if(!empty($rs_search['produk_nama'])): ?> value="<?php echo e($rs_search['produk_nama']); ?>" <?php else: ?> <?php endif; ?>>
                     <div class="input-group-append">
                         <button type="submit" class="btn btn-primary" value="tampilkan" name="search">
                             <i class="fa fa-search fa-sm mr-1"></i>
                             <div class="d-none d-xs-block">Search</div>
                         </button>
                         <button value="reset" name="search" class="btn btn-dark">
                             <i class="fa text-white fa-sync fa-sm mr-1"></i>
                             <div class="d-none d-xs-block">Reset</div>
                         </button>
                     </div>
                 </div>
             </form>
         </div>
         <div class="row align-items-center">
             <div class="col-lg-8">
                 <div class="arrival_filter_item filters">

                     <ul>
                         
                         
                         <?php $__currentLoopData = $kategoris; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $rs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         <li class="controls">
                             <div class="card">
                                 <div class="card-body">
                                     <img width="35" height="35" onclick="cariKategori(<?php echo e($rs['kategori_id']); ?>)"
                                         src="<?php echo e(base_url('assets/images/logo_kategori/').$rs['logo']); ?>" />
                                 </div>
                             </div>
                         </li>
                         
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </ul>
                 </div>
             </div>
         </div>
         <div class="new_arrival_iner" style="margin-top:2%">
             <?php if(!empty($produk)): ?>
                <?php $__currentLoopData = $produk; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $rs_rekomendasi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="single_arrivel_item col-md-3 mix women men">
                    <a href="<?php echo e(site_url('client/beranda/detail/'.$rs_rekomendasi['produk_id'])); ?>">
                        <img height="230" width="150"
                            src="<?php echo e(base_url('assets/images/gambar_produk/'.$gambar[$i]['gambar_nama'])); ?>" alt="#" class="rounded">
                        <div class="hover_text">
                            <p><?php echo e($rs_rekomendasi['kategori_nama']); ?></p>
                            <h3><?php echo e($rs_rekomendasi['nama']); ?></h3>
                            <h3>Rp. <?php echo e(number_format($rs_rekomendasi['harga'])); ?></h3>
                        </div>
                    </a>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
         </div>
     </div>

 </section>
 <!-- new arrival part end -->
 <?php $__env->startPush('ext_js'); ?>
 <script>
     function cariKategori(kategori_id) {
         $.ajax({
             type: "POST",
             url: "<?php echo e(site_url('client/favorit/search_by_kategori/')); ?>",
             data: {
                 'kategori_id': kategori_id
             },
             success: function (data) {
                 location.reload();
             }
         });
     }
 </script>
 <?php $__env->stopPush(); ?>