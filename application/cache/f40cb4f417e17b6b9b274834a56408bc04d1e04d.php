<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"><?php echo e($title); ?>

            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="" class="text-muted">Transaksi</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo e(site_url('transaksi/pembelian')); ?>"
                                class="text-muted"><?php echo e($title); ?></a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Detail Kurir</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="container-fluid">
    
    <?php echo $__env->make('template/notif', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-10">
                            <h4 class="card-title">Detail Kurir</h4>
                        </div>
                        <div class="col-lg-2">
                            <div class="text-right">
                                <a href="<?php echo e(site_url('transaksi/pembelian')); ?>" type="submit"
                                    class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                    </div>
                    <nav class="navbar navbar-expand-lg navbar-dark bg-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link btn btn-primary" href="<?php echo e(site_url('transaksi/pembelian/detail/')); ?><?php echo e($result['transaksi_id']); ?>">Bukti Transfer</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn btn-primary" href="<?php echo e(site_url('transaksi/pembelian/detail_produk/')); ?><?php echo e($result['transaksi_id']); ?>">Produk</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn btn-primary" href="<?php echo e(site_url('transaksi/pembelian/detail_tujuan/')); ?><?php echo e($result['transaksi_id']); ?>">Tujuan Pengririman</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link btn btn-primary" href="<?php echo e(site_url('transaksi/pembelian/detail_kurir/')); ?><?php echo e($result['transaksi_id']); ?>">Kurir Pengiriman<span class="sr-only">(current)</span></a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="row">
                    <div class="col-lg-12 card-body">
                        <h3>Kurir Pengiriman</h3>
                        <div class="row">
                            <div class="col-lg-3">
                                <label><b>Nama Kurir </b></label>
                            </div>
                            <div class="col-lg-3">
                                <label>: <?php echo e($kurir['nama_kurir']); ?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <label><b>Jenis Pelayanan </b></label>
                            </div>
                            <div class="col-lg-3">
                                <label>: <?php echo e($kurir['service']); ?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <label><b>Deskripsi Kurir </b></label>
                            </div>
                            <div class="col-lg-3">
                                <label>: <?php echo e($kurir['desk_kurir']); ?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <label><b>Waktu Perkiraan Sampai </b></label>
                            </div>
                            <div class="col-lg-3">
                                <label>: <?php echo e($kurir['wkt_perkiraan']); ?> hari</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <label><b>Biaya Ongkos Kirim</b></label>
                            </div>
                            <div class="col-lg-3">
                                <label>: Rp. <?php echo e(number_format($kurir['biaya_ongkir'])); ?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <label><b>Resi</b></label>
                            </div>
                            <div class="col-lg-3">
                                <label>: <?php echo e($kurir['resi']); ?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <?php if(!empty($status_resi['code'])): ?>
                                    <?php if($status_resi['code']): ?>
                                    <b>Status pengiriman</b>
                                    <table class="table table-responsive">
                                        <thead>
                                            <tr>
                                                <th class="text-align text-center" width="50%">Status</th>
                                                <th class="text-align text-center" width="25%">Penerima</th>
                                                <th class="text-align text-center" width="25%">Tanggal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-align text-center">
                                                    <?php if($track['delivery_status']['status']
                                                    == 'DELIVERED'): ?>
                                                    <span class="badge badge-success">Terkirim</span>
                                                    <?php else: ?>
                                                    <span class="badge badge-danger">Tidak
                                                        dikirim</span>
                                                    <?php endif; ?>
                                                </td>
                                                <td class="text-align text-center">
                                                    <?php echo $track['delivery_status']['pod_receiver']; ?>

                                                </td>
                                                <td class="text-align text-center">
                                                    <?php echo $track['delivery_status']['pod_date']; ?> <?php echo $track['delivery_status']['pod_time']; ?>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <?php  $jumlah = count($track['manifest'])  ?>
                                    <b>Riwayat Pengiriman</B>
                                    <table class="table table-responsive">
                                        <thead>
                                            <tr>
                                                <th class="text-align text-center" width="5%">No.</th>
                                                <th class="text-align text-center" width="25%">Tanggal</th>
                                                <th class="text-align text-center" width="20%">Kota</th>
                                                <th class="text-align text-center" width="50%">Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for($i = 0; $i < $jumlah; $i++): ?> <tr>
                                                <th class="text-align text-center" width="5%"> <?php echo e($i+1); ?> </th>
                                                <th class="text-align">
                                                    <?php echo $track['manifest'][$i]['manifest_date']; ?><br>
                                                    <?php echo $track['manifest'][$i]['manifest_time']; ?>

                                                </th>
                                                <th class="text-align"><?php echo $track['manifest'][$i]['city_name']; ?></th>
                                                <th class="text-align"><?php echo $track['manifest'][$i]['manifest_description']; ?></th>
                                                </tr>
                                                <?php endfor; ?>
                                        </tbody>
                                    </table>
                                    <?php elseif($status_resi['code'] == 400): ?>
                                    <div class="alert alert-danger" role="alert" style="margin-bottom:5%">
                                        <strong> Nomor resi yang diinputkan salah
                                            !</strong>
                                    </div>
                                    <?php endif; ?>
                                    <?php else: ?>
                                    Resi belum diinputkan !
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div><!-- /# column -->
                </div><!-- /# row -->
                    </div>
            </div>
        </div>
    </div>
</div>