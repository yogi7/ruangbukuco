 <!-- breadcrumb start-->
 <section class="breadcrumb breadcrumb_bg">
     <div class="container">
         <div class="row justify-content-center">
             <div class="col-lg-12">
                 <div class="breadcrumb_iner">
                     <div class="breadcrumb_iner_item">
                         <p>Transaksi</p>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div style="margin-right:4%">
         <a href="<?php echo e(site_url('client/beranda')); ?>" type="button" class="btn btn-primary m-b-10 m-l-5">Kembali</a>
     </div>
 </section>
 <!-- breadcrumb start-->


 <!-- new arrival part here -->
 <section class="new_arrival section_padding">
     <div class="container">
         <div class="row align-items-center">
             <div class="col-lg-8" style="margin-bottom:3%">
                 <div class="arrival_tittle">
                     <h2>List Transaksi</h2>
                 </div>
             </div>
         </div>
     </div>
     <form action="<?php echo e(site_url('client/Pembayaran/checkout_process')); ?>" method="post">
         <div class="row">
             <div class="col-lg-9">
                 <div class="container">
                     <!-- tampilan mobile -->
                     <div class="col-lg-6 col-md-6 d-lg-none d-block">
                     <div class="cart_inner">
                             <div class="table-responsive">
                                 <table class="table">
                                     <thead>
                                         <tr>
                                             <th scope="col">Transaksi</th>
                                             <th scope="col"></th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                         <div id="divCheckbox" style="display: none;">
                                             <?php echo e($prev_nil = 0); ?>

                                         </div>
                                         <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $rs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                         <tr>
                                             <td>
                                                 <div class="media">
                                                     <div class="d-flex">
                                                         <img height="50" width="50"
                                                             src="<?php echo e(base_url('assets/images/gambar_produk/'.$gambar[$key]['gambar_nama'])); ?>" />
                                                     </div>
                                                     <div class="media-body">
                                                         <p>
                                                             <h5><b><?php echo e($rs['transaksi_id']); ?></b></h5>
                                                             <div style="margin:2%">
                                                                 <h6>Rp. <?php echo e(number_format($rs['subtotal'])); ?></h6>
                                                             </div>
                                                             <div style="margin:2%">
                                                                 <h6>Batas bayar: <br>
                                                             </div>
                                                             <div style="margin:2%">
                                                                 <?php  $date=date_create($rs['tgl_batas_bayar']); echo
                                                                 date_format($date, 'd F Y');  ?>
                                                             </div>
                                                             <div style="margin:2%">
                                                                 <?php  echo substr($rs['tgl_batas_bayar'], 11, 8);
                                                                  ?>
                                                                 </h6>
                                                             </div>
                                                             <?php if($rs['transaksi_st'] == 'dibeli'): ?>
                                                     <span class="badge badge-pill badge-warning">Belum Dibayar</span>
                                                     <?php elseif($rs['transaksi_st'] == 'kirim_bukti'): ?>
                                                     <span class="badge badge-pill badge-info">Menunggu konfirmasi
                                                         bukti</span>
                                                     <?php elseif($rs['transaksi_st'] == 'dibayar'): ?>
                                                     <span class="badge badge-pill badge-success">Telah dibayar</span>
                                                     <?php else: ?>
                                                     <span class="badge badge-pill badge-danger">Dibatalkan</span>
                                                     <?php endif; ?> 
                                                         </p>
                                                     </div>
                                                 </div>
                                             </td>
                                             <td>
                                                 <a class="btn_3" 
                                                     href="<?php echo e(site_url('client/pembayaran/detail_transaksi/'.$rs['transaksi_id'])); ?>">Detail</a>
                                             </td>
                                         </tr>
                                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                     </div>
                     <!-- tampilan website -->
                     <div class="col-lg-12 col-md-12 d-none d-lg-block" style="margin-left:12%">
                         <div class="cart_inner">
                             <div class="table-responsive">
                                 <table class="table">
                                     <thead>
                                         <tr>
                                             <th scope="col">Transaksi</th>
                                             <th scope="col">Status</th>
                                             <th scope="col"></th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                         <div id="divCheckbox" style="display: none;">
                                             <?php echo e($prev_nil = 0); ?>

                                         </div>
                                         <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $rs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                         <tr>
                                             <td>
                                                 <div class="media">
                                                     <div class="d-flex">
                                                         <img height="130" width="150"
                                                             src="<?php echo e(base_url('assets/images/gambar_produk/'.$gambar[$key]['gambar_nama'])); ?>" />
                                                     </div>
                                                     <div class="media-body">
                                                         <p>
                                                             <h4><b><?php echo e($rs['transaksi_id']); ?></b></h4>
                                                             <div style="margin:2%">
                                                                 <h5>Rp. <?php echo e(number_format($rs['subtotal'])); ?></h5>
                                                             </div>
                                                             <div style="margin:2%">
                                                                 <h5>Batas bayar: <br>
                                                             </div>
                                                             <div style="margin:2%">
                                                                 <?php  $date=date_create($rs['tgl_batas_bayar']); echo
                                                                 date_format($date, 'd F Y');  ?>
                                                             </div>
                                                             <div style="margin:2%">
                                                                 <?php  echo substr($rs['tgl_batas_bayar'], 11, 8);
                                                                  ?>
                                                                 </h5>
                                                             </div>

                                                         </p>
                                                     </div>
                                                 </div>
                                             </td>
                                             <td>
                                                 <h4>
                                                     <?php if($rs['transaksi_st'] == 'dibeli'): ?>
                                                     <span class="badge badge-pill badge-warning">Belum Dibayar</span>
                                                     <?php elseif($rs['transaksi_st'] == 'kirim_bukti'): ?>
                                                     <span class="badge badge-pill badge-info">Menunggu konfirmasi
                                                         bukti</span>
                                                     <?php elseif($rs['transaksi_st'] == 'dibayar'): ?>
                                                     <span class="badge badge-pill badge-success">Telah dibayar</span>
                                                     <?php else: ?>
                                                     <span class="badge badge-pill badge-danger">Dibatalkan</span>
                                                     <?php endif; ?> </h4>
                                             </td>
                                             <td>

                                             </td>
                                             <td>
                                                 <a class="btn_1 checkout_btn_1"
                                                     href="<?php echo e(site_url('client/pembayaran/detail_transaksi/'.$rs['transaksi_id'])); ?>">Detail</a>
                                             </td>
                                         </tr>
                                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </form>

 </section>
 <!-- new arrival part end -->