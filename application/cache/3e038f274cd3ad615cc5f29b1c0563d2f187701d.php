<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><?php echo e($title); ?></h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html">Edit Banner</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    
    <?php echo $__env->make('template/notif', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="card-group">
        <div class="card">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <h3 class="text-dark mb-1 font-weight-medium"> Banner </h3>
                    </div>
                </div>
                <hr>
                <?php if($result != ''): ?>
                <center>
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <?php 
                        $i = 1;
                        $o = 1;
                         ?>
                        <ol class="carousel-indicators">
                            <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gbr_banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li data-target="#myCarousel" data-slide-to="<?php echo e($o++); ?>" class="<?php if($o==1): ?> <?php echo e('active'); ?> <?php endif; ?>"></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gbr_banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="carousel-item <?php if($i==1): ?> <?php echo e('active'); ?> <?php endif; ?> " id="<?php echo e($i++); ?>">
                                <img class="w-100" style="height: 700px" src="<?php echo e(base_url('assets/images/banner/'.$gbr_banner['nama_gambar'])); ?>">
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </center>
                <h3 style="margin-top:2%" class="text-dark mb-1 font-weight-medium"> List Banner </h3>
                    <!-- /# gambar banner -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Gambar Banner</h4>
                                    <br><span class="ti-info-alt" style="color:yellow"></span><span"> Klik gambar untuk menghapus banner</span>
                                </div>
                                <div class="card-body">
                                    <div class="row" style="margin-top:5px">
                                        <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gbr_banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-md-3">
                                            <div class="thumbnail">
                                                <a data-toggle="modal"
                                                    data-id="<?php echo e($gbr_banner['nama_gambar']); ?>"
                                                    class="open-modal" href="#modal">
                                                    <img src="<?php echo e(base_url('assets/images/banner/'.$gbr_banner['nama_gambar'])); ?>"
                                                        alt="Lights" style="width:300px;height:150px">
                                                </a>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>
                <?php endif; ?>
                    <!-- /# gambar banner -->
                    <form action="<?php echo e(site_url('setclient/banner/add_process')); ?>" method="post"
                    enctype="multipart/form-data">
                    <div class="card-body">
                        <div class="basic-form" style="margin-top:20px">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tambah Gambar Banner<sup style="color:red">*</sup>
                                </label>
                                <input type="file" name="files[]" class="form-control"
                                    multiple="multiple">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class=" form-group">
                            <div class="col-lg-12">
                                <label class="control-label"><b> Tanggal Terakhir Upload </b></label>
                            </div>
                            <div class="col-lg-12">
                            <?php if($result != ''): ?>
                                <label class="control-label"><?php echo e($rs_last_date['mdd']); ?></label>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-12">
                        <div class="text-right">
                            <button type="submit" class="btn btn-success m-b-10 m-l-5"> Simpan</button>
                            <button type="reset" class="btn btn-secondary m-b-10 m-l-5"> Reset</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
        <!-- Modal -->
        <div class="modal fade" id="open-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">konfirmasi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Hapus gambar <label id="nm"></label> ?
                        <input hidden name="nama_gambar" id="nama_gambar" value="nama_gambar">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-primary"
                            onclick="hapusGambar()">Hapus</button>
                    </div>
                </div>
            </div>
        </div>
        <?php $__env->startPush('ext_js'); ?>
        <script>
            $(document).on("click", ".open-modal", function () {
                var nama_gambar = $(this).data('id');
                $(".modal-body #nama_gambar").val(nama_gambar);
                document.getElementById("nm").innerHTML = nama_gambar;
                // As pointed out in comments, 
                // it is unnecessary to have to manually call the modal.
                $('#open-modal').modal('show');
            });

            //hapus gambar
            function hapusGambar() {
                var nama_gambar = $(".modal-body #nama_gambar").val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo e(site_url('setclient/banner/hapus_banner/')); ?>",
                    data: {
                        'nama_gambar': nama_gambar
                    },
                    success: function (data) {
                        if (data == "true") {
                            alert("Data berhasil dihapus.");
                            location.reload();
                        } else {
                            alert("Data gagal dihapus !");
                            location.reload();
                        }
                    }
                });
            }
        </script>
        <?php $__env->stopPush(); ?>