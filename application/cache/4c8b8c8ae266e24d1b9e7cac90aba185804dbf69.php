<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"><?php echo e($title); ?>

            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="" class="text-muted">Transaksi</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo e(site_url('transaksi/pembelian')); ?>"
                                class="text-muted"><?php echo e($title); ?></a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Detail Produk</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="container-fluid">
    
    <?php echo $__env->make('template/notif', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-10">
                            <h4 class="card-title">Detail Produk</h4>
                        </div>
                        <div class="col-lg-2">
                            <div class="text-right">
                                <a href="<?php echo e(site_url('transaksi/pembelian')); ?>" type="submit"
                                    class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                    </div>
                    <nav class="navbar navbar-expand-lg navbar-dark bg-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link btn btn-primary" href="<?php echo e(site_url('transaksi/pembelian/detail/')); ?><?php echo e($result['transaksi_id']); ?>">Bukti Transfer</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link btn btn-primary" href="<?php echo e(site_url('transaksi/pembelian/detail_produk/')); ?><?php echo e($result['transaksi_id']); ?>">Produk<span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn btn-primary" href="<?php echo e(site_url('transaksi/pembelian/detail_tujuan/')); ?><?php echo e($result['transaksi_id']); ?>">Tujuan Pengririman</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn btn-primary" href="<?php echo e(site_url('transaksi/pembelian/detail_kurir/')); ?><?php echo e($result['transaksi_id']); ?>">Kurir Pengiriman</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="table-responsive" style="margin-top:1%">
                        <table class="table">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th>Gambar</th>
                                    <th>Nama Produk</th>
                                    <th>Harga</th>
                                    <th>Jumlah</th>
                                    <th>Total Harga</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($result)): ?>
                                <?php $__currentLoopData = $produk; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $rs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><img height="130" width="150"
                                            src="<?php echo e(base_url('assets/images/gambar_produk/'.$gambar[$key]['gambar_nama'])); ?>" /><br>
                                        </td>
                                        <td><b><?php echo e($rs['nama']); ?></b></td>
                                        <td>Rp. <?php echo number_format($rs['harga']); ?></td>
                                        <td><?php echo e($rs['jumlah']); ?></td>
                                        <td>Rp. <?php echo e(number_format($rs['harga'] * $rs['jumlah'])); ?></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td> Sub Total :</td>
                                        <td>Rp. <?php echo e(number_format($result['subtotal'])); ?></td>
                                    </tr>
                                <?php else: ?>
                                    <tr>
                                        <th class="text-align text-center" colspan="7"><h3> Data kosong </h3></th>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>