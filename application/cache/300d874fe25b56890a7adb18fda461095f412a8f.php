<div class="content-wrap">
    <div class="main">
        <!-- breadcrum -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8 p-0">
                    <div class="page-header">
                        <div class="page-title">
                            <h1><?php echo e($title); ?></h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 p-0">
                    <div class="page-header">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="#">Master</a></li>
                                <li class="active"><?php echo e($title); ?></li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div>
                </div>
                <!-- akhir breadcrum -->
                <div class="main-content">

                    <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Daftar Registrasi Anggota</h4>
                                    <div class="card-header-right-icon">
                                    </div>
                                    <hr>
                                </div>
                                <div class="card-body" style="margin-top:20px">
                                    <div class="card-content">
                                        <div class="horizontal-form-elements">
                                        </div>

                                        
                                        <?php echo $__env->make('template/notif', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                                        <hr>
                                        <table class="table table-responsive table-striped">
                                            <thead>
                                                <tr>
                                                    <th class="text-align text-center">No.</th>
                                                    <th class="text-align text-center">Nama</th>
                                                    <th class="text-align text-center">Produk</th>
                                                    <th class="text-align text-center">Tanggal Pendaftaran</th>
                                                    <th class="text-align text-center">Status</th>
                                                    <th class="text-align text-center">Tanggal Verifikasi</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <th class="text-align text-center"> <?php echo e($no++); ?> </th>
                                                    <td><?php echo e($rs['nama']); ?></td>
                                                    <td><?php echo e($rs['produk_nama']); ?></td>
                                                    <td class="text-align text-center">  <?php  $date=date_create($rs['mdd']); echo date_format($date, 'd F Y'); ?> </td>
                                                    <td class="text-align text-center">
                                                        <?php if($rs['status_transfer'] == 'daftar'): ?>
                                                        <span class="badge badge-danger">Belum Ditransfer</span>
                                                        <?php elseif($rs['status_transfer'] == 'transfer'): ?>
                                                        <span class="badge badge-warning">Belum diverifikasi</span>
                                                        <?php elseif($rs['status_transfer'] == 'diverifikasi'): ?>
                                                        <span class="badge badge-success">Telah diverifikasi</span>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td class="text-align text-center">  <?php  $date=date_create($rs['mdd']); echo date_format($date, 'd F Y'); ?> </td>
                                                    <td>
                                                        <a href="<?php echo e(site_url('master/register_anggota_bisnis/detail/'.$rs['transaksi_bisnis_id'])); ?>"
                                                            type="button" class="btn btn-info btn-rounded m-b-10 m-l-5"
                                                            title="Detail"><i class="ti-eye"></i></a>
                                                    </td>
                                                </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>
                                        <div class="text-right">
                                            <?php if(isset($pagination)): ?>
                                            <ul class="pagination pagination-sm">
                                                <li class="page-item"><a class="page-link" href="#"><?php echo $pagination; ?></a></li>
                                            </ul>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>