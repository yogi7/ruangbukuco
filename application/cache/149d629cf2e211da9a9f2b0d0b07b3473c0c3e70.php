<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"><?php echo e($title); ?>

            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Transaksi</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page"><?php echo e($title); ?></li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<div class="container-fluid">

    <?php echo $__env->make('template/notif', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card"> 
                <div class="card-body">
                    <div class="row">
                        <div class="row">
                            <h4 class="card-title col-lg-12">Daftar Resi</h4>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th class="text-align text-center" width="10%">No.</th>
                                    <th class="text-align text-center" width="25%">Foto Produk</th>
                                    <th class="text-align text-center" width="25%">Pembeli</th>
                                    <th class="text-align text-center" width="25%">Tanggal Transaksi</th>
                                    <th class="text-align text-center" width="25%">Total</th>
                                    <th class="text-align text-center" width="25%">Status</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($result)): ?>
                                <?php $nomor = 1;?>
                                    <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $rs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <th class="text-align text-center" width="10%"> <?php echo e($no++); ?> </th>
                                        <th class="text-align text-center" width="25%"><img width="100"
                                                height="100"
                                                src="<?php echo e(base_url('assets/images/gambar_produk/').$gambar[$key]['gambar_nama']); ?>"/>
                                        </th>
                                        <th class="text-align text-center" width="10%"> <?php echo e($rs['nama']); ?>

                                        </th>
                                        <td width="25%">
                                            <?php  $date=date_create($rs['mdd']); echo
                                            date_format($date, 'd F Y');  ?>
                                            <br>
                                            <?php  echo substr($rs['mdd'], 11, 8);  ?>
                                        </td>
                                        <td class="text-align text-center" width="25%">
                                            Rp.
                                            <?php echo e(number_format($rs['subtotal'] + $rs['kode_unik'] + $rs['biaya_ongkir'])); ?>

                                        </td>
                                        <td class="text-align text-center" width="25%">
                                            <?php if($rs['transaksi_st'] == 'dibayar'): ?>
                                                <span class="badge badge-warning">Resi belum diinput</span>
                                            <?php elseif($rs['transaksi_st'] == 'dikirim'): ?>
                                                <span class="badge badge-success">Resi telah diinput</span>
                                            <?php endif; ?>
                                        </td>
                                        <td width="10%">
                                            <a href="<?php echo e(site_url('transaksi/resi/detail/'.$rs['transaksi_id'])); ?>"
                                                class="btn btn-info btn-rounded m-b-10 m-l-5"
                                                title="Detail"><i class="ti-eye"></i> </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <tr>
                                        <th class="text-align text-center" colspan="7"><h3> Data kosong </h3></th>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <?php if(isset($pagination)): ?>
                                <?php echo $pagination; ?>

                            <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>