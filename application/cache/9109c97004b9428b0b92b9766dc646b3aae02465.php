<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"><?php echo e($title); ?>

            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="" class="text-muted">Master Data</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo e(site_url('master/produk')); ?>"
                                class="text-muted"><?php echo e($title); ?></a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Hapus Data</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="container-fluid">
    
    <?php echo $__env->make('template/notif', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-10">
                            <h4 class="card-title">Hapus Data</h4>
                        </div>
                        <div class="col-lg-2">
                            <div class="text-right">
                                <a href="<?php echo e(site_url('master/produk')); ?>" type="submit"
                                    class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="alert alert-danger" style="margin-top: 20px">
                                    <label>Data yang telah terhapus tidak akan dapat dikembalikan lagi,<strong> Yakin
                                            hapus data ini?</strong></label>
                                </div>
                    <form action="<?php echo e(site_url('master/produk/delete_process')); ?>" method="post">
                    <input type="hidden" name="produk_id" value="<?php echo e($result['produk_id']); ?>">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label><b>Kategori </b></label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo e($result['kategori_nama']); ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <label><b>Nama Produk </b></label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo e($result['nama']); ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <label><b>Deskripsi </b></label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo e($result['deskripsi']); ?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label><b>Harga </b></label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Rp. <?php echo number_format($result['harga']); ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <label><b>Satuan </b></label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo e($result['satuan']); ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <label><b>Berat </b></label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo e($result['berat']); ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <label><b>Gambar </b></label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <?php $__currentLoopData = $gambar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <img width="150" height="150"
                                                    src="<?php echo e(base_url('assets/images/gambar_produk/').$i['gambar_nama']); ?>" />
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <label><b>Produk Rekomendasi </b></label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <?php if($result['rekomendasi_st']=='yes' ): ?>
                                            <span class="badge badge-success">Ya</span>
                                            <?php else: ?>
                                            <span class="badge badge-danger">Tidak</span>
                                            <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <label><b>Stok </b></label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <?php if($result['stok_st']=='yes' ): ?>
                                            <span class="badge badge-success">Tersedia</span>
                                            <?php else: ?>
                                            <span class="badge badge-danger">Tidak Tersedia</span>
                                            <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <label><b>Status Produk </b></label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <?php if($result['active_st']=='yes' ): ?>
                                            <span class="badge badge-success">Aktif</span>
                                            <?php else: ?>
                                            <span class="badge badge-danger">Tidak Aktif</span>
                                            <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-6">
                                    <label><b> Created by </b> </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo e($result['mdb_name']); ?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label><b> Date update </b> </label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo e($result['mdd']); ?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="col-lg-12">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-danger m-b-10 m-l-5"> Hapus</button>
                                    </div>
                                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>