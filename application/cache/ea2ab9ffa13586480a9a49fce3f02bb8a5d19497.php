<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"><?php echo e($title); ?>

            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Sistem</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page"><?php echo e($title); ?></li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<div class="container-fluid">

    <?php echo $__env->make('template/notif', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card"> 
                <div class="card-body">
                <div class="row">
                        <div class="col-lg-10">
                            <h4 class="card-title">Tambah Data</h4>
                        </div>
                        <div class="col-lg-2">
                            <div class="text-right">
                                <a href="<?php echo e(site_url('sistem/permission')); ?>" type="submit"
                                    class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                    <form class="form-horizontal" action="<?php echo e(site_url('sistem/permission/edit_process')); ?>" method="post">
                    <input type="hidden" name="role_id" value="<?php echo e($result['role_id']); ?>">
                        <table class="table">
                            <thead class="bg-primary text-white">
                            <tr>
                                <th  class="text-align text-center" width="10%">  <input type="checkbox" id="checked-all-menu" class="checked-all-menu">  Semua</th>
                                <th class="text-align text-center" width="50%">Judul Navigasi</th>
                                <th class="text-align text-center" width="10%">Create</th>
                                <th class="text-align text-center" width="10%">Read</th>
                                <th class="text-align text-center" width="10%">Update</th>
                                <th class="text-align text-center" width="10%">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php echo $list_menu; ?>

                            </tbody>
                        </table>
                        <hr>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label class="control-label"><b> Created by
                                            </b></label>
                                    </div>
                                    <div class="col-lg-12">
                                        <label
                                            class="control-label"><?php echo e($result['mdb_name']); ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label class="control-label"><b> Date update
                                            </b></label>
                                    </div>
                                    <div class="col-lg-12">
                                        <label
                                            class="control-label"><?php echo e($result['mdd']); ?></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="col-lg-12">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success m-b-10 m-l-5"> Simpan</button>
                                        <button type="reset" class="btn btn-secondary m-b-10 m-l-5"> Reset</button>
                                    </div>
                                </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->startPush('ext_js'); ?>
            <script>
                $(document).ready(function () {
                    $(".select2-single").select2({
                        width: '100%',
                        containerCssClass: ':all:'
                    });
                });
            </script>

            <script type="text/javascript">
                $(function () {
                    $(".checked-all").click(function () {
                        var status = $(this).is(":checked");
                        if (status === true) {
                            $(".r" + $(this).val()).prop('checked', true);
                        } else {
                            $(".r" + $(this).val()).prop('checked', false);
                        }
                    });
                    $(".checked-all-menu").click(function () {
                        var status = $(this).is(":checked");
                        if (status === true) {
                            $(".r-menu").prop('checked', true);
                        } else {
                            $(".r-menu").prop('checked', false);
                        }
                    });
                    $(".select-2").select2();
                });
            </script>
            <?php $__env->stopPush(); ?>