<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"><?php echo e($title); ?>

            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Transaksi</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page"><?php echo e($title); ?></li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<div class="container-fluid">

    <?php echo $__env->make('template/notif', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card"> 
                <div class="card-body">
                    <div class="row">
                        <div class="row">
                            <h4 class="card-title col-lg-12">Daftar Pembelian</h4>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th>No</th>
                                    <th>Pembelian</th>
                                    <th>Nama Pembeli</th>
                                    <th>Jumlah Bayar</th>
                                    <th>Batas Bayar</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($result)): ?>
                                <?php $nomor = 1;?>
                                    <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $rs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <th scope="row"><?php echo e($nomor++); ?></th>
                                        <td>
                                            <img height="130" width="150"
                                            src="<?php echo e(base_url('assets/images/gambar_produk/'.$gambar[$key]['gambar_nama'])); ?>" /><br>
                                            <b><?php echo e($rs['transaksi_id']); ?></b><br>
                                        </td>
                                        <td><?php echo e($rs['nama']); ?></td>
                                        <td>Rp. <?php echo e(number_format($rs['subtotal'] + $rs['kode_unik'] + $rs['biaya_ongkir'])); ?></td>
                                        <td> 
                                            <?php  $date=date_create($rs['tgl_batas_bayar']); echo date_format($date, 'd F Y');
                                             ?> <?php  echo substr($rs['tgl_batas_bayar'], 11, 8);  ?>
                                        </td>
                                        <td style="text-align:left">
                                            <?php if($rs['transaksi_st'] == 'dibeli'): ?>
                                            <span class="badge badge-pill badge-warning">Belum Dibayar</span>
                                            <?php elseif($rs['transaksi_st'] == 'kirim_bukti'): ?>
                                                <span class="badge badge-pill badge-info">Menunggu konfirmasi bukti</span>
                                            <?php elseif($rs['transaksi_st'] == 'dibayar'): ?>
                                                <span class="badge badge-pill badge-success">Telah dibayar</span>
                                            <?php else: ?>
                                                <span class="badge badge-pill badge-danger">Dibatalkan</span>
                                            <?php endif; ?> 
                                        </td>
                                        <td>
                                            <a href="<?php echo e(site_url('transaksi/pembelian/detail/'.$rs['transaksi_id'])); ?>" type="button" class="btn btn-info btn-rounded m-b-10 m-l-5" title="Detail"><i class="ti-eye"></i> </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <tr>
                                        <th class="text-align text-center" colspan="7"><h3> Data kosong </h3></th>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <?php if(isset($pagination)): ?>
                                <?php echo $pagination; ?>

                            <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>