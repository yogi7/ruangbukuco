<!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner">
                        <div class="breadcrumb_iner_item">
                            <p><?php echo e($title); ?> / Cari</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-right:4%">
            <a href="<?php echo e(site_url('client/beranda')); ?>" type="button" class="btn btn-primary m-b-10 m-l-5">Kembali</a>
        </div>
    </section>
    <!-- breadcrumb start-->
        
    <!--================Category Product Area =================-->
    <section class="cat_product_area section_padding border_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="left_sidebar_area">
                        <aside class="left_widgets p_filter_widgets sidebar_box_shadow">
                            <div class="l_w_title">
                                <h3>List Kategori</h3>
                            </div>
                            <div class="widgets_inner">
                                <ul class="list">
                                    <?php $__currentLoopData = $kategoris; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $rs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li>
                                        <a href="<?php echo e(site_url('client/beranda/search_item/'.$rs['kategori_id'])); ?>"><?php echo e($rs['kategori_nama']); ?></a>
                                    </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </aside>
                        <!-- <aside class="left_widgets p_filter_widgets price_rangs_aside sidebar_box_shadow">
                            <div class="l_w_title">
                                <h3>Range Harga</h3>
                            </div>
                            <div class="widgets_inner">
                                <input style="margin-top:2%" type="text" class="col-lg-11 formatnumber form-control min_harga" value="0">
                                <b>Sampai</b> <input type="text" class="col-lg-11 form-control formatnumber max_harga" value="<?php echo e($max_harga); ?>">
                            </div>
                        </aside> -->
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="row">
                    
                        <!-- tampilan mobile -->
                        <div class="d-lg-none d-block" style="margin-left:3%;margin-right:3%;margin-top:1%">
                            <form action="<?php echo e(site_url('client/beranda/search_item')); ?>" method="POST">
                                <input type="hidden" name="min_harga" value="0" id="min_harga_search">
                                <input type="hidden" name="max_harga" value="<?php echo e($max_harga); ?>" id="max_harga_search">
                                <div class="row">
                                    <div class="d-flex" style="margin-top:3%;margin-left:3%;margin-bottom:3%">

                                        <div class="row" style="margin-right:2%">
                                            <div class="col-md-12">
                                                <?php if($rs_search_kategori['kategori_id'] != ''): ?>
                                                    <input type="hidden" name="kategori_id" id="kategori_id" value="<?php echo e($rs_search_kategori['kategori_id']); ?>">
                                                <?php endif; ?>
                                                <h3><b><?php if($rs_search_kategori['kategori_nama'] == ''): ?> Tampilkan Semua <?php else: ?> <?php echo e($rs_search_kategori['kategori_nama']); ?> <?php endif; ?> </b></h3>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                    <input type="text" class="form-control" id="nama" value="<?php echo e($rs_search_kategori['nama']); ?>" name="nama" placeholder="Cari sesuatu">
                                            </div>
                                            <div class="col-md-3">
                                                <div class="product_bar_single">
                                                    <select name="urutkan" class="wide select2-single getSearch">
                                                        <option data-display="Default sorting" value="">Urutkan</option>
                                                        <option value="terbaru">Terbaru</option>
                                                        <option value="terdahulu">Terdahulu</option>
                                                        <option value="termurah">Termurah</option>
                                                        <option value="termahal">Termahal</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-3 row">
                                            <div class="col-lg-6">
                                                <button class="genric-btn primary-border circle" name="cari" value="cari"><i class="ti-search"></i></button>
                                            </div>
                                            <div class="col-lg-6">
                                                <button class="genric-btn danger-border circle" name="cari" value="reset"><i class="ti-close"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <!-- Tampilan Web -->
                        <div class="d-none d-lg-block">
                            <form action="<?php echo e(site_url('client/beranda/search_item')); ?>" method="POST">
                                <input type="hidden" name="min_harga" value="0" id="min_harga_search">
                                <input type="hidden" name="max_harga" value="<?php echo e($max_harga); ?>" id="max_harga_search">
                                <div class="row">
                                    <div class="product_top_bar d-flex">
                                        <div class="col-lg-3">
                                            <?php if($rs_search_kategori['kategori_id'] != ''): ?>
                                                <input type="hidden" name="kategori_id" id="kategori_id" value="<?php echo e($rs_search_kategori['kategori_id']); ?>">
                                            <?php endif; ?>
                                            <h3><b><?php if($rs_search_kategori['kategori_nama'] == ''): ?> Tampilkan Semua <?php else: ?> <?php echo e($rs_search_kategori['kategori_nama']); ?> <?php endif; ?> </b></h3>
                                        </div>
                                        <div class="col-lg-3">
                                                <input type="text" class="form-control" id="nama" value="<?php echo e($rs_search_kategori['nama']); ?>" name="nama" placeholder="Cari sesuatu">
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="product_bar_single">
                                                <select name="urutkan" class="wide select2-single getSearch">
                                                    <option data-display="Default sorting" value="">Urutkan</option>
                                                    <option value="terbaru">Terbaru</option>
                                                    <option value="terdahulu">Terdahulu</option>
                                                    <option value="termurah">Termurah</option>
                                                    <option value="termahal">Termahal</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 row">
                                            <div class="col-lg-6">
                                                <button class="genric-btn primary-border circle" name="cari" value="cari"><i class="ti-search"></i></button>
                                            </div>
                                            <div class="col-lg-6">
                                                <button class="genric-btn danger-border circle" name="cari" value="reset"><i class="ti-close"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <?php $__currentLoopData = $produk; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $rs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-lg-4 col-sm-6">
                            <a href="<?php echo e(site_url('client/beranda/detail/'.$rs['produk_id'])); ?>">
                            <div class="single_category_product">
                                <div class="single_category_img">
                                    <img height="250" width="150" src="<?php echo e(base_url('assets/images/gambar_produk/'.$gambar[$key]['gambar_nama'])); ?>" alt="">
                                    <div class="category_product_text">
                                        <a href="single-product.html"><h5><?php echo e($rs['nama']); ?></h5></a>
                                        <p>Rp. <?php echo e(number_format($rs['harga'])); ?></p>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Category Product Area =================-->
<!-- new arrival part end -->
<?php $__env->startPush('ext_js'); ?>
<script>
    var comments = document.getElementsByClassName('formatnumber');
    var numComments = comments.length;
    for (var i = 0; i < numComments; i++) {
        comments[i].addEventListener(
            'keyup', function () {
            var val = this.value;
            val = val.replace(/[^0-9\.]/g, '');
            if (val != "") {
                valArr = val.split('.');
                valArr[0] = (parseInt(valArr[0], 10)).toLocaleString();
                val = valArr.join('.');
            }
            this.value = val;
        }
        );
    }

    var comments_min = document.getElementsByClassName('min_harga');
    var numComments_min = comments_min.length;
    for (var i = 0; i < numComments_min; i++) {
        comments_min[i].addEventListener(
            'keyup', function () {
            $('#min_harga_search').val(this.value);
        }
        );
    }

    var comments_max = document.getElementsByClassName('max_harga');
    var numComments_max = comments_max.length;
    for (var i = 0; i < numComments_max; i++) {
        comments_max[i].addEventListener(
            'keyup', function () {
            $('#max_harga_search').val(this.value);
        }
        );
    }
    
</script>
<?php $__env->stopPush(); ?>