<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><?php echo e($title); ?></h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html">Edit Logo</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    
    <?php echo $__env->make('template/notif', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="card-group">
        <div class="card">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <h3 class="text-dark mb-1 font-weight-medium"> Logo </h3>
                    </div>
                </div>
                <hr>
                <center>
                    <img class="w-100" style="height: 700px" src="<?php echo e(base_url('assets/images/logo/'.$pref_toko['img_name'])); ?>">
                    <!-- <img class="w-100" style="height: 700px" src="<?php echo e(base_url('assets/images/logo/mandiri-logo.png')); ?>"> -->
                </center>
                <h3 style="margin-top:2%" class="text-dark mb-1 font-weight-medium"> Edit logo </h3>
                    <form action="<?php echo e(site_url('setclient/pref_toko/edit_process')); ?>" method="post"
                    enctype="multipart/form-data">
                    <div class="card-body">
                        <div class="basic-form" style="margin-top:20px">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Pilih Logo<sup style="color:red">*</sup>
                                </label>
                                <input type="file" name="files" class="form-control">
                            </div>
                        </div>
                        <div class="basic-form" style="margin-top:20px">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nama Toko<sup style="color:red">*</sup>
                                </label>
                                <input type="text" name="nama_toko" value="<?php echo e($pref_toko['value_pref']); ?>" class="form-control">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="col-lg-12">
                        <div class="text-right">
                            <button type="submit" class="btn btn-success m-b-10 m-l-5"> Simpan</button>
                            <button type="reset" class="btn btn-secondary m-b-10 m-l-5"> Reset</button>
                        </div>
                    </div>
                    </form>
            </div>
        </div>
    </div>