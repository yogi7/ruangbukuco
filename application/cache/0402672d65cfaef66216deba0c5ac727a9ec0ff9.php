<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"><?php echo e($title); ?>

            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Master Data</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page"><?php echo e($title); ?></li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<div class="container-fluid">

    <?php echo $__env->make('template/notif', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card"> 
                <div class="card-body">
                    <div class="row">
                        <div class="row">
                            <h4 class="card-title col-lg-12">Daftar Produk</h4>
                        </div>
                        <form class="col-lg-12 row" action="<?php echo e(site_url('master/produk/search_process')); ?>" method="POST">
                            <div class="col-lg-3">
                                <select name="kategori_nama" id="single" class="form-control select2-single">
                                    <option value="%">Kategori....</option>
                                    <?php $__currentLoopData = $kategoris; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kategori): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($kategori['kategori_nama']); ?>">
                                        <?php echo e($kategori['kategori_nama']); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <input type="text" name="nama" class="form-control" placeholder="Isian nama produk..." value="<?php echo e($search['a.nama']); ?>">
                            </div>
                            <div class="col-lg-3">
                                <button type="submit" name="search" value="tampilkan" class="btn btn-info">Cari</button>
                                <button type="submit" name="search" value="reset"
                                    class="btn btn-secondary">Reset</button>
                            </div>
                        </form>
                        <div class="text-right col-lg-12">
                            <a href="<?php echo e(site_url('master/produk/add')); ?>" type="submit" class="btn btn-primary">Tambah Data</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th class="text-align text-center" width="5%">No.</th>
                                    <th class="text-align text-center" width="15%">Gambar</th>
                                    <th class="text-align text-center" width="20%">Produk</th>
                                    <th class="text-align text-center" width="15%">Kategori</th>
                                    <th class="text-align text-center" width="10%">Harga</th>
                                    <th class="text-align text-center" width="10%">Rekomendasi</th>
                                    <th class="text-align text-center" width="10%">Stok</th>
                                    <th class="text-align text-center" width="10%">Status</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($result)): ?>
                                    <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $rs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td class="text-align text-center" width="5%"> <?php echo e($no++); ?> </td>
                                        <td class="text-align text-center" width="15%"><img width="150"
                                                height="150"
                                                src="<?php echo e(base_url('assets/images/gambar_produk/').$gambar[$key]['gambar_nama']); ?>" />
                                        </td>
                                        <td class="text-align" width="20%"><?php echo e($rs['nama']); ?></td>
                                        <td class="text-align" width="15%"><?php echo e($rs['kategori_nama']); ?></td>
                                        <td class="text-align text-right" width="10%">Rp. <?php echo number_format($rs['harga']); ?></td>
                                        <td class="text-align text-center" width="10%">
                                            <?php if($rs['rekomendasi_st'] == 'yes'): ?>
                                            <span class="badge badge-success">Aktif</span>
                                            <?php else: ?>
                                            <span class="badge badge-danger">Tidak aktif</span>
                                            <?php endif; ?>
                                        </td>
                                        <td class="text-align text-center" width="10%">
                                            <?php if($rs['stok_st'] == 'yes'): ?>
                                            <span class="badge badge-success">Tersedia</span>
                                            <?php else: ?>
                                            <span class="badge badge-danger">Tidak Tersedia</span>
                                            <?php endif; ?>
                                        </td>
                                        <td class="text-align text-center" width="10%">
                                            <?php if($rs['active_st'] == 'yes'): ?>
                                            <span class="badge badge-success">Aktif</span>
                                            <?php else: ?>
                                            <span class="badge badge-danger">Tidak aktif</span>
                                            <?php endif; ?>
                                        </td>
                                        <td width="10%">
                                            <a href="<?php echo e(site_url('master/produk/detail/'.$rs['produk_id'])); ?>"
                                                class="btn btn-info btn-rounded m-b-10 m-l-5"
                                                title="Edit"><i class="ti-eye"></i></a>
                                            <a href="<?php echo e(site_url('master/produk/edit/'.$rs['produk_id'])); ?>"
                                                class="btn btn-success btn-rounded m-b-10 m-l-5"
                                                title="Edit"><i class="ti-pencil"></i></a>
                                            <a href="<?php echo e(site_url('master/produk/delete/'.$rs['produk_id'])); ?>"
                                                class="btn btn-danger btn-rounded m-b-10 m-l-5"
                                                title="Delete"><i class="ti-trash"></i></button>
                                        </td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <tr>
                                        <th class="text-align text-center" colspan="7"><h3> Data kosong </h3></th>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <?php if(isset($pagination)): ?>
                                <?php echo $pagination; ?>

                            <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->startPush('ext_js'); ?>
         <script>
             $(document).ready(function () {
                 $(".select2-single").select2({
                     width: '100%',
                     containerCssClass: ':all:'
                 });
             });
         </script>
         <?php $__env->stopPush(); ?>