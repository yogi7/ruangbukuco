<!-- <nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container d-flex align-items-center">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>
	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav m-auto">
				<?php echo $list_navbar; ?>

	        </ul>
	      </div>
	    </div>
	  </nav> -->
	<!-- END nav -->    
	
	<!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-11">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <!-- tampilan mobile -->
                        <div class="d-lg-none d-block" style="margin-left:10%">
                            <a class="navbar-brand" href="<?php echo e(site_url('client/beranda/')); ?>"> <h3><b> <?php echo e($pref_toko['value_pref']); ?> </b></h3> </a>
                        </div>
                        <!-- tampilan websute -->
                        <div class="d-none d-lg-block">
                            <a class="navbar-brand" href="<?php echo e(site_url('client/beranda/')); ?>">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <img src="<?php echo e(base_url('assets/images/logo/'.$pref_toko['img_name'])); ?>" style="height: 40px" alt="">
                                    </div>
                                    <div class="col-lg-7">
                                        <h2><b> <?php echo e($pref_toko['value_pref']); ?> </b></h2>
                                    </div>  
                                </div>
                            </a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="menu_icon"><i class="fas fa-bars"></i></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                <?php echo $list_navbar; ?>

                            </ul>
                        </div>
                        <div class="hearer_icon d-flex">
                        <!-- count yang belum dibayar -->
                        <a href="<?php echo e(site_url('client/beranda/search')); ?>"><i class="ti-search"></i><span class="badge badge-pill badge-primary"></span></a>
                        <?php if($belum_dibayar > 0): ?>
                            <a href="<?php echo e(site_url('client/pembayaran/list_transaksi/')); ?>"><i class="ti-wallet"></i><span class="badge badge-pill badge-primary"><?php echo e($belum_dibayar); ?></span></a>
                        <?php else: ?>
                            <a href="<?php echo e(site_url('client/pembayaran/list_transaksi/')); ?>"><i class="ti-wallet"></i><span class="badge badge-pill badge-primary"></span></a>
                        <?php endif; ?>
                        <!-- count isi cart -->
                        <?php if($cart > 0): ?>
                            <a href="<?php echo e(site_url('client/cart/')); ?>"><i class="ti-shopping-cart-full"></i><span class="badge badge-pill badge-primary"><?php echo e($cart); ?></span></a>
                        <?php else: ?>
                            <a href="javascript:void(0)"><i class="ti-shopping-cart-full"></i><span class="badge badge-pill badge-primary"></span></a>
                        <?php endif; ?>
                        <!-- count isi favorite -->
                        <?php if($favorite > 0): ?>
                            <a href="<?php echo e(site_url('client/favorit/')); ?>"><i class="ti-heart"></i><span class="badge badge-pill badge-primary"><?php echo e($favorite); ?></span></a>
                        <?php else: ?>
                            <a href="<?php echo e(site_url('client/favorit/')); ?>"><i class="ti-heart"></i><span class="badge badge-pill badge-primary"></span></a>
                        <?php endif; ?>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <div class="search_input" id="search_input_box">
            <div class="container ">
                <form class="d-flex justify-content-between search-inner">
                    <input type="text" class="form-control" id="search_input" placeholder="Search Here">
                    <button type="submit" class="btn"></button>
                    <span class="ti-close" id="close_search" title="Close Search"></span>
                </form>
            </div>
        </div>
    </header>
    <!-- Header part end-->
