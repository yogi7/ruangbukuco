<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"><?php echo e($title); ?>

            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Master Data</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page"><?php echo e($title); ?></li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<div class="container-fluid">
    
    <?php echo $__env->make('template/notif', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card"> 
                <div class="card-body">
                    <div class="row">
                        <div class="row">
                            <h4 class="card-title col-lg-12">Daftar Kategori</h4>
                        </div>
                        <form class="col-lg-12 row" action="<?php echo e(site_url('master/user/search_process')); ?>" method="POST">
                            <div class="col-lg-3">
                                <input type="text" name="kategori_nama" class="form-control" placeholder="Isian nama kategori..." value="<?php echo e($search['nama']); ?>">
                            </div>
                            <div class="col-lg-3">
                                <button type="submit" name="search" value="tampilkan" class="btn btn-info">Cari</button>
                                <button type="submit" name="search" value="reset"
                                    class="btn btn-secondary">Reset</button>
                            </div>
                        </form>
                        <div class="text-right col-lg-12">
                            <a href="<?php echo e(site_url('master/kategori/add')); ?>"  type="submit" class="btn btn-primary">Tambah Data</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th class="text-align text-center" width="10%">No.</th>
                                    <th class="text-align text-center" width="25%">Logo Kategori</th>
                                    <th class="text-align text-center" width="25%">Nama Kategori</th>
                                    <th class="text-align text-center" width="25%">Status</th>
                                <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($result)): ?>
                                    <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <th class="text-align text-center" width="10%"> <?php echo e($no++); ?> </th>
                                            <th class="text-align text-center" width="25%"><img width="100"
                                                        height="100"
                                                        src="<?php echo e(base_url('assets/images/logo_kategori/').$rs['logo']); ?>" />
                                                </th>
                                            <td width="25%"><?php echo e($rs['kategori_nama']); ?></td>
                                            <td class="text-align text-center" width="25%">
                                                <?php if($rs['kategori_st'] == 'yes'): ?>
                                                <span class="badge badge-success">aktif</span> 
                                                <?php else: ?>
                                                <span class="badge badge-danger">tidak aktif</span>  
                                                <?php endif; ?>
                                            </td>
                                            <td width="10%">
                                                    <a href="<?php echo e(site_url('master/kategori/edit/'.$rs['kategori_id'])); ?>" class="btn btn-success btn-rounded m-b-10 m-l-5" title="Edit"><i class="ti-pencil"></i></a>
                                                    <a href="<?php echo e(site_url('master/kategori/delete/'.$rs['kategori_id'])); ?>" class="btn btn-danger btn-rounded m-b-10 m-l-5" title="Delete"><i class="ti-trash"></i></button>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <tr>
                                        <th class="text-align text-center" colspan="7"><h3> Data kosong </h3></th>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <div class="text-right">
                            <?php if(isset($pagination)): ?>
                            <?php echo $pagination; ?>

                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>