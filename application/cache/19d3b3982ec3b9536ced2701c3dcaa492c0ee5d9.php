<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"><?php echo e($title); ?>

            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="" class="text-muted">Transaksi</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo e(site_url('transaksi/pembelian')); ?>"
                                class="text-muted"><?php echo e($title); ?></a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Detail</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="container-fluid">
    
    <?php echo $__env->make('template/notif', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-10">
                            <h4 class="card-title">Detail</h4>
                        </div>
                        <div class="col-lg-2">
                            <div class="text-right">
                                <a href="<?php echo e(site_url('transaksi/pembelian')); ?>" type="submit"
                                    class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                    </div>
                    <nav class="navbar navbar-expand-lg navbar-dark bg-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item active">
                                    <a class="nav-link btn btn-primary" href="<?php echo e(site_url('transaksi/pembelian/detail/')); ?><?php echo e($result['transaksi_id']); ?>">Bukti Transfer<span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn btn-primary" href="<?php echo e(site_url('transaksi/pembelian/detail_produk/')); ?><?php echo e($result['transaksi_id']); ?>">Produk</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn btn-primary" href="<?php echo e(site_url('transaksi/pembelian/detail_tujuan/')); ?><?php echo e($result['transaksi_id']); ?>">Tujuan Pengririman</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn btn-primary" href="<?php echo e(site_url('transaksi/pembelian/detail_kurir/')); ?><?php echo e($result['transaksi_id']); ?>">Kurir Pengiriman</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="row">
                        <div class="col-lg-12 card-body">
                        <?php if(!empty($bukti_transfer)): ?>
                        <div  style="margin-top:1%; margin-bottom:1%">
                            <h3>Bukti Transfer</h3>
                            <img height="600" width="600" src="<?php echo e(base_url('assets/images/bukti_transfer/'.$bukti_transfer['nama_file'])); ?>" />   
                        </div>
                        <?php else: ?>
                        <div  style="margin-top:1%; margin-bottom:1%">
                            <b>Pembeli belum mengirimkan bukti pembayaran ! </b>
                        </div>
                        <?php endif; ?>
                        </div><!-- /# column -->
					</div>
                    

                </div>
            </div>
        </div>
    </div>
</div>