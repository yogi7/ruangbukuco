<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*
----------------------
Created by : Lathiif Aji Santhosho
Phone : 	082126641201
----------------------
*/

class Pref_toko extends MY_Controller 
{
    // constructor
	public function __construct()
	{
		parent::__construct();
		
		//load models
		$this->load->model('sistem/M_prefrensi');
		
		/*-------------------------------------------------------
		 kosongkan isi parsing js dan css jika tidak digunakan,
		contoh:
		$this->parsing_js([

			]);
		--------------------------------------------------------*/
		//parsing js url
		$this->parsing_js([
			'assets/plugin/select2/select2.full.min.js',
		   ]);
		//parsing css url
		$this->parsing_css([
			'assets/plugin/select2/select2.min.css',
			'assets/plugin/select2/select2-bootstrap.min.css',
			]);

		//parsing data tanpa template
		$this->parsing([
			'title' => 'Prefrensi Toko'
		]);
	}

	public function index()
	{
		// set page rules
		$this->_set_page_rule("R");
		//default notif
		$notif = $this->session->userdata('sess_notif');
		$where_pref_toko = array(
			'jenis_pref' => 'pref_toko'
		);
		$get_pref_toko = $this->M_prefrensi->get_pref($where_pref_toko);
		
		//parsing
		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'pref_toko' 	=> $get_pref_toko,
			'list_kab'		=> $this->ro_get_kab()
		];

		// $this->cek($data);exit;

		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->parsing_template('setclient/pref_toko/index', $data);
	}

	// edit process
	public function edit_process() {
		// set page rules
		$this->_set_page_rule("U");
		$this->form_validation->set_rules('nama_toko', 'User Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('keterangan', 'User Name', 'trim|required');
		$this->form_validation->set_rules('kab_id', 'User Name', 'trim|required');

		// process
		if ($this->form_validation->run() !== FALSE) {
			//process
			$filesCount = count($_FILES);
			if($_FILES["files"]['name'] == '') {
				//hanya edit nama
				$params = array(
					'value_pref' 	=> $this->input->post('nama_toko', true),
					'keterangan' 	=> $this->input->post('keterangan', true),
					'value_pref_3' 	=> $this->input->post('kab_id', true),
				);
				$where = array(
					'jenis_pref'	=> 'pref_toko'
				);
				// insert to gambar
				if (!$this->M_prefrensi->update('prefrensi', $params, $where)) {
					// default error
					$this->notif_msg('setclient/pref_toko', 'Error', 'Data gagal ditambahkan');
				}else{
					// default error
					$this->notif_msg('setclient/pref_toko', 'Sukses', 'Berhasil diganti.');	
				}
			}else{
				$where_pref_toko = array(
					'jenis_pref' => 'pref_toko'
				);
				$get_pref_toko = $this->M_prefrensi->get_pref($where_pref_toko);
				
				unlink(FCPATH.'assets/images/logo/'.$get_pref_toko['img_name']);

				$_FILES['file']['name']     = $_FILES['files']['name'];
				$_FILES['file']['type']     = $_FILES['files']['type'];
				$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'];
				$_FILES['file']['error']    = $_FILES['files']['error'];
				$_FILES['file']['size']     = $_FILES['files']['size'];
			
				// File upload configuration
				// $uploadPath = 'uploads/files/';
				$uploadPath = FCPATH.'assets/images/logo/';
				$config['upload_path'] = $uploadPath;
				$config['allowed_types'] = 'jpg|jpeg|png';
				
				// Load and initialize upload library
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				
				// Upload file to server
				if($this->upload->do_upload('files')){
					// Uploaded file data
					$fileData = $this->upload->data();
					$params = array(
						'img_name'		=> $fileData['file_name'],
						'value_pref' 	=> $this->input->post('nama_toko', true),
						'keterangan' 	=> $this->input->post('keterangan', true),
						'value_pref_3' 	=> $this->input->post('kab_id', true)
					);
					$where = array(
						'jenis_pref'	=> 'pref_toko'
					);
					// insert to gambar
					if (!$this->M_prefrensi->update('prefrensi', $params, $where)) {
						// default error
						$this->notif_msg('setclient/pref_toko', 'Error', 'Data gagal ditambahkan');
					}else{
						// default error
						$this->notif_msg('setclient/pref_toko', 'Sukses', 'Berhasil diganti.');	
					}
				}
			}

		} else {
			// default error
			$this->notif_msg('setclient/pref_toko', 'Error', 'Nama Toko tidak boleh kosong');
		}

    }
}
