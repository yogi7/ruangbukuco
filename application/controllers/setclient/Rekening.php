<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*
----------------------
Created by : Lathiif Aji Santhosho
Phone : 	082126641201
----------------------
*/

class Rekening extends MY_Controller 
{
    // constructor
	public function __construct()
	{
		parent::__construct();
		
		//load models
		$this->load->model('sistem/M_prefrensi');
		
		/*-------------------------------------------------------
		 kosongkan isi parsing js dan css jika tidak digunakan,
		contoh:
		$this->parsing_js([

			]);
		--------------------------------------------------------*/
		//parsing js url
		$this->parsing_js([
		   ]);
		//parsing css url
		$this->parsing_css([
			]);

		//parsing data tanpa template
		$this->parsing([
			'title' => 'Rekening Toko'
		]);
	}

	public function index()
	{
		// set page rules
		$this->_set_page_rule("R");
		//default notif
		$notif = $this->session->userdata('sess_notif');
		$where_rekening = array(
			'jenis_pref' => 'rekening'
		);
		$get_rekening = $this->M_prefrensi->get_pref($where_rekening);
		//parsing
		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'rekening' 		=> $get_rekening
		];

		// $this->cek($data['rekening']['img_name']);exit;

		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->parsing_template('setclient/rekening/index', $data);
	}

	// edit process
	public function edit_process() {
		// set page rules
		$this->_set_page_rule("U");
		$this->form_validation->set_rules('nama_bank', 'Nama Bank', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('keterangan', 'Atas nama', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('', 'Kode Bank', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('value_pref', 'Nomor Rekening', 'trim|required|max_length[50]');

		// process
		if ($this->form_validation->run() !== FALSE) {
			//process
			$filesCount = count($_FILES);
			if($_FILES["files"]['name'] == '') {
				//hanya edit nama
				$params = array(
					'value_pref_2' 	=> $this->input->post('nama_bank', true),
					'keterangan' 	=> $this->input->post('keterangan', true),
					'kode' 			=> $this->input->post('kode', true),
					'value_pref' 	=> $this->input->post('value_pref', true)
				);
				$where = array(
					'jenis_pref'	=> 'rekening'
				);
				// insert to gambar
				if (!$this->M_prefrensi->update('prefrensi', $params, $where)) {
					// default error
					$this->notif_msg('setclient/rekening', 'Error', 'Data gagal ditambahkan');
				}else{
					// default error
					$this->notif_msg('setclient/rekening', 'Sukses', 'Berhasil diganti.');	
				}
			}else{
				$where_rekening = array(
					'jenis_pref' => 'rekening'
				);
				$get_rekening = $this->M_prefrensi->get_pref($where_rekening);
				
				unlink(FCPATH.'assets/images/prefrensi/'.$get_rekening['img_name']);

				$_FILES['file']['name']     = $_FILES['files']['name'];
				$_FILES['file']['type']     = $_FILES['files']['type'];
				$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'];
				$_FILES['file']['error']    = $_FILES['files']['error'];
				$_FILES['file']['size']     = $_FILES['files']['size'];
			
				// File upload configuration
				// $uploadPath = 'uploads/files/';
				$uploadPath = FCPATH.'assets/images/prefrensi/';
				$config['upload_path'] = $uploadPath;
				$config['allowed_types'] = 'jpg|jpeg|png';
				
				// Load and initialize upload library
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				
				// Upload file to server
				if($this->upload->do_upload('files')){
					// Uploaded file data
					$fileData = $this->upload->data();
					$params = array(
						'img_name'		=> $fileData['file_name'],
						'value_pref_2' 	=> $this->input->post('nama_bank', true),
						'keterangan' 	=> $this->input->post('keterangan', true),
						'kode' 			=> $this->input->post('kode', true),
						'value_pref' 	=> $this->input->post('value_pref', true)
					);
					$where = array(
						'jenis_pref'	=> 'rekening'
					);
					// insert to gambar
					if (!$this->M_prefrensi->update('prefrensi', $params, $where)) {
						// default error
						$this->notif_msg('setclient/rekening', 'Error', 'Data gagal ditambahkan');
					}else{
						// default error
						$this->notif_msg('setclient/rekening', 'Sukses', 'Berhasil diganti.');	
					}
				}
			}

		} else {
			// default error
			$this->notif_msg('setclient/rekening', 'Error', 'Nama Toko tidak boleh kosong');
		}

    }
}
