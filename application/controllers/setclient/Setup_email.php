<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*
----------------------
Created by : Lathiif Aji Santhosho
Phone : 	082126641201
----------------------
*/

class Setup_email extends MY_Controller 
{
    // constructor
	public function __construct()
	{
		parent::__construct();
		
		//load models
		$this->load->model('sistem/M_prefrensi');
        
		/*-------------------------------------------------------
		 kosongkan isi parsing js dan css jika tidak digunakan,
		contoh:
		$this->parsing_js([

			]);
		--------------------------------------------------------*/
		//parsing js url
		$this->parsing_js([
		   ]);
		//parsing css url
		$this->parsing_css([
			]);

		//parsing data tanpa template
		$this->parsing([
			'title' => 'Konfigurasi Email'
		]);
	}

	public function index()
	{
		// set page rules
		$this->_set_page_rule("R");
		//default notif
		$notif = $this->session->userdata('sess_notif');
		//get data
		$where_pref = array(
			'jenis_pref' => 'email'
		);
		$pref = $this->M_prefrensi->get_pref($where_pref);
		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'pref'			=> $pref
		];

		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->parsing_template('setclient/setup_email/index', $data);
	}

	// edit process
	public function edit_process() {
		// set page rules
		$this->_set_page_rule("U");
        // cek input
        $this->form_validation->set_rules('value_pref_2', 'Nomor Telephone', 'trim|required');
		$this->form_validation->set_rules('kode', 'Nomor Whatsapp', 'trim|required');
		$this->form_validation->set_rules('value_pref', 'Fanpage Facebook', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'Alamat Email', 'trim|required');
		
        // process
        if ($this->form_validation->run() !== FALSE) {
			$params = array(
				'value_pref_2'		=> $this->input->post('value_pref_2', true), 
				'kode'				=> $this->input->post('kode', true), 
				'value_pref' 		=> $this->input->post('value_pref', true), 
				'keterangan'		=> $this->input->post('keterangan', true), 
			);
			$where = array(
				'jenis_pref'	=> 'email'
			);
			// var_dump($params);exit;
			if (!$this->M_prefrensi->update('prefrensi', $params, $where)) {
				//notif gagal
				$this->notif_msg('setclient/setup_email', 'Error', 'Gagal dirubah !');
			}else{
				$this->notif_msg('setclient/setup_email', 'Sukses', 'Data berhasil dirubah.');
			}
		}else{
			$this->notif_msg('setclient/setup_email', 'Error', 'Gagal dirubah !');
		}
    }
}
