<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*
----------------------
Created by : Lathiif Aji Santhosho
Phone : 	082126641201
----------------------
*/

class Favorit extends MY_Controller {

	const SESSION_SEARCH = 'search_produk_favorit';
	const SESSION_SEARCH_KATEGORI = 'search_produk_favorit_kategori';
	
	// constructor
	public function __construct()
	{
		parent::__construct();
		
		//load models
		$this->load->model('master/M_produk');
		$this->load->model('master/M_kategori');
		$this->load->model('client/M_favorit');

		//parsing data tanpa template
		$this->parsing([
			'title' => 'Favorit'
		]);
	}

	public function index()
	{
		//default notif
		$notif = $this->session->userdata('sess_notif');
		if (empty($this->get_login())) {
			redirect('welcome');
		}
		$search = $this->session->userdata(self::SESSION_SEARCH);
		$search_kategori = $this->session->userdata(self::SESSION_SEARCH_KATEGORI);

		//data
		$params = array(
			'user_id'		=> $this->get_login('user_id'),
			'produk_nama'	=> $search['produk_nama'],
			'kategori_id'	=> $search_kategori['kategori_id']
		);
		$produk 		= $this->M_produk->get_by_id_favorit($params);
		
		$kategori 		= $this->M_kategori->get_all_nf();
		//loop gambar
		$gambar = array();
		// echo"<pre>";print_r($gambar);die;
		if(!empty($produk)){	
			foreach ($produk as $key => $value) {
				$gambar[$key] = $this->M_produk->get_gambar_by_produk($value['produk_id']);
			}
		}

        //parsing
		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'kategoris' 	=> $kategori,
			'produk' 		=> $produk,
			'gambar' 		=> $gambar,
			'rs_search'		=> $search,
			'rs_search_kategori' => $search_kategori,
		];

		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->client_template('client/favorit/index', $data);
	}

	public function search() {

        if ($this->input->post('search', true) == "tampilkan") {
        $params = array(
			'produk_nama' 	=> $this->input->post('produk_nama', true)
			
		);
        $this->session->set_userdata(self::SESSION_SEARCH, $params);
        } else {
        $this->session->unset_userdata(self::SESSION_SEARCH);
        }
        redirect("client/favorit/");
	}

	public function search_by_kategori() {

        $kategori_id = $this->input->post('kategori_id', true); 
        $params_kategori = array(
			'kategori_id' 	=> $kategori_id
			
		);
		// echo"<pre>";print_r($params_kategori);die;
		if($kategori_id = 0){
			$this->session->unset_userdata(self::SESSION_SEARCH);

		}else{
			$this->session->set_userdata(self::SESSION_SEARCH_KATEGORI, $params_kategori);

		}
        
        redirect("client/favorit/");
	}

	public function add_favorit_process($produk_id = '')
	{
		if (empty($produk_id)) {
			// default error
			$this->notif_msg('client/beranda/detail/'.$produk_id, 'Error', 'Data tidak ditemukan');
		}
		//set params
		$where = array(
			'produk_id' => $produk_id,
			'user_id' 	=> $this->get_login('user_id')
		);

		$params = array(
			'user_id'		=> $this->get_login('user_id'), 
			'produk_id'		=> $produk_id, 
			'date_favorit'	=> date('Y-m-d H:i:s') 
		);
		// insert
		if ($this->M_favorit->insert('favorit', $params)) {
			// insert to kategori
			$this->notif_msg('client/beranda/detail/'.$produk_id, 'Sukses', 'Data berhasil ditambahkan ke list favorit');
		} else {
			// default error
			$this->notif_msg('client/beranda/detail/'.$produk_id, 'Error', 'Gagal ditambahkan ke list favorit');
		}
	}

	public function remove_favorit_process($produk_id = '')
	{
		if (empty($produk_id)) {
			// default error
			$this->notif_msg('client/beranda/detail/'.$produk_id, 'Error', 'Data tidak ditemukan');
		}
		//set params
		$where = array(
			'produk_id' => $produk_id,
			'user_id' 	=> $this->get_login('user_id')
		);
		// insert
		if ($this->M_favorit->delete('favorit', $where)) {
			// insert to kategori
			$this->notif_msg('client/beranda/detail/'.$produk_id, 'Sukses', 'Data berhasil dihapus dari list favorit');
		} else {
			// default error
			$this->notif_msg('client/beranda/detail/'.$produk_id, 'Error', 'Gagal dihapus dari list favorit');
		}
	}

}
