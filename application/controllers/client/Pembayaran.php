<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*
----------------------
Created by : Lathiif Aji Santhosho
Phone : 	082126641201
----------------------
*/

class Pembayaran extends MY_Controller {
    public $mail_message = array();

    // constructor
	public function __construct()
	{
		parent::__construct();
		
		//load models
		$this->load->model('client/M_transaksi');
		$this->load->model('sistem/M_prefrensi');
		$this->load->model('master/M_produk');

		//js
		$this->parsing_js([
		]);
		//parsing css url
		$this->parsing_css([
		]);

		//parsing data tanpa template
		$this->parsing([
			'title' => 'Pembayaran'
		]);
	}

	public function index($transaksi_id = '')
	{
		//default notif
		$notif = $this->session->userdata('sess_notif');
		$where = array(
			'jenis_pref' => 'rekening'
		);
		$pref = $this->M_prefrensi->get_pref($where);
		$result = $this->M_transaksi->get_by_id($transaksi_id);
		if (empty($result)) {
			// default error
			$this->notif_msg('client/beranda/', 'Error', 'Data tidak ditemukan !');
		}elseif(empty($this->get_login())){
			redirect('welcome');
		}
		//parsing
		$data = [
			'tipe'				=> $notif['tipe'],
			'pesan' 			=> $notif['pesan'],
			'pref'			   	=> $pref,
			'result' 			=> $result
		];
		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->client_template('client/checkout/index', $data);
	}

	public function checkout_process()
	{
		$this->form_validation->set_rules('produk_id[0]', '', 'required');
		$this->form_validation->set_rules('nama', '', 'required');
		$this->form_validation->set_rules('pos', '', 'trim|required');
		$this->form_validation->set_rules('province_id', '', 'trim|required');
		$this->form_validation->set_rules('city_id', '', 'trim|required');
		$this->form_validation->set_rules('subdistrict_id', '', 'trim|required');
		$this->form_validation->set_rules('alamat', '', 'required');
		$this->form_validation->set_rules('kurir', '', 'required');
		$this->form_validation->set_rules('kurir_service', '', 'trim|required');

		$datePrefix = date('ymdHis');
		$transaksi_id = $this->M_transaksi->generate_id($datePrefix);

		//cek validasi
		if ($this->form_validation->run() !== FALSE) {
			//cek kode unik
			do {
				$kode_unik = rand(100,999);
				$where = array(
					'kode_unik' 	=> $kode_unik,
					'transaksi_st'	=> 'dibeli'
				);
			} while ($this->M_transaksi->is_exist_kode_unik($where) > 0);
			//params transaksi

			// add 2 days to date
			$tgl_batas = Date('Y-m-d H:i:s', strtotime("+3 days"));

			$param_transaki = array(
				'transaksi_id'		=> $transaksi_id,  
				'user_id'			=> $this->get_login('user_id'),  
				'subtotal'			=> array_sum($this->input->post('harga_pluss_jumlah', TRUE)),
				'kode_unik'			=> $kode_unik,  
				'mdb'				=> $this->get_login('user_id'),
				'mdb_name'			=> $this->get_login('user_name'),
				'mdd'				=> date('Y-m-d H:i:s'),
				'tgl_batas_bayar'	=> $tgl_batas,
			);
			if ($this->M_transaksi->insert('transaksi', $param_transaki)) {
				// insert to info kurir
				$param_kurir = array(
					'transaksi_id'		=> $transaksi_id,  
					'nama_kurir'		=> $this->input->post('kurir', TRUE),  
					'service'			=> $this->input->post('kurir_service', TRUE),  
					'wkt_perkiraan'		=> $this->input->post('wkt_perkiraan', TRUE),  
					'biaya_ongkir'		=> $this->input->post('biaya_ongkir', TRUE),  
					'desk_kurir'		=> $this->input->post('desk', TRUE),  
				);
				if ($this->M_transaksi->insert('info_kurir', $param_kurir)) {
					// insert to alamat_transaksi
					$param_alamat = array(
						'transaksi_id'		=> $transaksi_id,  
						'nama_penerima'		=> $this->input->post('nama', TRUE),  
						'kode_pos'			=> $this->input->post('pos', TRUE),  
						'no_hp'				=> $this->input->post('phone', TRUE),  
						'email'				=> $this->input->post('email', TRUE),  
						'prov_id'			=> $this->input->post('province_id', TRUE),  
						'prov_nama'			=> $this->input->post('nama_provinsi', TRUE),  
						'kab_id'			=> $this->input->post('city_id', TRUE),  
						'kec_id'			=> $this->input->post('subdistrict_id', TRUE),  
						'kab_nama'			=> $this->input->post('nama_kota', TRUE),  
						'kec_nama'			=> $this->input->post('nama_kecamatan', TRUE),  
						'alamat_lengkap'	=> $this->input->post('alamat', TRUE),  
					);
					if ($this->M_transaksi->insert('alamat_transaksi', $param_alamat)) {
						//change status
						$param_cart = array(
							'transaksi_st' 	=> 'dibeli',
							'tgl_transaksi'	=> date('Y-m-d H:i:s') 
						);
						$produk_id = $this->input->post('produk_id', TRUE);
						$jumlah = $this->input->post('jumlah', TRUE);
						for ($i=0; $i < count($produk_id); $i++) { 
							$where = array(
								'produk_id' => $produk_id[$i],
								'user_id'	=> $this->get_login('user_id')
							);
							if (!$this->M_transaksi->update('cart', $param_cart, $where)) {
								// default error
								$this->notif_msg('client/cart/index/', 'Error', 'Gagal update status transaksi');
							}
							//insert to detail transaksi
							$params_transaksi_produk = array(
								'transaksi_id' 	=> $transaksi_id,
								'produk_id' 	=> $produk_id[$i],
								'jumlah'		=> $jumlah[$i]
							);
							if (!$this->M_transaksi->insert('transaksi_produk', $params_transaksi_produk)) {
								// default error
								$this->notif_msg('client/cart/index/', 'Error', 'Gagal insert transaksi produk !');
							}
						}
						//send mail
						$where_pref = array(
							'jenis_pref' => 'email'
						);
						$pref = $this->M_prefrensi->get_pref($where_pref);
						// //Load email library 
						// config
						$this->load->library('email');
						$config['mailtype'] = 'html';
          	$config['charset'] = 'utf-8';
            $config['newline'] = "\r\n";
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = $pref['value_pref_2'];
            $config['smtp_port'] = $pref['kode'];
            $config['smtp_user'] = $pref['value_pref'];
            $config['smtp_pass'] = $pref['keterangan'];
            // $config['mailtype']  = 'html'; 
            $confid['charset']   = 'iso-8859-1';
						$this->email->initialize($config);
						$this->mail_message = 'Anda telah berhasil melakukan pembelian di <b>Toko Online</b>,';
						$this->mail_message .= '<br>dengan produk sebagai berikut : <br>';
						$no = 1;
						for ($i=0; $i < count($produk_id); $i++) { 
							$get_produk = $this->M_produk->get_by_id($produk_id[$i]);
							$this->mail_message .=   $no .'. <b>'. $get_produk['nama'] . ' </b> ' . $jumlah[$i] . ' '. $get_produk['satuan'] .'<br>';
							$no++;
						}
						$this->mail_message .= 'untuk melihat informasi jumlah penagihan yang harus dibayar silahkan login ke akun anda. <br>';
						$this->mail_message .= '<b>Segera lakukan pembayaran sebelum stok kehabisan, transaksi akan otomatis dibatalkan jika lebih dari tanggal '. $tgl_batas .'</b>';
						$this->email->from($pref['value_pref'], 'Toko Online');
						$this->email->to($this->get_login('user_mail'));
						$this->email->subject('Pembelian Produk Toko Online');
						$this->email->message($this->mail_message); 
						//Send mail 
						$this->notif_msg('client/pembayaran/index/'.$transaksi_id, 'Sukses', 'Pemesanan anda berhasil dilakukan, harap hubungi admin ruangbuku.co apabila ada pertanyaan.');
						// if($this->email->send()){
						// 	//sukses
						// 	$this->notif_msg('client/pembayaran/index/'.$transaksi_id, 'Sukses', 'Pemesanan anda berhasil dilakukan, informasi pemesanan akan dikirimkan juga melalui email.');
						// }else{
						// 	$this->notif_msg('sistem/register', 'Error', 'Gagal melakukan pemesanan');    
						// }
					} else {
						// default error
						$this->notif_msg('client/cart/index/', 'Error', 'Gagal insert kurir !');
					}	
				} else {
					// default error
					$this->notif_msg('client/cart/index/', 'Error', 'Gagal insert kurir !');
				}
			} else {
				// default error
				$this->notif_msg('client/cart/index/', 'Error', 'Gagal insert kurir !');
			}
		} else {
			// default error
			$this->notif_msg('client/cart/index/', 'Error', 'Form isian tidak lengkap !');
        }
	}
	

	public function kirim_bukti_tf($transaksi_id = '')
	{
		//default notif
		$notif = $this->session->userdata('sess_notif');
		$result = $this->M_transaksi->get_by_id($transaksi_id);
		if (empty($result)) {
			// default error
			$this->notif_msg('client/beranda/', 'Error', 'Data tidak ditemukan !');
		}elseif(empty($this->get_login())){
			redirect('welcome');
		}
		//parsing
		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'result' 		=> $result
		];
		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->client_template('client/checkout/kirim_bukti_tf', $data);
	}

	public function kirim_bukti_tf_process()
	{
		$this->form_validation->set_rules('transaksi_id', '', 'trim|required');
		$this->form_validation->set_rules('no_rek_pentransfer', '', 'required');
		$this->form_validation->set_rules('nama_rek', '', 'trim|required');
		$transaksi_id = $this->input->post('transaksi_id', TRUE);
		$datePrefix = date('ymd');
		//cek validasi
		if ($this->form_validation->run() !== FALSE) {
			
			$filesCount = count($_FILES['file_bukti_tf']['name']);
			if ($filesCount < 1) {
				// default error
				$this->notif_msg('client/pembayaran/kirim_bukti_tf/'.$transaksi_id, 'Error', 'Foto Tidak boleh kosong !');
			}

			$_FILES['file']['name']     = $datePrefix.$_FILES['file_bukti_tf']['name'];
			$_FILES['file']['type']     = $_FILES['file_bukti_tf']['type'];
			$_FILES['file']['tmp_name'] = $_FILES['file_bukti_tf']['tmp_name'];
			$_FILES['file']['error']     = $_FILES['file_bukti_tf']['error'];
			$_FILES['file']['size']     = $_FILES['file_bukti_tf']['size'];
			
			// File upload configuration
			$uploadPath = FCPATH.'assets/images/bukti_transfer/';
			$config['upload_path'] = $uploadPath;
			$config['allowed_types'] = 'jpg|jpeg|png';
			
			// Load and initialize upload library
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			// Upload file to server
			if($this->upload->do_upload('file')){
				// Uploaded file data
				$fileData = $this->upload->data();
				
				$params = array(
					'transaksi_id'		=> $transaksi_id,  
					'nama_rek'			=> $this->input->post('nama_rek'),  
					'no_rek_pentransfer'=> $this->input->post('no_rek_pentransfer'),  
					'nama_file'			=> $fileData['file_name'],  
					'mdd'				=> date('Y-m-d H:i:s'),
				);
				if ($this->M_transaksi->insert('bukti_transfer', $params)) {
					$where = array(
						'transaksi_id' => $transaksi_id
					);
					$params_update = array(
						'transaksi_st' => 'kirim_bukti'
					);
					if ($this->M_transaksi->update('transaksi', $params_update, $where)) {
						//notif
						$this->notif_msg('client/beranda', 'Sukses', 'Bukti transfer berhasil di upload, mohon untuk menunggu konfirmasi dari admin.');
					}else{
						// default error
						$this->notif_msg('client/pembayaran/kirim_bukti_tf/'.$transaksi_id, 'Error', 'Gagal update status transaksi!');
					}
				} else {
					// default error
					$this->notif_msg('client/pembayaran/kirim_bukti_tf/'.$transaksi_id, 'Error', 'Gagal upload bukti transfer !');
				}
			}

		} else {
			// default error
			$this->notif_msg('client/pembayaran/kirim_bukti_tf/'.$transaksi_id, 'Error', 'Gagal upload bukti transfer !');
        }
	}

	public function list_transaksi()
	{
		//default notif
		$notif = $this->session->userdata('sess_notif');
		$result_transaksi = $this->M_transaksi->get_transaksi_by_user_id($this->get_login('user_id'));
		foreach ($result_transaksi as $key => $value) {
			$where = array(
				'user_id' 		=> $this->get_login('user_id'),
				'b.transaksi_id' 	=> $value['transaksi_id'] 
			);
			$transaksi_produk[] = $this->M_transaksi->get_by_user_transaksi_id($where);
		}

		if (!empty($transaksi_produk)) {
				//loop gambar
				$gambar = array();
				foreach ($transaksi_produk as $key => $value) {
					foreach ($value as $i => $val) {
						$gambar[$key] = $this->M_transaksi->get_gambar_by_produk($val['produk_id']);
					}
				}
		}else{
			$gambar = '';
		}
		//parsing
		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'result' 		=> $result_transaksi,
			'gambar' 		=> $gambar,
		];
		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->client_template('client/checkout/list_transaksi', $data);
	}

	public function detail_transaksi($transaksi_id = '')
	{
		//default notif
		$notif = $this->session->userdata('sess_notif');
		$where = array(
			'a.transaksi_id' => $transaksi_id
 		);
		$result_transaksi = $this->M_transaksi->get_by_transaksi_id($where);
		$result_produk = $this->M_transaksi->get_produk_by_transaksi($transaksi_id);
		$result_alamat = $this->M_transaksi->get_alamat_by_transaksi($transaksi_id);
		$result_kurir = $this->M_transaksi->get_kurir_by_transaksi($transaksi_id);
		// $this->cek($result_produk);
		//loop gambar
		$gambar = array();
		foreach ($result_produk as $key => $value) {
				$gambar[$key] = $this->M_transaksi->get_gambar_by_produk($value['produk_id']);
		}
		//parsing
		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'result' 		=> $result_transaksi,
			'produk' 		=> $result_produk,
			'gambar' 		=> $gambar,
			'alamat' 		=> $result_alamat,
			'kurir' 		=> $result_kurir,
		];
		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->client_template('client/checkout/detail_transaksi', $data);
	}

	public function detail_pengiriman($transaksi_id = '', $resi='')
	{
		if (empty($resi)) {
			// default error
			$this->notif_msg('client/pembayaran/detail_transaksi/'.$transaksi_id, 'Error', 'Resi belum diinputkan oleh admin !');
		}
		$where = array(
			'a.transaksi_id' => $transaksi_id
 		);
		$result_transaksi = $this->M_transaksi->get_by_transaksi_id($where);
		 if (empty($result_transaksi)) {
				// default error
			$this->notif_msg('client/pembayaran/detail_transaksi/'.$transaksi_id, 'Error', 'Data tidak ditemukan !');
		 }

		//result rajaongkir
		$result = $this->M_transaksi->get_kurir_by_transaksi($transaksi_id);
		//lacak
		$get_lacak = $this->ro_cek_resi($result['resi'], $result['nama_kurir']);
		if ($get_lacak) {
			$status_resi = $get_lacak['status'];
			$track = $get_lacak['result'];
		}else{
			$status_resi = '';
			$track = '';
		}

		//parsing
		$data = [
			'transaksi_id' 	=> $transaksi_id,
			'resi'		   	=> $resi,
			'status_resi' 	=> $status_resi,
			'track' 		=> $track,
		];
		//parsing (template_content, variabel_parsing)
		$this->client_template('client/checkout/detail_pengiriman', $data);
	}

}
