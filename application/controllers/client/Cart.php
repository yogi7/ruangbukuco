<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*
----------------------
Created by : Lathiif Aji Santhosho
Phone : 	082126641201
----------------------
*/

class Cart extends MY_Controller {

    // constructor
	public function __construct()
	{
		parent::__construct();
		
		//load models
		$this->load->model('master/M_produk');
		$this->load->model('master/M_kategori');
		$this->load->model('client/M_favorit');
		$this->load->model('sistem/M_prefrensi');


		//js
		$this->parsing_js([
		]);
		//parsing css url
		$this->parsing_css([
		]);

		//parsing data tanpa template
		$this->parsing([
			'title' => 'Keranjang'
		]);
	}

	public function index()
	{
		//default notif
		$notif = $this->session->userdata('sess_notif');
		if (empty($this->get_login())) {
			redirect('welcome');
		}
		$where = array(
			'a.user_id' 		=> $this->get_login('user_id'),
			'a.transaksi_st'	=> 'cart',
			'b.stok_st'			=> 'yes'
		);
		//data
		$produk 		= $this->M_cart->get_by_user_id($where);
		$kategori 		= $this->M_kategori->get_all_nf();
		
		//loop gambar
		$gambar = array();
		foreach ($produk as $key => $value) {
			$gambar[$key] = $this->M_produk->get_gambar_by_produk($value['produk_id']);
		}

        //parsing
		$data = [
			'tipe'				=> $notif['tipe'],
			'pesan' 			=> $notif['pesan'],
			'kategoris'		 	=> $kategori,
			'produk' 			=> $produk,
			'gambar' 			=> $gambar,
			'provinsi'			=> $this->ro_get_provinsi(),
			'status_anggota' 	=> $this->get_login('status_anggota')
		];
		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->client_template('client/cart/index', $data);
	}

	public function add_cart_process()
	{
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
		$this->form_validation->set_rules('produk_id', 'Produk', 'trim|required');
		$produk_id = $this->input->post('produk_id');	
		//set params
		$where = array(
			'produk_id' 	=> $produk_id,
			'user_id' 		=> $this->get_login('user_id'),
			'transaksi_st'	=> 'cart',
		);
		if ($this->M_cart->is_exist_produk($where)){
			// default error
			$this->notif_msg('client/beranda/detail/'.$produk_id, 'Error', 'Produk yang sama sudah ada di keranjang anda');
		}

        // process
        if ($this->form_validation->run() !== FALSE) {
			$params = array(
				'user_id'		=> $this->get_login('user_id'), 
				'produk_id'		=> $produk_id, 
				'jumlah'		=> $this->input->post('jumlah'), 
				'tgl_insert'	=> date('Y-m-d H:i:s') 
			);
            // insert
            if ($this->M_kategori->insert('cart', $params)) {
                // insert to kategori
				$this->notif_msg('client/beranda/detail/'.$produk_id, 'Sukses', 'Data berhasil ditambahkan');
            } else {
				// default error
				$this->notif_msg('client/beranda/detail/'.$produk_id, 'Error', 'Data gagal ditambahkan');
            }
        } else {
			// default error
			$this->notif_msg('client/beranda/detail/'.$produk_id, 'Error', 'Data gagal ditambahkan');
        }
	}

	public function add_favorit_process($produk_id = '')
	{
		if (empty($produk_id)) {
			// default error
			$this->notif_msg('client/beranda/detail/'.$produk_id, 'Error', 'Data tidak ditemukan');
		}
		//set params
		$where = array(
			'produk_id' => $produk_id,
			'user_id' 	=> $this->get_login('user_id')
		);

		$params = array(
			'user_id'		=> $this->get_login('user_id'), 
			'produk_id'		=> $produk_id, 
			'date_favorit'	=> date('Y-m-d H:i:s') 
		);
		// insert
		if ($this->M_favorit->insert('favorit', $params)) {
			// insert to kategori
			$this->notif_msg('client/beranda/detail/'.$produk_id, 'Sukses', 'Data berhasil ditambahkan ke list favorit');
		} else {
			// default error
			$this->notif_msg('client/beranda/detail/'.$produk_id, 'Error', 'Gagal ditambahkan ke list favorit');
		}
	}

	public function remove_favorit_process($produk_id = '')
	{
		if (empty($produk_id)) {
			// default error
			$this->notif_msg('client/beranda/detail/'.$produk_id, 'Error', 'Data tidak ditemukan');
		}
		//set params
		$where = array(
			'produk_id' => $produk_id,
			'user_id' 	=> $this->get_login('user_id')
		);
		// insert
		if ($this->M_favorit->delete('favorit', $where)) {
			// insert to kategori
			$this->notif_msg('client/beranda/detail/'.$produk_id, 'Sukses', 'Data berhasil dihapus dari list favorit');
		} else {
			// default error
			$this->notif_msg('client/beranda/detail/'.$produk_id, 'Error', 'Gagal dihapus dari list favorit');
		}
	}


	public function remove_cart_process()
	{
		$produk_id = $this->input->post('produk_id');
		if (empty($produk_id)) {
			// default error
			$this->notif_msg('client/cart/', 'Error', 'Data tidak ditemukan');
		}
		//set params
		$where = array(
			'produk_id' => $produk_id,
			'user_id' 	=> $this->get_login('user_id')
		);
		// insert
		if ($this->M_favorit->delete('cart', $where)) {
			// berhasil
			echo "true";
		} else {
			// default error
			echo "false";
		}
	}

	public function get_kabupaten_by_prov()
	{
		$prov_id = $this->input->post('prov_id');
		// print_r($this->ro_get_kabupaten_by_prov($prov_id));
		echo $this->ro_get_kabupaten_by_prov($prov_id);
	}

	public function get_kecamatan_by_kab()
	{
		$city_id = $this->input->post('city_id');
		print_r($this->ro_get_kecamatan_by_kab($city_id));
	}

	public function get_ongkir()
	{
		// $this->cek($this->input->post());

		$where_pref_toko = array(
			'jenis_pref' => 'pref_toko'
		);
		$get_pref_toko = $this->M_prefrensi->get_pref($where_pref_toko);

		$kota_asal = $get_pref_toko['value_pref_3']; //kota pengirim
		$kurir_nama = $this->input->post('kurir_nama');
		$tujuan = $this->input->post('tujuan');
		$produk_id = $this->input->post('produk_id');
		$jumlah = $this->input->post('jumlah');
		$get_berat = array();
		$tes_jumlah = array();
		for ($i=0; $i < count($produk_id) ; $i++) { 
			$get_produk = $this->M_produk->get_by_id($produk_id[$i]); 
			$get_berat[] = ($jumlah[$i] * $get_produk['berat']);
			$tes_jumlah[] = $jumlah[$i];
		}
		$total_berat = array_sum($get_berat);
		print_r($this->ro_get_onkir($kota_asal,$tujuan, $total_berat, $kurir_nama));
	}
	

	public function pembayaran()
	{
		$this->cek($this->input->post());
	}

}
