<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*
----------------------
Created by : Lathiif Aji Santhosho
Phone : 	082126641201
----------------------
*/

class Produk extends MY_Controller {

	//init serach name
	const SESSION_SEARCH = 'search_produk';
    // constructor
	public function __construct()
	{
		parent::__construct();
		
		//load models
		$this->load->model('master/M_produk');
		$this->load->model('master/M_kategori');
        //js
		$this->parsing_js([
			'assets/plugin/ckeditor/ckeditor.js',
			'assets/plugin/select2/select2.full.min.js',
			'assets/plugin/bootstrap4-toggle-3.6.1/js/bootstrap4-toggle.min.js',
		]);
	//parsing css url
		$this->parsing_css([
			'assets/plugin/select2/select2.min.css',
			'assets/plugin/select2/select2-bootstrap.min.css',
			'assets/plugin/bootstrap4-toggle-3.6.1/css/bootstrap4-toggle.min.css'
		]);

		//parsing data tanpa template
		$this->parsing([
			'title' => 'Produk'
		]);
	}

	public function index()
	{
		// set page rules
		$this->_set_page_rule("R");
		//default notif
		$notif = $this->session->userdata('sess_notif');

		// search session
		$search = $this->session->userdata(self::SESSION_SEARCH);

		//create pagination
		$this->load->library('pagination');
		$this->load->config('pagination');
		$config = $this->config->item('pagination_config');
		$total_row = $this->M_produk->count_all();
		$config['base_url'] = base_url('index.php/master/produk/index/');
		$config['total_rows'] = $total_row;
		$config['per_page'] = 10;
		$from = $this->uri->segment(4);
		$this->pagination->initialize($config);		
		$result = $this->M_produk->get_all($config['per_page'],$from, $search);
		if (empty($result)) {
			$no = 0;
		}else{
			$no = 1;
		}
		//get kategori
		$kategori = $this->M_kategori->get_all_nf();
		//loop gambar
		$gambar = array();
		foreach ($result as $key => $value) {
			$gambar[$key] = $this->M_produk->get_gambar_by_produk($value['produk_id']);
		}
		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'search' 		=> $search,
			'kategoris' 	=> $kategori,
			'result' 		=> $result,
			'no' 			=> $no,
			'gambar' 		=> $gambar,
			'pagination'	=> $this->pagination->create_links()
		];

		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->parsing_template('master/produk/index', $data);
	}

	public function search_process() {
		// page rule read
		$this->_set_page_rule("R");

		if ($this->input->post('search', true) == "tampilkan") {
		  $params = array(
			'b.kategori_nama'   	=> $this->input->post('kategori_nama', true),
			'a.nama'   			=> $this->input->post('nama', true)
		  );
		  $this->session->set_userdata(self::SESSION_SEARCH, $params);
		} else {
		  $this->session->unset_userdata(self::SESSION_SEARCH);
		}
		redirect("master/produk");
	}

	public function add($notif='')
	{
		// set page rules
		$this->_set_page_rule("C");
		//default notif
		$notif = $this->session->userdata('sess_notif');
		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//get kategori
		$kategori = $this->M_kategori->get_all_nf();
		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'kategoris' 	=> $kategori,
		];
		//parsing (template_content, variabel_parsing)
		$this->parsing_template('master/produk/add', $data);
	}

	 // add process
	 public function add_process() {
		// set page rules
		$this->_set_page_rule("C");
        // cek input
		$this->form_validation->set_rules('kategori_id', 'Kategori', 'trim|required');
		$this->form_validation->set_rules('nama', '', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('harga', '', 'trim|required');
		$this->form_validation->set_rules('berat', '', 'trim|required');
		$this->form_validation->set_rules('satuan', '', 'trim|required|max_length[25]');
		$this->form_validation->set_rules('deskripsi', '', 'trim|required');
		$this->form_validation->set_rules('stok_st', '', 'trim|required');
		$this->form_validation->set_rules('active_st', '', 'trim|required');

		if ($this->input->post('rekomendasi_st',TRUE) == 'on') {
			$rekomendasi = 'yes';
		}else{
			$rekomendasi = 'no';
		}

		$datePrefix = date('ymd');
		if($this->form_validation->run() !== FALSE && !empty($_FILES['files']['name'])){
			$filesCount = count($_FILES['files']['name']);
			if ($filesCount > 6) {
				// default error
				$this->notif_msg('master/produk/add', 'Error', 'Jumlah gambar tidak boleh lebih dari 6.');
				redirect('master/produk/add');
			}
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['file']['name']     = $datePrefix.$_FILES['files']['name'][$i];
                $_FILES['file']['type']     = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['file']['error']     = $_FILES['files']['error'][$i];
                $_FILES['file']['size']     = $_FILES['files']['size'][$i];
                
                // File upload configuration
				$uploadPath = FCPATH.'assets/images/gambar_produk/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'jpg|jpeg|png';
                
                // Load and initialize upload library
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                // Upload file to server
                if($this->upload->do_upload('file')){
                    // Uploaded file data
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = $fileData['file_name'];
                    $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                }
			}
			$produk_id = $this->M_produk->generate_id($datePrefix);
			//format number
			$harga = preg_replace("/[^a-zA-Z0-9]/", "", $this->input->post('harga',TRUE));
			//insert data ke database
			$params = array(
				'produk_id'		=> $produk_id,
				'kategori_id'	=> $this->input->post('kategori_id',TRUE), 
				'nama'			=> $this->input->post('nama',TRUE), 
				'harga'			=> $harga, 
				'rekomendasi_st'=> $rekomendasi,  
				'berat'			=> $this->input->post('berat',TRUE), 
				'satuan'		=> $this->input->post('satuan',TRUE), 
				'deskripsi'		=> $this->input->post('deskripsi',TRUE), 
				'stok_st'		=> $this->input->post('stok_st',TRUE), 
				'active_st'		=> $this->input->post('active_st',TRUE), 
				'mdb'			=> $this->get_login('user_id'),
				'mdb_name'		=> $this->get_login('user_name'),
				'mdd'			=> date('Y-m-d H:i:s') 
			);
			if ($this->M_produk->insert('produk', $params)) {
				foreach ($uploadData as $key => $value) {
					$params_produk = array(
						'produk_id'		=> $produk_id,
						'gambar_nama'	=> $value['file_name'],
						'mdb'			=> $this->get_login('user_id'),
						'mdb_name'		=> $this->get_login('user_name'),
						'mdd'			=> date('Y-m-d H:i:s') 
					);
					// insert to gambar
					if (!$this->M_produk->insert('gambar_produk', $params_produk)) {
						// default error
						$this->notif_msg('master/produk/add', 'Error', 'Data gagal ditambahkan');
						redirect('master/produk/add');
					}
				}
			} else {
				// default error
				$this->notif_msg('master/produk/add', 'Error', 'Data gagal ditambahkan');
				redirect('master/produk/add');
			}				

			$this->notif_msg('master/produk/add', 'Sukses', 'Data berhasil ditambahkan');
			redirect('master/produk/add');	
		}
	}
	

	public function detail($produk_id='')
	{
		// set page rules
		$this->_set_page_rule("R");
		//cek data
		if (empty($produk_id)) {
			// default error
			$this->notif_msg('master/produk', 'Error', 'Data tidak ditemukan !');
		}

		//parsing
		$data = [
			'result' => $this->M_produk->get_by_id($produk_id),
			'gambar' => $this->M_produk->get_all_gambar_by_produk($produk_id)
		];
		$this->parsing_template('master/produk/detail', $data);
	}

	public function delete($produk_id='')
	{
		// set page rules
		$this->_set_page_rule("R");
		//cek data
		if (empty($produk_id)) {
			// default error
			$this->notif_msg('master/produk', 'Error', 'Data tidak ditemukan !');
		}

		//parsing
		$data = [
			'result' => $this->M_produk->get_by_id($produk_id),
			'gambar' => $this->M_produk->get_all_gambar_by_produk($produk_id)
		];
		$this->parsing_template('master/produk/delete', $data);
	}

	public function delete_process()
	{
		// set page rules
		$this->_set_page_rule("D");
		$produk_id = $this->input->post('produk_id', true);
		//cek data
		if (empty($produk_id)) {
			// default error
			$this->notif_msg('master/produk', 'Error', 'Data tidak ditemukan !');
		}

		$where = array(
			'produk_id' => $produk_id
		);
		//process
		if ($this->M_produk->delete('produk', $where)) {
			$gambar = $this->M_produk->get_all_gambar_by_produk($produk_id);
			foreach ($gambar as $value) {
				if(!unlink(FCPATH.'assets/images/gambar_produk/'.$value['gambar_nama'])){
					//default error
					$this->notif_msg('master/produk', 'Error', 'Gambar gagal dihapus !');
				}
			}
			if ($this->M_produk->delete('gambar_produk', $where)) {
				//sukses notif
				$this->notif_msg('master/produk', 'Sukses', 'Data berhasil dihapus');
			}else{
				//default error
				$this->notif_msg('master/produk', 'Error', 'Gambar gagal dihapus !');
			}			
		}else{
			//default error
			$this->notif_msg('master/produk', 'Error', 'Data gagal dihapus !');
		}
	}

	public function edit($produk_id='')
	{
		// set page rules
		$this->_set_page_rule("R");
		//default notif
		$notif = $this->session->userdata('sess_notif');
		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//cek data
		if (empty($produk_id)) {
			// default error
			$this->notif_msg('master/produk', 'Error', 'Data tidak ditemukan !');
		}

		//parsing
		$data = [
			'tipe'		=> $notif['tipe'],
			'pesan' 	=> $notif['pesan'],
			'result' 	=> $this->M_produk->get_by_id($produk_id),
			'gambar' 	=> $this->M_produk->get_all_gambar_by_produk($produk_id),
			'kategoris' => $this->M_kategori->get_all_nf()
		];
		$this->parsing_template('master/produk/edit', $data);
	}

	public function hapus_gambar()
	{
		$nama_gambar = $this->input->post('nama_gambar');
		$where = array(
			'gambar_nama' => $nama_gambar
		);
		if ($this->M_produk->delete('gambar_produk', $where)) {
			if (unlink(FCPATH.'assets/images/gambar_produk/'.$nama_gambar)) {
				echo "true";
			}else{
				echo "false";
			}
		}else{
			echo "false";
		}
	}

	// edit process
	public function edit_process() {
		// set page rules
		$this->_set_page_rule("U");
        // cek input
		$this->form_validation->set_rules('kategori_id', 'Kategori', 'trim|required');
		$this->form_validation->set_rules('nama', '', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('harga', '', 'trim|required');
		$this->form_validation->set_rules('berat', '', 'trim|required');
		$this->form_validation->set_rules('satuan', '', 'trim|required|max_length[25]');
		$this->form_validation->set_rules('deskripsi', '', 'trim|required');
		$this->form_validation->set_rules('stok_st', '', 'trim|required');
		$this->form_validation->set_rules('active_st', '', 'trim|required');

		//init var
		$produk_id = $this->input->post('produk_id',TRUE);
		$datePrefix = date('ymd');
		if (empty($produk_id)) {
			// default error
			$this->notif_msg('master/produk', 'Error', 'Data tidak ditemukan');
		}
		
		if ($this->input->post('rekomendasi_st',TRUE) == 'on') {
			$rekomendasi = 'yes';
		}else{
			$rekomendasi = 'no';
		}

		if (empty($_FILES['files']['name'][0])) {
			//tanpa upload gambar
			if ($this->form_validation->run() !== FALSE) {
				//format number
				$harga = preg_replace("/[^a-zA-Z0-9]/", "", $this->input->post('harga',TRUE));
				//insert data ke database
				$params = array(
					'kategori_id'	=> $this->input->post('kategori_id',TRUE), 
					'nama'			=> $this->input->post('nama',TRUE), 
					'harga'			=> $harga, 
					'satuan'		=> $this->input->post('satuan',TRUE), 
					'berat'			=> $this->input->post('berat',TRUE), 
					'deskripsi'		=> $this->input->post('deskripsi',TRUE), 
					'rekomendasi_st'=> $rekomendasi,  
					'stok_st'		=> $this->input->post('stok_st',TRUE), 
					'active_st'		=> $this->input->post('active_st',TRUE), 
					'mdb'			=> $this->get_login('user_id'),
					'mdb_name'		=> $this->get_login('user_name'),
					'mdd'			=> date('Y-m-d H:i:s') 
				);
				$where = array(
					'produk_id' => $produk_id
				);
				if ($this->M_produk->update('produk', $params, $where)) {
					// default error
					$this->notif_msg('master/produk/edit/'.$produk_id, 'Sukses', 'Data berhasil dirubah');
	
				} else {
					// default error
					$this->notif_msg('master/produk/edit/'.$produk_id, 'Error', 'Data gagal ditambahkan');
	
				}
			}else{
				// default error
				$this->notif_msg('master/produk/edit/'.$produk_id, 'Error', 'Data gagal ditambahkan');

			}
		}else{
			//dengan gambar
			$filesCount = count($_FILES['files']['name']);
			if ($filesCount > 6) {
				// default error
				$this->notif_msg('master/produk/edit/'.$produk_id, 'Error', 'Jumlah gambar tidak boleh lebih dari 6.');
			}
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['file']['name']     = $datePrefix.$_FILES['files']['name'][$i];
                $_FILES['file']['type']     = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['file']['error']     = $_FILES['files']['error'][$i];
                $_FILES['file']['size']     = $_FILES['files']['size'][$i];
                
                // File upload configuration
				$uploadPath = FCPATH.'assets/images/gambar_produk/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'jpg|jpeg|png';
                
                // Load and initialize upload library
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                // Upload file to server
                if($this->upload->do_upload('file')){
                    // Uploaded file data
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = $fileData['file_name'];
                    $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                }
			}
			//format number
			$harga = preg_replace("/[^a-zA-Z0-9]/", "", $this->input->post('harga',TRUE));
			//insert data ke database
			$params = array(
				'kategori_id'	=> $this->input->post('kategori_id',TRUE), 
				'nama'			=> $this->input->post('nama',TRUE), 
				'harga'			=> $harga, 
				'berat'			=> $this->input->post('berat',TRUE), 
				'satuan'		=> $this->input->post('satuan',TRUE), 
				'rekomendasi_st'=> $rekomendasi,  
				'deskripsi'		=> $this->input->post('deskripsi',TRUE), 
				'stok_st'		=> $this->input->post('stok_st',TRUE), 
				'active_st'		=> $this->input->post('active_st',TRUE), 
				'mdb'			=> $this->get_login('user_id'),
				'mdb_name'		=> $this->get_login('user_name'),
				'mdd'			=> date('Y-m-d H:i:s') 
			);
			$where = array(
				'produk_id' => $produk_id
			);
			if ($this->M_produk->update('produk', $params, $where)) {
				foreach ($uploadData as $key => $value) {
					$params_produk = array(
						'produk_id'		=> $produk_id,
						'gambar_nama'	=> $value['file_name'],
						'mdb'			=> $this->get_login('user_id'),
						'mdb_name'		=> $this->get_login('user_name'),
						'mdd'			=> date('Y-m-d H:i:s') 
					);
					// insert to gambar
					if (!$this->M_produk->insert('gambar_produk', $params_produk)) {
						// default error
						$this->notif_msg('master/produk/edit/'.$produk_id, 'Error', 'Data gagal ditambahkan');
		
					}
				}
			} else {
				// default error
				$this->notif_msg('master/produk/edit/'.$produk_id, 'Error', 'Data gagal ditambahkan');
			}				

			$this->notif_msg('master/produk/edit/'.$produk_id, 'Sukses', 'Data berhasil ditambahkan');		
		}
	}
}