<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*
----------------------
Created by : Lathiif Aji Santhosho
Phone : 	082126641201
----------------------
*/

class Kategori extends MY_Controller {

	//init serach name
	const SESSION_SEARCH = 'search_kategori';
    // constructor
	public function __construct()
	{
		parent::__construct();
		
		//load models
		$this->load->model('master/M_kategori');
        
		//parsing js url
		$this->parsing_js([
			// 'assets/costum/js/main.js'
		   ]);
		//parsing css url
		$this->parsing_css([
			// 'assets/css/smart_wizard_theme_dots.css'
			]);

		//parsing data tanpa template
		$this->parsing([
			'title' => 'Kategori'
		]);
	}

	public function index()
	{
		// set page rules
		$this->_set_page_rule("R");
		//default notif
		$notif = $this->session->userdata('sess_notif');

		// search session
		$search = $this->session->userdata(self::SESSION_SEARCH);

		//create pagination
		$this->load->library('pagination');
		$this->load->config('pagination');
		$config = $this->config->item('pagination_config');
		$total_row = $this->M_kategori->count_all();
		$config['base_url'] = base_url('index.php/master/kategori/index/');
		$config['total_rows'] = $total_row;
		$config['per_page'] = 10;
		$from = $this->uri->segment(4);
		$this->pagination->initialize($config);		
		$result = $this->M_kategori->get_all($config['per_page'],$from, $search);
		if (empty($result)) {
			$no = 0;
		}else{
			$no = 1;
		}

		/*
		isi parameter yang akan di parsing dalam bentuk array 		
		variabel parsing = [
			penamaan 	=> isi dari sebuah data
		];
		*/
		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'search' 		=> $search,
			'result' 		=> $result,
			'no' 			=> $no,
			'pagination'	=> $this->pagination->create_links()
		];
		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->parsing_template('master/kategori/index', $data);
	}

	public function search_process() {
		// page rule read
		$this->_set_page_rule("R");

		if ($this->input->post('search', true) == "tampilkan") {
		  $params = array(
			'kategori_nama'   	=> $this->input->post('nama', true)
		  );
		  $this->session->set_userdata(self::SESSION_SEARCH, $params);
		} else {
		  $this->session->unset_userdata(self::SESSION_SEARCH);
		}
		redirect("master/kategori");
	}

	public function add($notif='')
	{
		// set page rules
		$this->_set_page_rule("C");
		//default notif
		$notif = $this->session->userdata('sess_notif');
		//delete session notif
		$this->session->unset_userdata('sess_notif');
		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
		];
		//parsing (template_content, variabel_parsing)
		$this->parsing_template('master/kategori/add', $data);
	}

	 // add process
	 public function add_process() {
		// set page rules
		$this->_set_page_rule("C");
        // cek input
		$this->form_validation->set_rules('kategori_nama', 'Nama Kategori', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('kategori_st', 'Status Kategori', 'trim|required');
        // process
        if ($this->form_validation->run() !== FALSE) {
			$filesCount = count($_FILES['files']['name']);
			if ($filesCount < 1) {
				// default error
				$this->notif_msg('master/kategori/add', 'Error', 'Logo tidak boleh kosong');
			}
			$datePrefix = date('ymd');
			$_FILES['file']['name']     = $datePrefix.$_FILES['files']['name'];
			$_FILES['file']['type']     = $_FILES['files']['type'];
			$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'];
			$_FILES['file']['error']     = $_FILES['files']['error'];
			$_FILES['file']['size']     = $_FILES['files']['size'];
			// File upload configuration
			$uploadPath = FCPATH.'assets/images/logo_kategori/';
			$config['upload_path'] = $uploadPath;
			$config['allowed_types'] = 'jpg|jpeg|png|ico';
			// Load and initialize upload library
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			// Upload file to server
			if($this->upload->do_upload('file')){
				// Uploaded file data
				$fileData = $this->upload->data();
				$params = array(
					'logo'			=> $fileData['file_name'],
					'kategori_nama'	=> $this->input->post('kategori_nama'), 
					'kategori_st'	=> $this->input->post('kategori_st'), 
					'mdb'			=> $this->get_login('user_id'),
					'mdb_name'		=> $this->get_login('user_name'),
					'mdd'			=> date('Y-m-d H:i:s') 
				);
				// insert
				if ($this->M_kategori->insert('kategori', $params)) {
					// insert to kategori
					$this->notif_msg('master/kategori/add', 'Sukses', 'Data berhasil ditambahkan');
				} else {
					// default error
					$this->notif_msg('master/kategori/add', 'Error', 'Data gagal ditambahkan');
				}
			}else{
				// default error
				$this->notif_msg('master/kategori/add', 'Error', 'Logo gagal diupload');
			}
			
        } else {
			// default error
			$this->notif_msg('master/kategori/add', 'Error', 'Data gagal ditambahkan');
        }
    }

	public function delete($kategori_id='')
	{
		// set page rules
		$this->_set_page_rule("D");
		//cek data
		if (empty($kategori_id)) {
			// default error
			$this->notif_msg('master/kategori', 'Error', 'Data tidak ditemukan !');
		}
		//parsing
		$data = [
			'result' => $this->M_kategori->get_by_id($kategori_id)
		];
		$this->parsing_template('master/kategori/delete', $data);
	}

	public function delete_process()
	{
		// set page rules
		$this->_set_page_rule("D");
		$kategori_id = $this->input->post('kategori_id', true);
		//cek data
		if (empty($kategori_id)) {
			// default error
			$this->notif_msg('master/kategori', 'Error', 'Data tidak ditemukan !');
		}

		$where = array(
			'kategori_id' => $kategori_id
		);
		//process
		if ($this->M_kategori->delete('kategori', $where)) {
			//sukses notif
			$this->notif_msg('master/kategori', 'Sukses', 'Data berhasil dihapus');
		}else{
			//default error
			$this->notif_msg('master/kategori', 'Error', 'Data gagal dihapus !');
		}
	}

	public function edit($kategori_id='')
	{
		// set page rules
		$this->_set_page_rule("U");
		//default notif
		$notif = $this->session->userdata('sess_notif');
		//cek data
		if (empty($kategori_id)) {
			// default error
			$this->notif_msg('master/kategori', 'Error', 'Data tidak ditemukan !');
		}
		//parsing
		$data = [
			'tipe'		=> $notif['tipe'],
			'pesan' 	=> $notif['pesan'],
			'result' 	=> $this->M_kategori->get_by_id($kategori_id),
		];
		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing and view content
		$this->parsing_template('master/kategori/edit', $data);
	}

	// edit process
	public function edit_process() {
		// set page rules
		$this->_set_page_rule("U");
        // cek input
        $this->form_validation->set_rules('kategori_nama', 'User Email', 'trim|required');
		// check data
        if (empty($this->input->post('kategori_nama'))) {
            //sukses notif
			$this->notif_msg('master/kategori', 'Error', 'Data tidak ditemukan');
		}
		$kategori_id = $this->input->post('kategori_id');
        // process
        if ($this->form_validation->run() !== FALSE) {
			$filesCount = count($_FILES['files']['name']);
			if ($filesCount < 1) {
				//tanpalogo
				$params= array(
					'kategori_nama' => $this->input->post('kategori_nama'),
					'kategori_st'   => $this->input->post('kategori_st'),
				);
				$where = array(
					'kategori_id' 	=> $kategori_id 	
				);
	
				if ($this->M_kategori->update('kategori', $params ,$where)) {
					//sukses notif
					$this->notif_msg('master/kategori/edit/'.$kategori_id, 'Sukses', 'Data berhasil diubah');
				}else{
					//default error
					$this->notif_msg('master/kategori/edit/'.$kategori_id, 'Error', 'Data gagal diubah !');
				}
			}else{
				//dengan logo
				unlink(FCPATH.'assets/images/logo_kategori/'.$this->input->post('old_logo'));
				$datePrefix = date('ymd');
				$_FILES['file']['name']     = $datePrefix.$_FILES['files']['name'];
				$_FILES['file']['type']     = $_FILES['files']['type'];
				$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'];
				$_FILES['file']['error']     = $_FILES['files']['error'];
				$_FILES['file']['size']     = $_FILES['files']['size'];
				// File upload configuration
				$uploadPath = FCPATH.'assets/images/logo_kategori/';
				$config['upload_path'] = $uploadPath;
				$config['allowed_types'] = 'jpg|jpeg|png|ico';
				// Load and initialize upload library
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				// Upload file to server
				if($this->upload->do_upload('file')){
					// Uploaded file data
					$fileData = $this->upload->data();
					//tanpalogo
					$params= array(
						'logo' 			=> $fileData['file_name'],
						'kategori_nama' => $this->input->post('kategori_nama'),
						'kategori_st'   => $this->input->post('kategori_st'),
					);
					$where = array(
						'kategori_id' 	=> $kategori_id 	
					);
		
					if ($this->M_kategori->update('kategori', $params ,$where)) {
						//sukses notif
						$this->notif_msg('master/kategori/edit/'.$kategori_id, 'Sukses', 'Data berhasil diubah');
					}else{
						//default error
						$this->notif_msg('master/kategori/edit/'.$kategori_id, 'Error', 'Data gagal diubah !');
					}
				}else{
					// default error
					$this->notif_msg('master/kategori/add', 'Error', 'Logo gagal diupload');
				}
			}
		} else {
			// default error
			$this->notif_msg('master/kategori/edit/'.$kategori_id, 'Error', 'Data gagal diedit');
		}
    }
}
