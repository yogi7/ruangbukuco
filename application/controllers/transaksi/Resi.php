<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*
----------------------
Created by : Lathiif Aji Santhosho
Phone : 	082126641201
----------------------
*/

class Resi extends MY_Controller {

    // constructor
	public function __construct()
	{
		parent::__construct();
		
		//load models
		$this->load->model('client/M_transaksi');
		$this->load->model('master/M_produk');
        //js
		$this->parsing_js([
		]);
		//parsing css url
		$this->parsing_css([
		]);

		//parsing data tanpa template
		$this->parsing([
			'title' => 'Resi'
		]);
	}

	public function index()
	{
		// set page rules
		$this->_set_page_rule("R");
		//default notif
		$notif = $this->session->userdata('sess_notif');
		//result
		$result = $this->M_transaksi->get_all_pembelian();
		//loop gambar
		$gambar = array();
		if (!empty($result)) {
			foreach ($result as $key => $value) {
				$where = array(
					'b.transaksi_id' 	=> $value['transaksi_id'] 
				);
				$transaksi_produk[] = $this->M_transaksi->get_by_user_transaksi_id($where);
			}
			//loop gambar
			foreach ($transaksi_produk as $key => $value) {
				foreach ($value as $i => $val) {
					$gambar[$key] = $this->M_transaksi->get_gambar_by_produk($val['produk_id']);
				}
			}
		}
		if (!empty($result)) {
			$no = 1;
		}else{
			$no = '';
		}

		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'result' 		=> $result,
			'no' 			=> $no,
			'gambar' 		=> $gambar
		];

		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->parsing_template('transaksi/resi/index', $data);
	}

	public function detail($transaksi_id = '')
	{
		// set page rules
		$this->_set_page_rule("U");
		//default notif
		$notif = $this->session->userdata('sess_notif');
		//result
		$result = $this->M_transaksi->get_kurir_by_transaksi($transaksi_id);
		//lacak
		$get_lacak = $this->ro_cek_resi($result['resi'], $result['nama_kurir']);
		if ($get_lacak) {
			$status_resi = $get_lacak['status'];
			$track = $get_lacak['result'];
		}else{
			$status_resi = '';
			$track = '';
		}
		//parsing
		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'result' 		=> $result,
			'status_resi' 	=> $status_resi,
			'track' 		=> $track,
		];


		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->parsing_template('transaksi/resi/detail', $data);
	}

	public function update_resi_process()
	{
		$this->form_validation->set_rules('transaksi_id', '', 'trim|required');
		$transaksi_id = $this->input->post('transaksi_id', TRUE);
		//cek validasi
		if ($this->form_validation->run() !== FALSE) {
			$params = array(
				'resi' => $this->input->post('resi', TRUE)
			);
			$where = array(
				'transaksi_id' => $transaksi_id
			);
			if ($this->M_transaksi->update('info_kurir', $params, $where)) {
				$params_transaksi = array(
					'transaksi_st' => 'dikirim'
				);
				if ($this->M_transaksi->update('transaksi', $params_transaksi, $where)) {
					//notif
					$this->notif_msg('transaksi/resi/detail/'.$transaksi_id, 'Sukses', 'Resi berhasil di perbarui');
				}else{
					// default error
					$this->notif_msg('transaksi/resi/detail/'.$transaksi_id, 'Error', 'Resi gagal diperbarui !');
				}
			}else{
				// default error
				$this->notif_msg('transaksi/resi/detail/'.$transaksi_id, 'Error', 'Resi gagal diperbarui !');
			}
		}		
	}

}