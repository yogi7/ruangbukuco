<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*
----------------------
Created by : Lathiif Aji Santhosho
Phone : 	082126641201
----------------------
*/

class Pembelian extends MY_Controller {

    // constructor
	public function __construct()
	{
		parent::__construct();
		
		//load models
		$this->load->model('client/M_transaksi');
        //js
		$this->parsing_js([
		]);
		//parsing css url
		$this->parsing_css([
		]);

		//parsing data tanpa template
		$this->parsing([
			'title' => 'Pembelian'
		]);
	}

	public function index()
	{
		// set page rules
		$this->_set_page_rule("R");
		//default notif
		$notif = $this->session->userdata('sess_notif');
		//result
        $result = $this->M_transaksi->get_all_transaksi();
       
		//loop gambar
		$gambar = array();
		foreach ($result as $key => $value) {
				$gambar[$key] = $this->M_transaksi->get_gambar_by_produk($value['produk_id']);
		}
		$data = [
			// 'tipe'			=> $notif['tipe'],
			// 'pesan' 		=> $notif['pesan'],
            'result' 		=> $result,
            'gambar'        => $gambar,
		];

		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->parsing_template('transaksi/pembelian/index', $data);
    }
    
    public function detail($transaksi_id = '')
	{
		//default notif
		$notif = $this->session->userdata('sess_notif');
		$where = array(
			'a.transaksi_id' => $transaksi_id
 		);
		$result_transaksi = $this->M_transaksi->get_by_transaksi_id($where);
        $bukti_transfer = $this->M_transaksi->get_bukti_by_transaksi($transaksi_id);
		//parsing
		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'result' 		=> $result_transaksi,
            'bukti_transfer'=> $bukti_transfer,
		];
		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->parsing_template('transaksi/pembelian/detail', $data);
	}

	public function detail_produk($transaksi_id = '')
	{
		//default notif
		$notif = $this->session->userdata('sess_notif');
		$where = array(
			'a.transaksi_id' => $transaksi_id
 		);
		$result_transaksi = $this->M_transaksi->get_by_transaksi_id($where);
		$result_produk = $this->M_transaksi->get_produk_by_transaksi($transaksi_id);
		//loop gambar
		$gambar = array();
		foreach ($result_produk as $key => $value) {
				$gambar[$key] = $this->M_transaksi->get_gambar_by_produk($value['produk_id']);
		}
		// $this->cek($result_transaksi);
		//parsing
		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'result' 		=> $result_transaksi,
			'produk' 		=> $result_produk,
			'gambar' 		=> $gambar,
		];
		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->parsing_template('transaksi/pembelian/detail_produk', $data);
	}
	
	public function detail_tujuan($transaksi_id = '')
	{
		//default notif
		$notif = $this->session->userdata('sess_notif');
		$where = array(
			'a.transaksi_id' => $transaksi_id
 		);
		$result_transaksi = $this->M_transaksi->get_by_transaksi_id($where);
		$result_alamat = $this->M_transaksi->get_alamat_by_transaksi($transaksi_id);
		//parsing
		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'result' 		=> $result_transaksi,
			'alamat' 		=> $result_alamat,
		];
		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->parsing_template('transaksi/pembelian/detail_tujuan', $data);
	}

	public function detail_kurir($transaksi_id = '')
	{
		//default notif
		$notif = $this->session->userdata('sess_notif');
		$where = array(
			'a.transaksi_id' => $transaksi_id
 		);
		$result_transaksi = $this->M_transaksi->get_by_transaksi_id($where);
		$result_kurir = $this->M_transaksi->get_kurir_by_transaksi($transaksi_id);

		//lacak
		$get_lacak = $this->ro_cek_resi($result_kurir['resi'], $result_kurir['nama_kurir']);
		if ($get_lacak) {
			$status_resi = $get_lacak['status'];
			$track = $get_lacak['result'];
		}else{
			$status_resi = '';
			$track = '';
		}

		//parsing
		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'result' 		=> $result_transaksi,
			'kurir' 		=> $result_kurir,
			'status_resi' 	=> $status_resi,
			'track' 		=> $track,
		];
		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->parsing_template('transaksi/pembelian/detail_kurir', $data);
	}
}