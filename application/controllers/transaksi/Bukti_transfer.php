<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*
----------------------
Created by : Lathiif Aji Santhosho
Phone : 	082126641201
----------------------
*/

class Bukti_transfer extends MY_Controller {

    // constructor
	public function __construct()
	{
		parent::__construct();
		
		//load models
		$this->load->model('client/M_transaksi');
        //js
		$this->parsing_js([
		]);
		//parsing css url
		$this->parsing_css([
		]);

		//parsing data tanpa template
		$this->parsing([
			'title' => 'Bukti Transfer'
		]);
	}

	public function index()
	{
		// set page rules
		$this->_set_page_rule("R");
		//default notif
		$notif = $this->session->userdata('sess_notif');
		//result
		$result = $this->M_transaksi->get_all_bukti();
		if (empty($result)) {
			$no = '';
		}else{
			$no = 1;
		}

		$data = [
			'tipe'			=> $notif['tipe'],
			'pesan' 		=> $notif['pesan'],
			'result' 		=> $result,
			'no' 			=> $no
		];

		//delete session notif
		$this->session->unset_userdata('sess_notif');
		//parsing (template_content, variabel_parsing)
		$this->parsing_template('transaksi/bukti_transfer/index', $data);
	}

	public function search_process() {
		// page rule read
		$this->_set_page_rule("R");

		if ($this->input->post('search', true) == "tampilkan") {
		  $params = array(
			'kategori_nama'   	=> $this->input->post('nama', true)
		  );
		  $this->session->set_userdata(self::SESSION_SEARCH, $params);
		} else {
		  $this->session->unset_userdata(self::SESSION_SEARCH);
		}
		redirect("master/produk");
	}

	public function confirm_process($transaksi_id = '')
	{
		// set page rules
		$this->_set_page_rule("U");
		if (empty($transaksi_id)) {
			// default error
			$this->notif_msg('transaksi/bukti_transfer', 'Error', 'Data tidak ditemukan !');
		}

		$params = array(
			'transaksi_st' => 'dibayar'
		);

		$where = array(
			'transaksi_id' => $transaksi_id
		);

		if ($this->M_transaksi->update('transaksi', $params, $where)) {
			// notif
			$this->notif_msg('transaksi/bukti_transfer', 'Sukses', 'Bukti transfer berhasil di verifikasi, segera kirim produknya.');
		} else {
			// default error
			$this->notif_msg('transaksi/bukti_transfer', 'Error', 'Gagal memverifikasi !');
		}
	}

	public function reject_process($transaksi_id = '')
	{
		// set page rules
		$this->_set_page_rule("U");
		if (empty($transaksi_id)) {
			// default error
			$this->notif_msg('transaksi/bukti_transfer', 'Error', 'Data tidak ditemukan !');
		}

		$params = array(
			'transaksi_st' => 'dibeli'
		);

		$where = array(
			'transaksi_id' => $transaksi_id
		);

		$bukti_transaksi = $this->M_transaksi->get_bukti_by_transaksi($where);

		if ($this->M_transaksi->update('transaksi', $params, $where)) {
			if ($this->M_transaksi->delete('bukti_transfer', $where)) {
				// unlink
				if(!unlink(FCPATH.'assets/images/bukti_transfer/'.$bukti_transaksi['nama_file'])){
					// default error
					$this->notif_msg('transaksi/bukti_transfer', 'Error', 'Gagal memverifikasi !');
				}
			}else{
				// default error
				$this->notif_msg('transaksi/bukti_transfer', 'Error', 'Gagal memverifikasi !');
			}
			// default error
			$this->notif_msg('transaksi/bukti_transfer', 'Sukses', 'Bukti transfer berhasil ditolak.');
		} else {
			// default error
			$this->notif_msg('transaksi/bukti_transfer', 'Error', 'Gagal memverifikasi !');
		}
	}
}